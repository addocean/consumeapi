import sys
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from consumeAPIRest import ConsumeAPIRest
from consumeTP_APIRest import ConsumeTP_APIRest

delete_all = 0

if len(sys.argv) == 2:
	voyage_number = sys.argv[1]
elif len(sys.argv) == 1:
	delete_all = input("Confirmas que quieres borrar todos los viajes?")
	if delete_all != 's':
		print("Argumentos incorrectos.")
		exit()
else:
	print("Argumentos incorrectos.")
	exit()

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcdc2/")

username = "apmtAdmin"
print("Conecto a SLCD como " + username)
slcd_api.send_user(username = username)

# Borrado de todos los viajes
if delete_all == 's':

	for counter in range(0,500):
		voyage = slcd_api.get_voyage_from_id(counter)

		if voyage != None:
			if "SV/" in voyage.number:

				print("Borrando viaje " + voyage.number)

				username = "coscoAdmin"
				print("Conecto a SLCD como " + username)
				slcd_api.send_user(username = username)

				lists = slcd_api.get_lists_from_voyage(voyage.number)
				for anylist in lists:
					utis = slcd_api.get_utis_from_voyage(anylist.id)
					for uti in utis:
						slcd_api.delete_uti(uti)

					slcd_api.delete_list(anylist)

				username = "apmtAdmin"
				print("Conecto a SLCD como " + username)
				slcd_api.send_user(username = username)

				lists = slcd_api.get_lists_from_voyage(voyage.number)
				for anylist in lists:
					slcd_api.delete_list(anylist)

				slcd_api.delete_voyage(voyage)

	exit() 

# Borrado de un viaje por su número
else:

	voyages = slcd_api.get_voyages_from_number(voyage_number)

	username = "coscoAdmin"
	print("Conecto a SLCD como " + username)
	slcd_api.send_user(username = username)

	if len(voyages) > 0:
		lists = slcd_api.get_lists_from_voyage(voyage_number)
		for anylist in lists:
			utis = slcd_api.get_utis_from_voyage(anylist.id)
			for uti in utis:
				slcd_api.delete_uti(uti)

			slcd_api.delete_list(anylist)

		username = "apmtAdmin"
		print("Conecto a SLCD como " + username)
		slcd_api.send_user(username = username)

		lists = slcd_api.get_lists_from_voyage(voyage_number)
		for anylist in lists:
			slcd_api.delete_list(anylist)

		slcd_api.delete_voyage(voyages[0])
	else:
		print("No encuentro en viaje.")






