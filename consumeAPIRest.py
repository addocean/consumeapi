
import requests
import time
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.RMT import RMT
from model.Notification import Notification

USER_ENDPOINT = "user"
VOYAGE_ENDPOINT = "voyage-api/voyage/"
LIST_ENDPOINT = "list-api/list/"
UTI_ENDPOINT = "uti-api/uti/"
RMT_ENDPOINT = "rmt-api/rmt/"
XML_ENDPOINT = "xml-api/xml/"
XML_ENDPOINT_NEW_COARRI = "xml-api-obj/coarri/"
XML_ENDPOINT_NEW_COARRI_MASSIVE = "xml-api-obj/coarris/"
NOTIFICATIONS_ENDPOINT = "notifications-api/notification/"


class ConsumeAPIRest:

    def __init__(self ,url):
        self.name = ""
        self.api = url
        self.token = None

    def send_user(self, username, print_log=False):
        headers = {'Content-type': 'application/json'}
        # time.sleep(1)
        response = requests.post(self.api + "userOld", data=username, headers=headers)
        if not response.ok:
            if print_log:
                print("\tsend_user - ERROR: " + str(response))
            self.token = None
        else:
            # print("\tsend_user - Token: Ok")
            if print_log:
                print("send_user - Token: " + str(response.content))
            self.token = response.content
        return response

    def send_user_new(self, user, print_log=False):
        headers = {'Content-type': 'application/json'}
        # time.sleep(1)
        response = requests.post(self.api + "user", data=user.to_json(), headers=headers)
        if not response.ok:
            if print_log:
                print("\tsend_user - ERROR: " + str(response))
            self.token = None
        else:
            # print("\tsend_user - Token: Ok")
            if print_log:
                print("send_user - Token: " + str(response.content))
            self.token = response.content
        return response

    def create_voyage(self, voyage, print_log=False):
        result = -1
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.post(self.api + VOYAGE_ENDPOINT, data=voyage.to_json(), headers=headers)
        #print(voyage.to_json())
        if not response.ok:
            if print_log:
                print("\tcreate_voyage - ERROR: " + str(response) + " - " + str(response.content))
        else:
            if print_log:
                print("\tcreate_voyage - OK -> " + str(response.content))
            #print(str(response.content))
            if not response.content == b"Updated":
                result = int(response.content[len("Created with id "):])
        return result

    def put_voyage(self, voyage):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # print(voyage.to_json())
        # time.sleep(1)
        response = requests.put(self.api + VOYAGE_ENDPOINT, data=voyage.to_json(), headers=headers)
        if not response.ok:
            print("\tput_voyage - ERROR: " + str(response))
        else:
            print("\tput_voyage  (Result = " + str(response.content) + ")")

    def get_voyage_from_id(self, voyage_id):
        voyage = None
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + VOYAGE_ENDPOINT + str(voyage_id), headers=headers)
        if not response.ok:
            print("\tget_voyage_from_id " + str(voyage_id) + " - ERROR: " + str(response))
        else:
            # print(response.json())
            voyage = Voyage.create_from_json(response.json())
            #print("\tget_voyage_from_id " + str(voyage_id) + " (Result = Ok)")
            # voyage.print()
        return voyage

    def get_voyages_from_number(self, number, print_log = False):
        result = list()
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + VOYAGE_ENDPOINT + "findByParams?number=" + str(number), headers=headers)
        if not response.ok:
            if print_log:
                print("\tget_voyage_from_number - ERROR: " + str(response))
        else:
            if print_log:
                print("\tget_voyage_from_number (Result = " + str(len(response.json())) + ")")
            for json_element in response.json():
                voyage_ = Voyage.create_from_json(json_element)
                result.append(voyage_)
                # voyage_.print()
                # print()
        return result

    def get_voyages_from_visit_number(self, number, print_log = False):
        result = list()
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + VOYAGE_ENDPOINT + "findByParams?number=&visit_number=" + str(number), headers=headers)
        if not response.ok:
            if print_log:
                print("\tget_voyages_from_visit_number - ERROR: " + str(response))
        else:
            if print_log:
                print("\tget_voyages_from_visit_number (Result = " + str(len(response.json())) + ")")
            for json_element in response.json():
                voyage_ = Voyage.create_from_json(json_element)
                result.append(voyage_)
                # voyage_.print()
                # print()
        return result

    def delete_voyage(self, voyage_to_delete):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.delete(self.api + VOYAGE_ENDPOINT + str(voyage_to_delete.id), headers=headers)
        if not response.ok:
            print("\tdelete_voyage - ERROR: " + str(response))
        else:
            print("\tdelete_voyage: Ok")

    def create_list(self, list_, print_json=False):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        if print_json:
            print(list_.to_json())
        # time.sleep(1)
        response = requests.post(self.api + LIST_ENDPOINT, data=list_.to_json(), headers=headers)
        if not response.ok:
            print("\tcreate_list - ERROR: " + str(response) + " - " + str(response.content))
        elif print_json:
            print("\tcreate_list - OK -> " + str(response.content))

    def get_list_from_id(self, id_):
        list_ = None
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + LIST_ENDPOINT + str(id_), headers=headers)
        if not response.ok:
            print("\tget_list_from_id " + str(id_) + " - ERROR: " + str(response))
        else:
            # print("\tget_list_from_id: Ok")
            list_ = List.create_from_json(response.json())
            # print(response.json())
            # list_.print()
            # print()
        return list_

    def get_lists_from_voyage(self, number, print_log = False):
        result = []
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + LIST_ENDPOINT + "findByParams?number=" + str(number), headers=headers)
        if not response.ok:
            if print_log:
                print("\tget_lists_from_voyage - ERROR: " + str(response))
        else:
            if print_log:
                print("\tget_lists_from_voyage (Result = " + str(len(response.json())) + "):")
            for json_element in response.json():
                list_ = List.create_from_json(json_element)
                result.append(list_)
                # list_.print()
                # print()
        return result

    def put_list(self, list_):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # print(list_.to_json())
        # time.sleep(1)
        response = requests.put(self.api + LIST_ENDPOINT, data=list_.to_json(), headers=headers)
        if not response.ok:
            print("\tput_list - ERROR: " + str(response))
        else:
            print("\tput_list  (Result = " + str(response.content) + ")")

    def delete_list(self, list_to_delete):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.delete(self.api + LIST_ENDPOINT + str(list_to_delete.id), headers=headers)
        if not response.ok:
            print("\tdelete_list - ERROR: " + str(response))
        else:
            print("\tdelete_list: Ok")

    def send_uti_to_voyage(self, new_uti, print_json=False):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        if print_json:
            print(new_uti.to_json())
        # time.sleep(1)
        response = requests.post(self.api + UTI_ENDPOINT, data=new_uti.to_json(), headers=headers)
        if not response.ok:
            print("\tsend_uti_to_voyage - ERROR: " + str(response))
            print("\tsend_uti_to_voyage - ERROR: " + str(response.content))
        else:
            print("\tsend_uti_to_voyage - OK -> " + str(response.content))
        return response

    def get_utis_from_voyage(self, list_id, print_log = False):
        result = []
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + UTI_ENDPOINT + "findByParams?list_id=" + str(list_id), headers=headers)
        if not response.ok:
            if print_log:
                print("\tget_utis_from_voyage - ERROR: " + str(response))
        else:
            if print_log:
                print("\tget_utis_from_voyage (Result = " + str(len(response.json())) + "):")
            for json_element in response.json():
                # print(json_element)
                uti = Uti.create_from_json(json_element)
                result.append(uti)
                # uti.print()
                # print()
        return result

    def get_uti_from_id(self, id_, print_json=False):
        uti_ = None
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + UTI_ENDPOINT + str(id_), headers=headers)
        if not response.ok:
            print("\tget_uti_from_id " + str(id_) + "- ERROR: " + str(response))
        else:
            print("\tget_uti_from_id: Ok")
            if print_json:
                print (response.json())
            uti_ = Uti.create_from_json(response.json())
        return uti_

    def put_uti(self, uti_to_modify, print_json=False):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        if print_json:
            print(uti_to_modify.to_json())
        # time.sleep(1)
        response = requests.put(self.api + UTI_ENDPOINT, data=uti_to_modify.to_json(), headers=headers)
        if not response.ok:
            print("\tput_uti - ERROR: " + str(response))
        else:
            print("\tput_uti  (Result = " + str(response.content) + ")")

    def delete_uti(self, uti):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.delete(self.api + UTI_ENDPOINT + str(uti.id), headers=headers)
        if not response.ok:
            print("\tdelete_uti - ERROR: " + str(response))
        else:
            print("\tdelete_uti: Ok")

    def create_rmt(self, rmt, print_json=False):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}

        if print_json:
            print(rmt.to_json())

        # time.sleep(1)
        response = requests.post(self.api + RMT_ENDPOINT, data=rmt.to_json(), headers=headers)
        if not response.ok:
            print("\tcreate_rmt - ERROR: " + str(response))
        else:
            print("\tcreate_rmt  (Result = " + str(response.content) + ")")

    def get_rmt_from_id(self, id_, print_json=False):
        rmt = None
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + RMT_ENDPOINT + str(id_), headers=headers)
        if print_json:
            print(str(response.content))
        if not response.ok:
            print("\tget_rmt_from_id - ERROR: " + str(response))
        else:
            print("\tget_rmt_from_id: Ok")
            rmt = RMT.create_from_json(response.json())
        return rmt

    def modify_rmt(self, rmt, print_json=False):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # print(list_.to_json())
        # time.sleep(1)
        response = requests.put(self.api + RMT_ENDPOINT, data=rmt.to_json(), headers=headers)
        if not response.ok:
            print("\tmodify_rmt - ERROR: " + str(response))
        else:
            print("\tmodify_rmt  (Result = " + str(response.content) + ")")

    def delete_rmt(self, rmt):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.delete(self.api + RMT_ENDPOINT + str(rmt.id), headers=headers)
        if not response.ok:
            print("\tdelete_rmt - ERROR: " + str(response))
        else:
            print("\tdelete_rmt: Ok")

    def find_rmt(self, plate, booking, print_json=False):
        result = []
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + RMT_ENDPOINT + "findByParams?plate=" + plate + "&booking=" + booking, headers=headers)
        if not response.ok:
            print("\tfind_rmt - ERROR: " + str(response))
        else:
            print("\tfind_rmt (Result = " + str(len(response.json())) + "):")
            if print_json:
                print(response.json())
            for json_element in response.json():
                rmt = RMT.create_from_json(json_element)
                result.append(rmt)
                # list_.print()
                # print()
        return result

    def import_xml(self, xmldata, print_json=False):
        headers = {'Content-type': 'application/json', 'Authorization': self.token}

        if print_json:
            print(xmldata.to_json())

        result = 0
        response = requests.post(self.api + XML_ENDPOINT, data=xmldata.to_json(), headers=headers)
        if not response.ok:
            print("\timport_xml - ERROR: " + str(response) + " " + str(response.content))
        else:
            print("\timport_xml  (Result = " + str(response.content) + ")")

            if response.status_code == 202:
                time.sleep(10)
                xml_id = int(response.content[len("Xml pending to be processed. Id = "):])
                print("\timport_xml Accepted with id " + str(xml_id))

                index = 1
                while response.status_code == 202:
                    print(". " + str(index))
                    index = index + 1
                    time.sleep(1)
                    response = requests.get(self.api + XML_ENDPOINT + str(xml_id), headers=headers)

            response_string = response.content
            response_string = response_string[len("Created "):]
            response_string = response_string[:len(response_string)-len(" utis.")]
            result = int(response_string)

            print("\timport_xml  (Result = " + str(response.content) + ")")

        return result

    def import_xml_new_coarri(self, xmldata, massive=False, print_json=False):
        headers = {'Content-type': 'application/xml', 'Accept': 'application/json', 'Authorization': self.token}

        if print_json:
            print(xmldata.to_json())

        url = self.api + XML_ENDPOINT_NEW_COARRI
        if massive:
            url = self.api + XML_ENDPOINT_NEW_COARRI_MASSIVE

        result = 0
        response = requests.post(url, data=xmldata.xml, headers=headers)
        if not response.ok:
            print("\timport_xml (massive: " + str(massive) + ") - ERROR: " + str(response) + " " + str(response.content))
        else:
            print("\timport_xml (massive: " + str(massive) + ") - (Result = " + str(response.status_code) + ")")

            if response.status_code == 202:
                time.sleep(10)
                xml_id = int(response.content[len("Xml pending to be processed. Id = "):])
                print("\timport_xml Accepted with id " + str(xml_id))

                index = 1
                while response.status_code == 202:
                    print(". " + str(index))
                    index = index + 1
                    time.sleep(1)
                    response = requests.get(self.api + XML_ENDPOINT_NEW_COARRI + str(xml_id), headers=headers)

            print(response.content)
            response_string = response.json()["textResult"]

            if len(response_string) > len("Created XXX utis."):
                result = 0
            else:
                response_string = response_string[len("Created "):]
                response_string = response_string[:len(response_string) - len(" utis.")]
                result = int(response_string)

            print("\timport_xml  (Result = " + str(response.content) + ")")

        return result

    def get_notifications(self):
        result = []
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        # time.sleep(1)
        response = requests.get(self.api + NOTIFICATIONS_ENDPOINT, headers=headers)
        if not response.ok:
            print("\tget_notifications - ERROR: " + str(response))
        else:
            print("\tget_notifications (Result = " + str(len(response.json())) + "):")
            for json_element in response.json():
                notification = Notification.create_from_json(json_element)
                result.append(notification)
        return result
