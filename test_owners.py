# -*- coding: utf-8
import sys
import requests
import random
import string
import requests

from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from consumeAPIRest import ConsumeAPIRest
from datetime import datetime, timedelta
from Generator import Generator

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
#DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"


slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcdc2/")

terminal_username = "ttiaAdmin"
print("====================")
print("Conecto como " + terminal_username)
result = slcd_api.send_user(username = terminal_username)
if not result.ok:
	print("No he podido conectarme como " + username)
	exit()

voyage_number = "TTIA/0001"
voyage_eta = datetime(2020, 4, 15, 0, 0, 0)
voyage_imo = "9999999"
voyage = Voyage(imo = voyage_imo, number = voyage_number, eta = voyage_eta.strftime(DATETIME_FORMAT))
print("Creo el viaje " + voyage_number)
slcd_api.create_voyage(voyage)

print("Pido el viaje por número y por id:")
voyages_list = slcd_api.get_voyages_from_number(voyage_number)
voyage_id = voyages_list[0].id
voyage_got = slcd_api.get_voyage_from_id(voyage_id)

print("Pido las listas del viaje " + voyage_number)
ttia_lists = slcd_api.get_lists_from_voyage(voyage_number)
print("Obtengo " + str(len(ttia_lists)) + " listas:")

utilists = list()

for rxlist in ttia_lists:
	utilists = slcd_api.get_utis_from_voyage(rxlist.id)
	print("Lista Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
	if rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Carga":
		carga_parent_list = rxlist
	elif rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Descarga":
		descarga_parent_list = rxlist


terminal_username = "apmtAdmin"
print("====================")
print("Conecto como " + terminal_username)
result = slcd_api.send_user(username = terminal_username)
if not result.ok:
	print("No he podido conectarme como " + username)
	exit()

print("Pido el viaje por número y por id:")
voyages_list = slcd_api.get_voyages_from_number(voyage_number)
voyage_got = slcd_api.get_voyage_from_id(voyage_id)

print("Pido las listas del viaje " + voyage_number)
apmt_lists = slcd_api.get_lists_from_voyage(voyage_number)
print("Obtengo " + str(len(apmt_lists)) + " listas:")

utilists = list()

for rxlist in apmt_lists:
	utilists = slcd_api.get_utis_from_voyage(rxlist.id)
	print("Lista Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
	if rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Carga":
		carga_parent_list = rxlist
	elif rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Descarga":
		descarga_parent_list = rxlist


username ="coscoAdmin_fpp"
print("====================")
print("Conecto como " + username)
slcd_api.send_user(username = username)

print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)

if len(rxlists) == 0:

	print("No hay lista simples, creo listas simples de Carga y Descarga:")
	newlist = List(voyage = voyage_got, circuit="Carga", parent_list = carga_parent_list)
	slcd_api.create_list(newlist)
	newlist = List(voyage = voyage_got, circuit="Descarga", parent_list = descarga_parent_list)
	slcd_api.create_list(newlist)

	print("Y pido de nuevo las listas del viaje " + voyage.number)
	rxlists = slcd_api.get_lists_from_voyage(voyage.number)
	if len(rxlists) == 0:
		print("Algo ha ido mal creando listas simples ...")
		exit()

	print("Obtengo " + str(len(rxlists)) + " listas:")
	my_simple_list = None
	for rxlist in rxlists:
		print("Lista id: " + str(rxlist.id) + " Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit)

		total_utis = 2
		print("Añado " + str(total_utis) + " utis ...")

		for counter in range(0,total_utis):
			uti = Uti(rxlist)
			uti.verifiedWeight = 0
			result = slcd_api.send_uti_to_voyage(uti)
			if not result.ok:
				print("Algo ha ido mal creando una UTI ...")
				print(uti.to_json())
				exit()

		utilist = slcd_api.get_utis_from_voyage(rxlist.id)
		print("Obtengo las utis ... " + str(len(utilist)))

username ="cmaAdmin_fpp"
print("====================")
print("Conecto como " + username)
slcd_api.send_user(username = username)

print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)

utilist = slcd_api.get_utis_from_voyage(rxlist.id)
print("Obtengo las utis ... " + str(len(utilist)))


exit()



