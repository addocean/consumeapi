from consumeAPIRest import ConsumeAPIRest
from model.RMT import RMT
from model.RMTAgent import RMTAgent

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")

ol_username = "coscoUser2_fpp"
print("Conecto a SLCD como " + ol_username)
slcd_api.send_user(username = ol_username)

print("============")
go = input("Realizar prueba de creación de una RMT? ")
if go == 's':
	rmt = RMT()
	rmt.id = 0
	rmt.plate = "FPPU0000007"
	rmt.booking = "BN/8"
	rmt.type = "CN"
	rmt.rmtToListType = "LOLO"
	rmt.rmtStatus = "PREPARACION"
	rmt.totalPackageQty = 80
	rmt.totalGoodsWeight = 800.0

	rmt.utiAgentList = list()

	agent1 = RMTAgent()
	agent1.nif = "111111111"
	rmt.utiAgentList.append(agent1)

	agent2 = RMTAgent()
	agent2.nif = "222222222"
	rmt.utiAgentList.append(agent2)

	slcd_api.create_rmt(rmt=rmt, print_json=True)

print("============")
gp = input("Realizar prueba de consulta de una RMT? ")

plate = input("Introduce matrícula: ")
bn = input("Introduce BN: ")

rmt_list = slcd_api.find_rmt(plate=plate, booking=bn, print_json=True)

if len(rmt_list) > 0:
	for rmt_ in rmt_list:
		print("---")
		rmt_.print()

print("FIN de la prueba")
