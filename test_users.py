# -*- coding: utf-8
import sys
import requests
import random
import string
import requests

from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from consumeAPIRest import ConsumeAPIRest
from datetime import datetime, timedelta

#DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcdc2/")

username = "apmtAdmin"
print("====================")
print("Conecto como " + username)
result = slcd_api.send_user(username = username)
if not result.ok:
	print("No he podido conectarme como " + username)
	exit()

voyage_number = "TESTUSER/001"
print("Pido viaje por number: " + voyage_number)
voyages_list = slcd_api.get_voyages_from_number(voyage_number)
if len(voyages_list) == 0:
	print("Creo el viaje " + voyage_number)
	voyage_eta = datetime(2020, 2, 29, 0, 0, 0)
	voyage_imo = "9450337"
	voyage = Voyage(imo = voyage_imo, number = voyage_number, eta = voyage_eta.strftime(DATETIME_FORMAT))
	slcd_api.create_voyage(voyage)
	voyages_list = slcd_api.get_voyages_from_number(voyage_number)

if len(voyages_list) == 0:
	print("Algo ha ido mal creando viaje ...")
	exit()

my_voyage = voyages_list[0]

#print(voyage.to_json())

print("Pido las listas del viaje " + my_voyage.number)
rxlists = slcd_api.get_lists_from_voyage(my_voyage.number)
print("Obtengo " + str(len(rxlists)) + " listas:")

utilists = list()

for rxlist in rxlists:
	utilists = slcd_api.get_utis_from_voyage(rxlist.id)
	print("Lista Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
	if rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Carga":
		carga_parent_list = rxlist
	elif rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Descarga":
		descarga_parent_list = rxlist

usernames =["coscoAdmin", "cmaAdmin"]
for username in usernames:

	print("====================")
	print("Conecto como " + username)
	slcd_api.send_user(username = username)

	print("Pido las listas del viaje " + my_voyage.number)
	rxlists = slcd_api.get_lists_from_voyage(my_voyage.number)

	if len(rxlists) == 0:

		print("No hay lista simples, creo listas simples de Carga y Descarga:")
		newlist = List(voyage = my_voyage, circuit="Carga", parent_list = carga_parent_list)
		slcd_api.create_list(newlist)
		newlist = List(voyage = my_voyage, circuit="Descarga", parent_list = descarga_parent_list)
		slcd_api.create_list(newlist)
	
		rxlists = slcd_api.get_lists_from_voyage(my_voyage.number)
		print("Obtengo " + str(len(rxlists)) + " listas:")

	my_simple_list = None
	for rxlist in rxlists:
		print("Lista id: " + str(rxlist.id) + " Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit)

exit()





