from consumeAPIRest import ConsumeAPIRest
import random
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.RMT import RMT

URL = "http://localhost:8090/"
URL = "http://pre.addocean-pcs.es/slcd/"
NUMBER_OF_LISTS = 1
NUMBER_OF_CONTAINERS = 10
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class SimpleTestRMTs():
    def __init__(self):
        self.api = ConsumeAPIRest(URL)

    @staticmethod
    def new_step_message(message, force_input=False):
        do_input = False
        if do_input or force_input:
            input("\n-----\n" + message + " _")
        else:
            print(message)

    def execute(self):

        self.api.send_user("coscoAdmin_ffe", False)
        self.new_step_message("Vamos a crear una rmt", True)
        rmt = RMT()
        random_number = random.randint(1, 10000)
        rmt.plate = "FFEPlate" + str(random_number).rjust(5, '0')
        rmt.booking = "FFEBooking" + str(random_number).rjust(5, '0')
        self.api.create_rmt(rmt, False)

        self.new_step_message("Vamos a buscar la rmt por matrícula y reserva", True)
        rmts = self.api.find_rmt(rmt.plate, rmt.booking, True)
        print("Utis encontradas " + str(len(rmts)))
        rmts[0].print()

        self.new_step_message("Vamos a buscar la rmt por id", True)
        rmt2 = self.api.get_rmt_from_id(rmts[0].id, False)
        rmt2.print()

        self.new_step_message("Vamos a modifcarle la matrícula", True)
        rmt2.utiAgentList = list()
        rmt2.plate = rmt2.plate + "1"
        self.api.modify_rmt(rmt2)
        rmt2 = self.api.get_rmt_from_id(rmts[0].id, False)
        rmt2.print()

        self.new_step_message("Vamos a borrarla", True)
        self.api.delete_rmt(rmt2)


SimpleTestRMTs().execute()
