# -*- coding: utf-8
import sys
from datetime import datetime, timedelta

from Generator import Generator
from consumeAPIRest import ConsumeAPIRest
from model.Voyage import Voyage
from model.User import User

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
#DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"


if len(sys.argv) == 5:
	terminal_username = sys.argv[1]
	voyage_number = sys.argv[2]
	lists_qty = int(sys.argv[3])
	lists_size = int(sys.argv[4])
else:
	print("Argumentos incorrectos. Uso: test_utis.py terminal_username voyage_number lists_qty lists_size")
	exit()

print("Se va a generar " + str(lists_qty) + " viajes de " + terminal_username + 
	" con 4 LS (x2:C&D) con " + str(lists_size) + " UTIs en cada lista.")

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")
#slcd_api = ConsumeAPIRest("https://pruebas.teleport.es/slcd/")

go = input("Continuar? ")
if go is not 's':
	print("\nexit")
	exit()

generator = Generator(slcd_api)

for counter in range(1,lists_qty + 1):

	print("====================")
	print("Conecto como " + terminal_username)
	user = User(terminal_username, "e2bcbf95e071db30384448aad676f937")
	result = slcd_api.send_user_new(user, False)
	if not result.ok:
		print("No he podido conectarme como " + terminal_username)
		exit()

	new_voyage_number = "{}/{:02d}".format(voyage_number,counter)

	print("====================")
	print("Pido viaje por number: " + new_voyage_number)
	voyages_list = slcd_api.get_voyages_from_number(new_voyage_number)
	if len(voyages_list) == 0:
		print("Creo el viaje " + new_voyage_number)

		# Tocar en timedelta para que las listas sean del pasado o futuras
		voyage_eta = datetime.now() + timedelta(days=20)

		voyage_imo = "9450337"
		voyage = Voyage(imo = voyage_imo, number = new_voyage_number, eta = voyage_eta.strftime(DATETIME_FORMAT))
		slcd_api.create_voyage(voyage)
		voyages_list = slcd_api.get_voyages_from_number(new_voyage_number)

	if len(voyages_list) == 0:
		print("Algo ha ido mal creando viaje ...")
		exit()

	generator.addSimpleLists2Voyage(voyages_list[0], lists_size)

exit()





