from consumeAPIRest import ConsumeAPIRest
import random
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.RMT import RMT
import asserts
import time

# URL = "http://pre.addocean-pcs.es/slcd/"
URL = "http://localhost:8090/"
NUMBER_OF_LISTS = 1
NUMBER_OF_CONTAINERS = 20
DELETE_OBJECTS = False
CREATE_RMTS = True
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class SimpleTest:
    def __init__(self):
        self.api = ConsumeAPIRest(URL)
        self.plates = list()

    def get_new_plate(self, index):
        random_number = random.randint(0, 2500)
        # random_number = index
        new_plate = "ADDU" + str(random_number).rjust(7, '0')
        while self.plates.__contains__(new_plate) and len(self.plates) < 2500:
            random_number = random.randint(0, 2500)
            new_plate = "ADDU" + str(random_number).rjust(7, '0')
        self.plates.append(new_plate)
        return new_plate

    @staticmethod
    def new_step_message(message, force_input=False):
        do_input = False
        if do_input or force_input:
            input("\n-----\n" + message + " _")
        else:
            print(message)

    def check_list (self, user, id):
        response = self.api.send_user("apmtAdmin_ffe")
        actual_list = self.api.get_list_from_id(id)
        if actual_list is None:
            print("list " + str(id) + " for user " + user + " is none")
        else:
            print("list " + str(id) + " for user " + user + " ok")

    def execute(self):
        voyage_number = "CMADESC01"

        self.new_step_message("Vamos a crear un viaje para APMT", False)
        self.api.send_user("apmtAdmin_ffe")
        eta_ = (datetime.now() + timedelta(hours=100)).strftime(DATETIME_FORMAT)
        new_voyage = Voyage(imo="9839454", number=voyage_number, eta=eta_)
        self.api.create_voyage(new_voyage)

        self.new_step_message("Vamos a crear una lista simple de descarga para CMA", False)
        self.api.send_user("cmaAdmin_ffe")
        simple_list = List()
        simple_list.voyageNumber = voyage_number
        simple_list.circuit = "Descarga"
        self.api.create_list(simple_list)

        self.new_step_message("Como CMA -> Cargar utis de COPRAR DESCARGA CMA CMADESC01.edi en la lista de descarga", False)

        self.new_step_message("Como APMT -> Cerrar la lista de descarga e importar Coarri de COARRI 95B DESCARGA CMA CMADESC01.edi y cerrar el informe", False)

        self.new_step_message("Vamos a crear un viaje para APMT", False)
        voyage_number = "CMACARG02"
        self.api.send_user("apmtAdmin_ffe")
        eta_ = (datetime.now() + timedelta(hours=100)).strftime(DATETIME_FORMAT)
        new_voyage = Voyage(imo="9839454", number=voyage_number, eta=eta_)
        new_voyage.consolidator = "VESSEL_OPERATOR"
        self.api.create_voyage(new_voyage)

        self.new_step_message("Vamos a crear una lista simple de carga para CMA", False)
        self.api.send_user("cmaAdmin_ffe")
        simple_list = List()
        simple_list.voyageNumber = voyage_number
        simple_list.circuit = "Carga"
        self.api.create_list(simple_list)
        cmaSimpleList = self.api.get_lists_from_voyage(voyage_number)[0]

        self.new_step_message("Vamos a crear una lista simple de carga para Cosco", False)
        self.api.send_user("coscoAdmin_ffe")
        simple_list = List()
        simple_list.voyageNumber = voyage_number
        simple_list.circuit = "Carga"
        self.api.create_list(simple_list)
        coscoSimpleList = self.api.get_lists_from_voyage(voyage_number)[0]

        self.new_step_message("Como CMA -> Cargar utis de COPRAR CARGA CMA MSKDEMO01.edi en la lista de carga", False)

        self.new_step_message("Como Cosco -> Cargar utis de COPRAR CARGA COS MSKDEMO01.edi en la lista de carga", False)

        plate = "COSU0000001"
        self.new_step_message("Vamos a esperar que la UTI de matrícula " + plate + " de cosco esté aceptada")

        while True:
            utis = self.api.get_utis_from_voyage(coscoSimpleList.id)
            for uti in utis:
                if uti.plate == plate:
                    print ("Encontrada uti " + uti.plate + " con estado: " + uti.utiStatus)
                    if uti.levanteStatus == "ACCEPTED":
                        break
            time.sleep(10)

        exit(0)

        for j in range(NUMBER_OF_LISTS):
            self.plates = list()
            random_number = random.randint(1, 1000000)
            voyage_number = "FFE" + str(random_number).rjust(7, '0')
            print("VoyageNumber = " + voyage_number)

            self.new_step_message("Vamos a crear un viaje", False)
            response = self.api.send_user("apmtAdmin_ffe")
            asserts.assert_starts_with(response.content.decode("utf-8"), "Bearer ")
            eta_ = (datetime.now() + timedelta(hours=j-10)).strftime(DATETIME_FORMAT)
            new_voyage = Voyage(imo="9839454", number=voyage_number, eta=eta_)
            new_voyage_id = self.api.create_voyage(new_voyage)
            asserts.assert_bigger_than(new_voyage_id, 0)

            self.new_step_message("Vamos a pedir el viaje por id " + str(new_voyage_id))
            actual_voyage = self.api.get_voyage_from_id(new_voyage_id)
            actual_voyage.print()

            self.new_step_message("Vamos a pedir el viaje por número " + voyage_number)
            actual_voyage = self.api.get_voyages_from_number(voyage_number)[0]
            actual_voyage.print()

            # self.new_step_message("Vamos a sumarle dos horas al ETA")
            # new_eta = datetime.strptime(actual_voyage.dateTimeETA.replace("+0000", ""), DATETIME_FORMAT)
            # new_eta = new_eta + timedelta(hours=2)
            # actual_voyage.dateTimeETA = new_eta.strftime(DATETIME_FORMAT)
            # self.api.put_voyage(actual_voyage)
            # actual_voyage.print()

            self.new_step_message("Vamos a obtener las listas del viaje por el número de viaje " + voyage_number)
            parent_lists = self.api.get_lists_from_voyage(voyage_number)
            for parent_list in parent_lists:
                parent_list.print()

            self.new_step_message("Vamos a obtener la lista del viaje por el id " + str(parent_lists[0].id))
            self.api.get_list_from_id(parent_lists[0].id).print()

            for username in ("coscoAdmin_ffe", "cmaAdmin_ffe"):
                self.new_step_message("Cambio de usuario")
                self.api.send_user(username)

                self.new_step_message("Vamos a perdir las listas para este usuario")
                lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
                print("Listas actuales para el viaje = " + str(len(lists_from_voyage)))
                for list_from_voyage in lists_from_voyage:
                    list_from_voyage.print()

                self.new_step_message("Vamos a crear una lista simple")
                simple_list = List()
                simple_list.voyageNumber = parent_lists[0].voyageNumber
                self.api.create_list(simple_list, True)

                self.new_step_message("Vamos a perdir las listas para este usuario")
                lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
                print("Listas actuales para el viaje = " + str(len(lists_from_voyage)))
                for list_from_voyage in lists_from_voyage:
                    list_from_voyage.print()

                list_to_change = lists_from_voyage[0]
                self.new_step_message("Vamos a modificar el OpVoyageNumber de la lista " + str(list_to_change.id))
                print("Antes del cambio:")
                list_to_change.print()
                list_to_change.opVoyageNumber = username[:3] + "A12345"
                self.api.put_list(list_to_change)
                list_to_change = self.api.get_list_from_id(list_to_change.id)
                print("Después del cambio:")
                list_to_change.print()

                self.new_step_message("Pasamos a la gestión de UTIs", False)

                for actual_list in lists_from_voyage:
                    if actual_list.type == "Simple":
                        utis = self.api.get_utis_from_voyage(actual_list.id)
                        print("Utis en la lista: " + str(len(utis)))
                        asserts.assert_equals_than(0, len(utis))

                        for i in range(NUMBER_OF_CONTAINERS):
                            self.new_step_message("Vamos a crear una UTI " + str(i+1))
                            new_uti = Uti(actual_list.id)
                            new_uti.plate = self.get_new_plate()
                            self.api.send_uti_to_voyage(new_uti, False)

                        if CREATE_RMTS:
                            total_utis = self.api.get_utis_from_voyage(actual_list.id)
                            asserts.assert_equals_than(len(total_utis), NUMBER_OF_CONTAINERS)
                            for uti in total_utis:
                                self.new_step_message("Vamos a crear una rmt", False)
                                rmt = RMT()
                                rmt.plate = uti.plate
                                rmt.booking = uti.booking
                                self.api.create_rmt(rmt, False)

                                self.new_step_message("Vamos a buscar la rmt por matrícula y reserva", False)
                                rmts = self.api.find_rmt(rmt.plate, rmt.booking, False)
                                print("RMTs encontradas " + str(len(rmts)))
                                asserts.assert_equals_than(1, len(rmts))

                                self.new_step_message("Vamos a buscar la rmt por id", False)
                                rmt2 = self.api.get_rmt_from_id(rmts[0].id, False)
                                print("Id rmt encontrada = " + str(rmt2.id))
                                asserts.assert_equals_than(rmts[0].id, rmt2.id)

                if DELETE_OBJECTS:
                    lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
                    asserts.assert_equals_than(len(lists_from_voyage), 1)
                    for actual_list in lists_from_voyage:
                        if actual_list.type.code == "SIMPLE":
                            utis = self.api.get_utis_from_voyage(actual_list.id)
                            asserts.assert_equals_than(len(utis), NUMBER_OF_CONTAINERS)
                            for uti in utis:
                                self.api.delete_uti(uti)

                                if CREATE_RMTS:
                                    self.new_step_message("Vamos a borrar la rmt", False)
                                    rmts = self.api.find_rmt(uti.plate, uti.booking, False)
                                    print("RMTs encontradas " + str(len(rmts)))
                                    self.api.delete_rmt(rmts[0])

                                    self.new_step_message("Vamos a buscar la rmt por matrícula y reserva", False)
                                    rmts = self.api.find_rmt(uti.plate, uti.booking, False)
                                    print("RMTs encontradas " + str(len(rmts)))
                                    asserts.assert_equals_than(0, len(rmts))

                            utis = self.api.get_utis_from_voyage(actual_list.id)
                            asserts.assert_equals_than(len(utis), 0)
                            self.api.delete_list(actual_list)

                    lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
                    asserts.assert_equals_than(len(lists_from_voyage), 0)

                    response = self.api.send_user("apmtAdmin_ffe")
                    lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
                    asserts.assert_equals_than(len(lists_from_voyage), 2)
                    for actual_list in lists_from_voyage:
                        if actual_list.type.code == "CONSOLIDADA":
                            self.api.delete_list(actual_list)

                    lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
                    asserts.assert_equals_than(len(lists_from_voyage), 0)

                    self.api.delete_voyage(actual_voyage)
                    self.new_step_message("Vamos a pedir el viaje por id " + str(new_voyage_id))
                    actual_voyage = self.api.get_voyage_from_id(new_voyage_id)
                    asserts.assert_none_object(actual_voyage)


SimpleTest().execute()
