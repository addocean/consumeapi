# -*- coding: utf-8
import requests
import random
from datetime import datetime, timedelta

from Voyage import Voyage
from List import List
from Uti import Uti
from consumeAPIRest import ConsumeAPIRest

#DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"
		
slcd_api = ConsumeAPIRest()

username = "apmtAdmin"
print("Conecto como " + username)
slcd_api.send_user(username = username)

print("===================")

print("Prueba de creación de viaje - POST sobre voyage-api/voyage")
voyage_number = "FPP/TEST00008"

voyage_eta = datetime(2020, 1, 30, 0, 0, 0)
voyage_imo = "9450337"
print("Pido viaje por number: " + voyage_number)
voyages_list = slcd_api.get_voyages_from_number(voyage_number)
if len(voyages_list) == 0:
	print("Creo el viaje " + voyage_number)
	voyage = Voyage(imo = voyage_imo, number = voyage_number, eta = voyage_eta.strftime(DATETIME_FORMAT))
	print("Creando viaje:\n" + voyage.to_json())
	go = input("Continuar?")
	slcd_api.create_voyage(voyage)
else:
	voyage = voyages_list[0]

print("===================")

print("Prueba de petición de viaje - GET sobre voyage-api/voyage por numero de viaje = " + voyage.number)
go = input("Continuar?")
voyage_list = slcd_api.get_voyages_from_number(voyage.number)
print(voyage_list[0].to_json())
get_id = voyage_list[0].id
voyage_list.clear()

print("===================")

print("Prueba de petición de viaje - GET sobre voyage-api/voyage por id = " + str(get_id))
go = input("Continuar?")
new_voyage = slcd_api.get_voyage_from_id(voyage_id=get_id)
print(new_voyage.to_json())

print("===================")

print("Prueba de modificación de viaje - PUT sobre sobre voyage-api/voyage")
new_number = input("Introduce nuevo número de viaje: ")
if new_number != "":
	new_voyage.number = new_number
else:
	print ("No se modifica el número.")
eta_delay = 0
try:
	eta_delay = int(input("Introduce un retraso del eta: "))
except ValueError:
	print ("No se modifica el eta.")
new_eta = voyage_eta + timedelta(hours = eta_delay)
new_voyage.dateTimeETA = new_eta.strftime(DATETIME_FORMAT)
print("Nuevo viaje:")
print(new_voyage.to_json())
go = input("Continuar?")
slcd_api.put_voyage(new_voyage)
print("Consulto de nuevo el viaje de id: " + str(get_id))
print(slcd_api.get_voyage_from_id(voyage_id=get_id).to_json())

print("Y miro que tiene dos listas consolidadas:")
my_parent_list = slcd_api.get_lists_from_voyage(new_voyage.number)[0]

print("===================")

go = input("Continuar?")
username = "coscoAdmin"
print("Conecto como " + username)
slcd_api.send_user(username = username)

print("===================")

print("Prueba de creación de lista - POST sobre list-api/list")
mylist = List(voyage = new_voyage, circuit="Carga", parent_list = my_parent_list)
print("Creando lista:\n" + mylist.to_json())
go = input("Continuar?")
slcd_api.create_list(mylist)

print("===================")

print("Prueba de petición de lista - GET sobre list-api/list por numero de viaje = " + new_voyage.number)
go = input("Continuar?")
rxlist = slcd_api.get_lists_from_voyage(new_voyage.number)[0]
print(rxlist.to_json())

print("===================")

print("Prueba de petición de lista - GET sobre list-api/list por id = " + str(rxlist.id))
go = input("Continuar?")
alist = slcd_api.get_list_from_id(rxlist.id)
print(alist.to_json())

print("===================")

print("Prueba de modificación de lista - PUT sobre list-api/list")
go = input("Continuar?")
new_list = alist
new_list.opVoyageNumber = "OPVN/0000001"
print(new_list.to_json())
slcd_api.put_list(new_list)
print("Consulto de nuevo la lista de id: " + str(new_list.id))
go = input("Continuar?")
print(slcd_api.get_list_from_id(new_list.id).to_json())

print("===================")

print("Prueba de creación de uti - POST sobre uti-api/uti")
go = input("Continuar?")
uti = Uti(new_list)
print(uti.to_json())
slcd_api.send_uti_to_voyage(uti)

print("===================")

print("Prueba de petición de uti - GET sobre uti-api/uti por list_id = " + str(new_list.id))
go = input("Continuar?")
rxuti = slcd_api.get_utis_from_voyage(new_list.id)[0]
print(rxuti.to_json())

print("===================")

print("Prueba de borrado de uti - DELETE sobre uti-api/uti por uti_id = " + str(rxuti.id))
go = input("Continuar?")
slcd_api.delete_uti(rxuti)

print("===================")

print("Prueba de borrado de lista simple - DELETE sobre list-api/list por list_id = " + str(new_list.id))
go = input("Continuar?")
slcd_api.delete_list(new_list)

print("===================")
print("Prueba de borrado de listas consolidadas - DELETE sobre list-api/list por list_id = " + str(new_list.id))
username = "apmtAdmin"
print("Conecto como " + username)
slcd_api.send_user(username = username)
print("Y obtengo primero las listas consolidadas para borralas:")
con_lists = slcd_api.get_lists_from_voyage(new_voyage.number)
go = input("Continuar?")
for anylist in con_lists:
	slcd_api.delete_list(anylist)

print("Prueba de borrado de viaje - DELETE sobre voyage-api/voyage por voyage_id = " + str(new_voyage.id))
go = input("Continuar?")
slcd_api.delete_voyage(new_voyage)

print("FIN de la batida de pruebas")

