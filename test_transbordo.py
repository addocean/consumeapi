# -*- coding: utf-8
import sys
import requests
import random
import string
import requests

from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.Uti import UtiCircuit
from consumeAPIRest import ConsumeAPIRest
from datetime import datetime, timedelta
from Generator import Generator
from consumeTP_APIRest import ConsumeTP_APIRest


DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcdc2/")

print("====================")
terminal_username = "ttiaAdmin"
print("Conecto como " + terminal_username)
result = slcd_api.send_user(username = terminal_username)
if not result.ok:
	print("No he podido conectarme como " + username)
	exit()

# Creo viaje el 20/03 a las 00:40 del 9708681
# que tiene ETD: 21/03/2020 00:00
voyage_number = "AAA/0001"
print("Pido viaje por number: " + voyage_number)
voyages_list = slcd_api.get_voyages_from_number(voyage_number)
if len(voyages_list) == 0:
	print("Creo del 20/03 a las 01:40 del 9708681: " + voyage_number)
	voyage_eta = (datetime(2020, 3, 15, 0, 0, 0)).strftime(DATETIME_FORMAT)
	voyage_imo = "9708681"
	voyage = Voyage(imo = voyage_imo, number = voyage_number, eta = voyage_eta)
	voyage.initOpsDateTime = (datetime(2020, 3, 15, 0, 0, 0)).strftime(DATETIME_FORMAT)
	voyage.endOpsDateTime = (datetime(2020, 3, 15, 10, 0, 0)).strftime(DATETIME_FORMAT)
	slcd_api.create_voyage(voyage)
	voyages_list = slcd_api.get_voyages_from_number(voyage_number)

if len(voyages_list) == 0:
	print("Algo ha ido mal creando viaje ...")
	exit()

print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)
print("Obtengo " + str(len(rxlists)) + " listas:")

for rxlist in rxlists:
	utilists = slcd_api.get_utis_from_voyage(rxlist.id)
	print("Lista Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
	if rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Carga":
		carga_parent_list = rxlist
	elif rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Descarga":
		descarga_parent_list = rxlist

co_username = "coscoAdmin_fpp"
print("====================")
print("Conecto como " + co_username)
slcd_api.send_user(username = co_username)

print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)

if len(rxlists) == 0:

	print("No hay lista simples, creo lista simple de Descarga:")
	newlist = List(voyage = voyages_list[0], circuit="Descarga", parent_list = descarga_parent_list)
	slcd_api.create_list(newlist)

	print("Y pido de nuevo las listas del viaje." + voyage_number)
	rxlists = slcd_api.get_lists_from_voyage(voyage_number)
	if len(rxlists) == 0:
		print("Algo ha ido mal creando listas simples ...")
		exit()

print("Añado 4 UTIs, la 1 y 3 SIN COARRI y la 2 y 4 CON COARRI...")

uti_1 = Uti(rxlists[0])
uti_1.filledStatus = "Lleno"
uti_1.transshipmentCall = "BBB/0001"
uti_1.circuit = UtiCircuit("TRANSBORDO_MISMA")
uti_1.plate = "DESU0000001"
uti_1.coarriNumber = None
result = slcd_api.send_uti_to_voyage(uti_1)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti.to_json())
	exit()

uti_2 = Uti(rxlists[0])
uti_2.filledStatus = "Lleno"
uti_2.transshipmentCall = "BBB/0001"
uti_2.circuit = UtiCircuit("TRANSBORDO_MISMA")
uti_2.plate = "DESU0000002"
uti_2.coarriNumber = 1
result = slcd_api.send_uti_to_voyage(uti_2)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti.to_json())
	exit()

uti_3 = Uti(rxlists[0])
uti_3.filledStatus = "Lleno"
uti_3.transshipmentCall = "CCC/0001"
uti_3.circuit = UtiCircuit("TRANSBORDO_MISMA")
uti_3.plate = "DESU0000003"
uti_3.coarriNumber = None
result = slcd_api.send_uti_to_voyage(uti_3)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti.to_json())
	exit()

uti_4 = Uti(rxlists[0])
uti_4.filledStatus = "Lleno"
uti_4.transshipmentCall = "CCC/0001"
uti_4.circuit = UtiCircuit("TRANSBORDO_MISMA")
uti_4.plate = "DESU0000004"
uti_4.coarriNumber = 1
result = slcd_api.send_uti_to_voyage(uti_4)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti.to_json())
	exit()


print("====================")
terminal_username = "ttiaAdmin"
print("Conecto como " + terminal_username)
result = slcd_api.send_user(username = terminal_username)
if not result.ok:
	print("No he podido conectarme como " + username)
	exit()

print("Conecto a Teleport")
tp_api = ConsumeTP_APIRest()
tp_api.send_user()


delay = 10
find_eta = (datetime(2020, 3, 15, 0, 0, 0) + timedelta(days=delay)).strftime("%Y%m%d%H00")
print("Busco estancia dentro de " + str(delay) + " dias: " + find_eta)
stops = tp_api.get_stops_from_ETA(find_eta)

while(len(stops) == 0):
	if delay > 20:
		exit()
	print("No encuentro estancia en " + find_eta )
	delay += 1
	find_eta = (datetime.now() + timedelta(days=delay)).strftime("%Y%m%d%H00")
	print("Pruebo en " + find_eta )
	stops = tp_api.get_stops_from_ETA(find_eta)

stop = stops[0]
eta_stop = datetime.strptime(stop["stopEta"][:len(stop["stopEta"])-5],DATETIME_FORMAT) + timedelta(minutes = 125)
voyage_number = "BBB/0001"
print("Creo viaje " + voyage_number + " ETA: " + eta_stop.strftime(DATETIME_FORMAT))
new_voyage = Voyage(imo = stop["shipImo"], number = voyage_number, eta = eta_stop.strftime(DATETIME_FORMAT))
slcd_api.create_voyage(new_voyage)


print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)
print("Obtengo " + str(len(rxlists)) + " listas:")

for rxlist in rxlists:
	utilists = slcd_api.get_utis_from_voyage(rxlist.id)
	print("Lista Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
	if rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Carga":
		carga_parent_list = rxlist
	elif rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Descarga":
		descarga_parent_list = rxlist


co_username = "coscoAdmin_fpp"
print("====================")
print("Conecto como " + co_username)
slcd_api.send_user(username = co_username)

print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)
voyages_list = slcd_api.get_voyages_from_number(voyage_number)

if len(rxlists) == 0:

	print("No hay lista simples, creo lista simple de Carga:")
	newlist = List(voyage = voyages_list[0], circuit="Carga", parent_list = carga_parent_list)
	slcd_api.create_list(newlist)

	print("Y pido de nuevo las listas del viaje." + voyage_number)
	rxlists = slcd_api.get_lists_from_voyage(voyage_number)
	if len(rxlists) == 0:
		print("Algo ha ido mal creando listas simples ...")
		exit()

print("Añado 2 UTIs, la 1 SIN COARRI y la 2 CON COARRI...")

uti_1.listData = rxlists[0]
uti_1.transshipmentCall = "AAA/0001"
result = slcd_api.send_uti_to_voyage(uti_1)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti_1.to_json())
	exit()

uti_2.listData = rxlists[0]
uti_2.transshipmentCall = "AAA/0001"
result = slcd_api.send_uti_to_voyage(uti_2)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti_1.to_json())
	exit()


print("====================")
terminal_username = "ttiaAdmin"
print("Conecto como " + terminal_username)
result = slcd_api.send_user(username = terminal_username)
if not result.ok:
	print("No he podido conectarme como " + username)
	exit()

delay = 15
find_eta = (datetime(2020, 3, 15, 0, 0, 0) + timedelta(days=delay)).strftime("%Y%m%d%H00")
print("Busco estancia dentro de " + str(delay) + " dias: " + find_eta)
stops = tp_api.get_stops_from_ETA(find_eta)

while(len(stops) == 0):
	if delay > 25:
		exit()
	print("No encuentro estancia en " + find_eta )
	delay += 1
	find_eta = (datetime.now() + timedelta(days=delay)).strftime("%Y%m%d%H00")
	print("Pruebo en " + find_eta )
	stops = tp_api.get_stops_from_ETA(find_eta)

stop = stops[0]
eta_stop = datetime.strptime(stop["stopEta"][:len(stop["stopEta"])-5],DATETIME_FORMAT) + timedelta(minutes = 125)
voyage_number = "CCC/0001"
print("Creo viaje " + voyage_number + " ETA: " + eta_stop.strftime(DATETIME_FORMAT))
new_voyage = Voyage(imo = stop["shipImo"], number = voyage_number, eta = eta_stop.strftime(DATETIME_FORMAT))
slcd_api.create_voyage(new_voyage)


print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)
print("Obtengo " + str(len(rxlists)) + " listas:")

for rxlist in rxlists:
	utilists = slcd_api.get_utis_from_voyage(rxlist.id)
	print("Lista Tipo: " + rxlist.type.code + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
	if rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Carga":
		carga_parent_list = rxlist
	elif rxlist.type.code == "CONSOLIDADA" and rxlist.circuit == "Descarga":
		descarga_parent_list = rxlist


co_username = "coscoAdmin_fpp"
print("====================")
print("Conecto como " + co_username)
slcd_api.send_user(username = co_username)

print("Pido las listas del viaje " + voyage_number)
rxlists = slcd_api.get_lists_from_voyage(voyage_number)
voyages_list = slcd_api.get_voyages_from_number(voyage_number)

if len(rxlists) == 0:

	print("No hay lista simples, creo lista simple de Carga:")
	newlist = List(voyage = voyages_list[0], circuit="Carga", parent_list = carga_parent_list)
	slcd_api.create_list(newlist)

	print("Y pido de nuevo las listas del viaje." + voyage_number)
	rxlists = slcd_api.get_lists_from_voyage(voyage_number)
	if len(rxlists) == 0:
		print("Algo ha ido mal creando listas simples ...")
		exit()

print("Añado 2 UTIs, la 3 SIN COARRI y la 4 CON COARRI...")

uti_3.listData = rxlists[0]
uti_3.transshipmentCall = "AAA/0001"
result = slcd_api.send_uti_to_voyage(uti_3)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti_3.to_json())
	exit()

uti_4.listData = rxlists[0]
uti_4.transshipmentCall = "AAA/0001"
result = slcd_api.send_uti_to_voyage(uti_4)
if not result.ok:
	print("Algo ha ido mal creando una UTI ...")
	print(uti_4.to_json())
	exit()

exit()





