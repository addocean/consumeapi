# -*- coding: utf-8
import sys
import os

filenames = os.listdir(".")

for filename in filenames:

	#print("Analizando " + filename)

	vn=""
	cs=""
	eta=""

	if "17" in filename:

		file = open(filename,"r")
		lines = file.readlines()

		for line in lines:
			pos1 = line.find("<vesselCallSign>")
			if pos1 >= 0:
				pos2 = line.find("</vesselCallSign>")
				if pos2 > 0:
					cs = line[pos1+len("<vesselCallSign>"):pos2]

		for line in lines:
			pos1 = line.find("<voyageNumber>")
			if pos1 >= 0:
				pos2 = line.find("</voyageNumber>")
				if pos2 > 0:
					vn = line[pos1+len("<voyageNumber>"):pos2]

		for line in lines:
			pos1 = line.find("<arrivalDate>")
			if pos1 >= 0:
				pos2 = line.find("</arrivalDate>")
				if pos2 > 0:
					eta = line[pos1+len("<arrivalDate>"):pos2]

		print(vn + "," + cs + "," + eta)