# -*- coding: utf-8
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti

import requests
import sys
import random

test_name = raw_input("Escribe un nombre para la prueba: ")

total_voyages = 0
while (total_voyages == 0):
    try:
        total_voyages = int(raw_input("Cuantos viajes quieres generar? "))
    except ValueError:
        print ("Por favor, introduce un numero entero.")

total_lists = 0
while (total_lists == 0):
    try:
        total_lists = int(raw_input("Cuantas listas por viaje quieres generar? "))
    except ValueError:
        print ("Por favor, introduce un numero entero.")


total_utis = 0
while (total_utis == 0):
    try:
        total_utis = int(raw_input("Cuantas utis por lista quieres generar? "))
    except ValueError:
        print ("Por favor, introduce un numero entero.")

voyages_counter = 0
voyages_list = list()

while voyages_counter < total_voyages:
	voyages_list.append(Voyage(id = voyages_counter))
	voyages_counter += 1

lists_counter = 0
lists_list = list()

for voyage in voyages_list:
    lists_counter = 0
    while lists_counter < total_lists:
		lists_list.append(List(id = lists_counter + voyage.id * total_lists,voyage = voyage))
		lists_counter += 1

plate_number = 1000
utis_counter = 0

for eachlist in lists_list:
	utis_counter = 0

	while utis_counter < total_utis:
		new_uti = Uti(listData_id=eachlist.id, plate = str(plate_number))
		new_uti.consolidator = eachlist.consolidator

		if eachlist.circuit == "LOADING":
			new_uti.circuit = "EXPORT"
		elif eachlist.circuit == "UNLOADING":
			new_uti.circuit = "IMPORT"
		else:
			print "Tipo de lista desconocido."

		eachlist.utis.append(new_uti)
		utis_counter += 1
		plate_number += 1

print "Generadas {} listas:".format(len(lists_list))

for alist in lists_list:
	print "Lista {} del viaje {} con {} utis.".format(alist.id, alist.voyage.number, len(alist.utis))

	file_name = test_name + "_lcd{0:03d}.json".format(alist.id)
	file = open(file_name,"w")
	file.write(alist.to_json())
	file.close()



