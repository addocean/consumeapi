from datetime import datetime, timedelta
from consumeTP_APIRest import ConsumeTP_APIRest

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"

valid_status_list = ["ACEPTADO","AUTORIZADO","INICIADO","FACTURANDO","FACTURADO"]

'''
NAME;CODE
CERNAVAL DIQUE FLOTANTE;CENVAL
CERNAVAL DIQUE SECO;CENVAS
DIQUE ISLA VERDE EXTERIOR;DIQIVE
DIQUE NORTE;DIQNOR
TARIFA DIQUES M. RIBERA;DIQRIB
DIQUE SUR;DIQSUD
TARIFA DIQUE SAGRADO CORAZON;SAGCOR
TARIFA ATRAQUE 1;TARIF1
ACERINOX;ACERNX
ADOSADO RORO;ADOROR
CAMPAMENTO EXTERIOR;CAMPEX
CAMPAMENTO NORTE;CAMPNO
CAMPAMENTO SUR;CAMPSU
DARSENA PESQUERA;DARPES
MUELLE DE ESPERA SALADILLO;ESPESL
PRINCIPE FELIPE;FELIPE
GALERA ATRAQUE 8;GALER8
GALERA ATRAQUE 9;GALER9
GALERA ATRAQUE 7A;GALE7A
GALERA ATRAQUE 7B;GALE7B
ISLA VERDE;ISLVER
JUAN CARLOS I ESTE;JCIEST
JUAN CARLOS I NORTE;JCINOT
JUAN CARLOS I NORTE RO-RO;JCINRO
JUAN CARLOS I OESTE;JCIOES
JUAN CARLOS I SUR RO-RO;JCISRO
JUAN CARLOS I SUR;JCISUR
LA LINEA;LALINE
MONOBOYA;MNBOYA
MUELLE NORTE RORO;MNORRO
MUELLE NORTE;MNORTE
MUELLE ESTE;MUESTE
TARIFA ATRAQUE 2;TARIF2
TARIFA ATRAQUE 3;TARIF3
TARIFA DARSENA PESQUERA;TARPEQ
TARIFA VARADERO;TARVAR
TRAFICO COMBUSTIBLE;TRACOM
VARADERO ISLA VERDE;VARISL
VOPAK ADOSADO 1;VOPKA1
VOPAK ADOSADO 2;VOPKA2
VOPAK ADOSADO 3;VOPKA3
CEPSA - REFINERIA ATRAQUE A;CEPSAA
CEPSA - REFINERIA ATRAQUE B;CEPSAB
CEPSA - REFINERIA ATRAQUE C;CEPSAC
CEPSA - REFINERIA ATRAQUE D;CEPSAD
CEPSA - REFINERIA ATRAQUE E;CEPSAE
CEPSA - REFINERIA ATRAQUE F;CEPSAF
CEPSA - REFINERIA ATRAQUE G;CEPSAG
CEPSA - REFINERIA ATRAQUE H;CEPSAH
CEPSA - REFINERIA ATRAQUE I;CEPSAI
CEPSA - SIN BONIFICACION CONCESIONAL;CEPSIN
ENDESA PUERTOS CARGA;ENDECA
ENDESA PUERTOS DESCARGA;ENDEDE
ENDESA PUERTOS TRASDOS;ENDETS
GALERA ATRAQUE 1;GALER1
GALERA ATRAQUE 2;GALER2
GALERA ATRAQUE 3;GALER3
GALERA ATRAQUE 4;GALER4
GALERA ATRAQUE 5;GALER5
GALERA ATRAQUE 6;GALER6
PUENTE MAYORGA;PUEMAY
PANTALAN VOPAK NORTE;VOPKNO
PANTALAN VOPAK SUR;VOPKSU
CAMPAMENTO EXTERIOR NORTE;CAPEXN
DIQUE FLOTANTE CERNAVAL;DFLOTC
'''


ttia_locations = ['MNORTE', 'MUESTE']
apmt_locations = ['JCIEST', 'JCINOR', 'JCISUR']
anch_locations = ['FONDOA','FONDOB']

while True:
   try:
       fecha_str = input('\nIntroducir fecha (ejemplo "18/01/1952", se analizarán 15 dias previos y 30 posteriores): ')
       fecha = datetime.strptime(fecha_str, '%d/%m/%Y')
       break
   except:
       print("\n No ha ingresado una fecha correcta...")

print("Fecha: ", fecha)

print("Conecto a Teleport")
tp_api = ConsumeTP_APIRest()
tp_api.send_user()

stops_list = list()

# Saltos de 12h para consultar
for delta in range(-30,60):

	datetime_eta = fecha + timedelta(hours=delta*12)
	if delta % 2 == 0:
		find_eta = datetime_eta.strftime("%Y%m%d1200")
	else:
		find_eta = datetime_eta.strftime("%Y%m%d0000")
	stops = tp_api.get_stops_from_ETA(find_eta)
	print("\tFind ETA:" + find_eta)

	for stop in stops:

		eta_stop = datetime.strptime(stop["stopEta"],DATETIME_FORMAT) + timedelta(minutes = 5)
		if eta_stop > datetime.now().astimezone(None) + timedelta(days=-15):

			include = 0

			# Compruebo si tiene un estado válido
			for valid_status in valid_status_list:
				if stop["visitStatusId"] == valid_status:

					include = 1

					# Si no la tengo ya, la añado
					for included_stop in stops_list:
						if stop["id"] == included_stop["id"]:
							include = 0

					if include:
						stops_list.append(stop)

print("Muestra:")
print(stops_list[0])

print("\n=================")
print("Analizando " + str(len(stops_list)) + " estancias encontradas.")
for stop in stops_list:
	print(str(stop["id"]) + " " +
		stop["visitNumber"] + " " +
		stop["visitStatusId"].ljust(12) + " " +
		stop["stopEta"][0:19] + " " +
		stop["shipImo"] + " " +
		stop["locationCode"] + " " +
		stop["agentName"] + "...")

print("\n=================")
print("De APMT:")
contador_APMT = 0
for stop in stops_list:
	for location in apmt_locations:
		if stop["locationCode"] == location:
			print(str(stop["id"]) + " " +
				stop["visitNumber"] + " " +
				stop["visitStatusId"].ljust(12) + " " +
				stop["stopEta"][0:19] + " " +
				stop["shipImo"] + " " +
				stop["locationCode"] + " " +
				stop["agentName"] + "...")
			contador_APMT += 1
print(str(contador_APMT) + " estancias de APMT")

print("\n=================")
print("De TTIA:")
contador_TTIA = 0
for stop in stops_list:
	for location in ttia_locations:
		if stop["locationCode"] == location:
			print(str(stop["id"]) + " " +
				stop["visitNumber"] + " " +
				stop["visitStatusId"].ljust(12) + " " +
				stop["stopEta"][0:19] + " " +
				stop["shipImo"] + " " +
				stop["locationCode"] + " " +
				stop["agentName"] + "...")
			contador_APMT += 1
print(str(contador_TTIA) + " estancias de TTIA")

print("\n=================")
print("Fondeos:")
contador_fondeos = 0
for stop in stops_list:
	for location in anch_locations:
		if stop["locationCode"] == location:
			print(str(stop["id"]) + " " +
				stop["visitNumber"] + " " +
				stop["visitStatusId"].ljust(12) + " " +
				stop["stopEta"][0:19] + " " +
				stop["shipImo"] + " " +
				stop["locationCode"] + " " +
				stop["agentName"] + "...")
			contador_fondeos += 1
print(str(contador_fondeos) + " fondeos")

print("\n=================")
print(str(contador_APMT + contador_TTIA + contador_fondeos) + " identificadas de un total de " + str(len(stops_list)))
print("")
#print(stops_list[0])

