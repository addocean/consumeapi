def execute_result(result, message):
    if not result:
        print("ERROR: " + message)
        exit(-1)


def assert_starts_with(actual_string, expected_start_string):
    result = actual_string[:len(expected_start_string)] == expected_start_string
    execute_result(result, ("assert starts with failed. Expected = " + expected_start_string + "... Received = " + actual_string))


def assert_bigger_than(actual, expected):
    result = actual > expected
    execute_result(result, ("assert bigger than failed. Expected > " + str(expected) + "... Received = " + str(actual)))


def assert_equals_than(actual, expected):
    result = actual == expected
    execute_result(result, ("assert equals failed. Expected = " + str(expected) + "... Received = " + str(actual)))


def assert_none_object(actual_voyage):
    result = actual_voyage is None
    execute_result(result, ("assert none object failed. Expected = None... Received = " + str(type(actual_voyage).__name__)))