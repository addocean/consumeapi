from consumeTP_APIRest import ConsumeTP_APIRest
from consumeAPIRest import ConsumeAPIRest
from datetime import datetime
import pickle
import os
import json

URL = "http://pruebas.teleport.es/slcd/"


class Stop:
    def __init__(self):
        self.vessel_name = None
        self.vessel_imo = None
        self.vessel_callsign = None
        self.stop_eta = None
        self.stop_ata = None
        self.stop_etd = None
        self.stop_atd = None
        self.visit_eta = None
        self.visit_ata = None
        self.visit_etd = None
        self.visit_atd = None
        self.visit_number = None
        self.location = None
        self.status = None
        self.stop_status = None

    def create_from_json(self, json_):
        self.vessel_name = json_["shipName"]
        self.vessel_imo = json_["shipImo"]
        self.vessel_callsign = json_["callSign"]
        self.stop_eta = json_["stopEta"]
        self.stop_ata = json_["stopAta"]
        self.stop_etd = json_["stopEtd"]
        self.stop_atd = json_["stopAtd"]
        self.visit_eta = json_["visitEta"]
        self.visit_ata = json_["visitAta"]
        self.visit_etd = json_["visitEtd"]
        self.visit_atd = json_["visitAtd"]
        self.visit_number = json_["visitNumber"]
        self.location = json_["locationCode"]
        self.status = json_["visitStatusId"]
        self.stop_status = json_["stopStatus"]

    def print(self):
        return self.visit_number + "(" + self.status + " - " + self.stop_status + ") - " + self.vessel_name + "(" + self.vessel_imo + ")"

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class CheckVisits:
    def __init__(self):
        self.tp_api = ConsumeTP_APIRest()
        self.tp_api.send_user()
        self.api = ConsumeAPIRest(URL)
        self.old_stops = self.initialize_stops_map()
        self.new_stops = self.get_new_stops()
        special_visits = []
        self.new_stops.extend(self.process_special_visits(special_visits))

    def get_new_stops(self):
        eta = datetime.now()
        stops_from_teleport = self.tp_api.get_stops_from_ETA(eta.strftime("%Y%m%d%H%M"))
        return self.process_stops_from_teleport(stops_from_teleport)

    def process_special_visits(self, special_visits):
        stops_from_teleport_list = list()
        for visit_number in special_visits:
            stops_from_teleport = self.tp_api.get_stops_from_Number(visit_number)
            stops_from_teleport_list.extend(self.process_stops_from_teleport(stops_from_teleport))
        return stops_from_teleport_list

    @staticmethod
    def process_stops_from_teleport(stops_from_teleport):
        new_stops = list()
        valid_locations = ["MUESTE", "MNORTE"]  # , "JCIEST", "JCINOT", "JCISUR"]
        for json_stop in stops_from_teleport:
            location_code = json_stop["locationCode"]
            if location_code in valid_locations:
                stop = Stop()
                stop.create_from_json(json_stop)
                new_stops.append(stop)
        return new_stops

    def persist_stops_obtained(self):
        with open('teleport_stops_obtained.pickle', 'wb') as token:
            pickle.dump(self.new_stops, token)

    @staticmethod
    def initialize_stops_map():
        if os.path.exists('teleport_stops_obtained.pickle'):
            with open('teleport_stops_obtained.pickle', 'rb') as token:
                stops_obtained = pickle.load(token)
                print("Stops readed = " + str(len(stops_obtained)))
        else:
            stops_obtained = list()
            print("New Stops list")
        return stops_obtained

    def compare_stops(self):
        for stop in self.new_stops:
            old_stop = next((old_stop for old_stop in self.old_stops if stop.visit_number == old_stop.visit_number), None)
            if old_stop is not None:
                self.old_stops.remove(old_stop)
                self.compare_stop(stop, old_stop)
            else:
                print("New visit: " + stop.print())

        for stop in self.old_stops:
            print("Stop not present: " + stop.print())
            if stop.stop_atd is not None:
                print("Stop with ATD. Removing it: " + stop.print())
            else:
                self.new_stops.append(stop)

        self.persist_stops_obtained()

    @staticmethod
    def compare_stop(stop, old_stop):
        if not stop.to_json() == old_stop.to_json():
            print("Stop Modified: " + stop.print())

    def list_working(self):
        errors = list()
        self.api.send_user("ttiaAdmin")
        for stop in self.new_stops:
            if stop.stop_ata is not None and stop.stop_atd is None:
                print("Stop: " + stop.print() + " working")
                voyages = self.api.get_voyages_from_visit_number(stop.visit_number)
                if len(voyages) == 0:
                    errors.append("ERROR: No hay viajes para la escala " + stop.print())
                else:
                    lists = self.get_lists_from_voyages(voyages)
                    consolidated_closed = 0
                    for actual_list in lists:
                        if actual_list.type == "Consolidada" and actual_list.consolidationState == "CERRADA":
                            consolidated_closed += 1
                    if consolidated_closed != 2:
                        errors.append("Error. No existen dos consolidadas cerradas para el viaje " + stop.print())

        print("-----------")
        for error in errors:
            print(error)

    def get_lists_from_voyages(self, voyages):
        lists = list()
        for voyage in voyages:
            lists = self.api.get_lists_from_voyage(voyage.number, False)
        return lists


check_visits = CheckVisits()
check_visits.compare_stops()
check_visits.list_working()
