import sys
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from consumeAPIRest import ConsumeAPIRest
from consumeTP_APIRest import ConsumeTP_APIRest

delete_all = 0

if len(sys.argv) == 3:
	rmt_plate = sys.argv[1]
	rmt_booking = sys.argv[2]
elif len(sys.argv) == 1:
	delete_all = input("Confirmas que quieres borrar todas las RMT? ")
	if delete_all != 's':
		print("Descarto borrado ...")
		exit()
else:
	print("Argumentos incorrectos. Uso python3 test_deleteRMT.py [rmt_plate rmt_booking]")
	exit()

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")

terminal_username = "addocean"
print("Conecto a SLCD como " + terminal_username)
slcd_api.send_user(username = terminal_username)

# Borrado de todas las RMT
if delete_all == 's':

	first_rmt_id = int(input("Introduce el primer id de RMT: "))
	last_rmt_id = int(input("Introduce el último id de RMT: "))

	for counter in range(first_rmt_id,last_rmt_id):

		print("\nBuscando RMT " + str(counter))
		rmt = slcd_api.get_rmt_from_id(counter)

		if rmt is None:
			print("No encuentro esta RMT.")
		else:
			slcd_api.delete_rmt(rmt)

# Borrado de una rmt por su matrícula y BN
else:

	print("\nBuscando RMT " + rmt_plate + " " + rmt_booking)
	rmts = slcd_api.find_rmt(rmt_plate,rmt_booking)

	if len(rmts) is 0:
		print("No encuentro esta RMT.")
	else:
		slcd_api.delete_rmt(rmts[0])
