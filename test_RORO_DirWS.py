# -*- coding: utf-8
from suds.client import Client
import logging
import os

logging.basicConfig(level=logging.INFO)
# logging.getLogger('suds.client').setLevel(logging.DEBUG)


class ConsumeROROWS:
    def __init__(self):
        url = 'file:///Users/franchofelez/Code/ListaCargaDescarga/consumeAPIRest/listaCarga.wsdl'
        self.client = Client(url)

    def send_message(self):
        print(self.client)

        with os.scandir('ROROMsgs/') as entries:
            for entry in entries:
                if entry.name == "PruebaROROXML error.xml":
                    print(entry.name)
                    with open(entry, 'r') as f:
                        data = f.read()
                        result = self.client.service.getListaCarga("frsAdmin", "$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq", data)
                        if result is None:
                            print("\tOK")
                        else:
                            print("\t" + result)

        print(result)


ws = ConsumeROROWS()
ws.send_message()