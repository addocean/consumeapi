import sys
from consumeAPIRest import ConsumeAPIRest

delete_all = 0

def delete_list(list_to_delete):

	utis = slcd_api.get_utis_from_voyage(list_to_delete.id)
	for uti in utis:

		if uti.booking is None:
			uti.booking = ""
		print("Borrando UTI: " + uti.plate + " " + uti.booking)

		rmts = slcd_api.find_rmt(uti.plate, uti.booking)
		for any_rmt in rmts:
			if any_rmt.voyageNumber == list_to_delete.voyageNumber:
				slcd_api.delete_rmt(any_rmt)

		slcd_api.delete_uti(uti)

	slcd_api.delete_list(anylist)


if len(sys.argv) == 2:
	voyage_number = sys.argv[1]
elif len(sys.argv) == 1:
	delete_all = input("Confirmas que quieres borrar todos los viajes?")
	if delete_all != 's':
		print("Descarto borrado ...")
		exit()
else:
	print("Argumentos incorrectos. Uso python3 test_deletevoyage.py [voyage_number]")
	exit()

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")

# Borrado de todos los viajes
if delete_all == 's':

	for counter in range(0,200):

		print("\nBuscando viaje " + str(counter))

		terminal_username = "ttiaAdmin"
		print("Conecto a SLCD como " + terminal_username)
		slcd_api.send_user(username = terminal_username)

		voyage = slcd_api.get_voyage_from_id(counter)

		if voyage is None:
			terminal_username = "apmtAdmin"
			print("Conecto a SLCD como " + terminal_username)
			slcd_api.send_user(username = terminal_username)

			voyage = slcd_api.get_voyage_from_id(counter)

		if voyage is None:
			print("No encuentro este viaje.")

		if voyage is not None and "SV/" in voyage.number:

			print("Borrando viaje " + voyage.number + " como " + terminal_username)

			username = "coscoAdmin"
			print("Conecto a SLCD como " + username)
			slcd_api.send_user(username = username)

			lists = slcd_api.get_lists_from_voyage(voyage.number)
			for anylist in lists:
				delete_list(anylist)

			username = "cmaAdmin"
			print("Conecto a SLCD como " + username)
			slcd_api.send_user(username = username)

			lists = slcd_api.get_lists_from_voyage(voyage.number)
			for anylist in lists:
				delete_list(anylist)

			username = "mscAdmin"
			print("Conecto a SLCD como " + username)
			slcd_api.send_user(username = username)

			lists = slcd_api.get_lists_from_voyage(voyage.number)
			for anylist in lists:
				delete_list(anylist)

			print("Conecto a SLCD como " + terminal_username)
			slcd_api.send_user(username = terminal_username)

			lists = slcd_api.get_lists_from_voyage(voyage.number)
			for anylist in lists:
				slcd_api.delete_list(anylist)

			slcd_api.delete_voyage(voyage)

	exit() 

# Borrado de un viaje por su número
else:

	terminal_username = "ttiaAdmin"
	print("Conecto a SLCD como " + terminal_username)
	slcd_api.send_user(username = terminal_username)

	voyages = slcd_api.get_voyages_from_number(voyage_number)

	if len(voyages) == 0:
		terminal_username = "apmtAdmin"
		print("Conecto a SLCD como " + terminal_username)
		slcd_api.send_user(username = terminal_username)

		voyages = slcd_api.get_voyages_from_number(voyage_number)

	if len(voyages) == 0:
		print("No encuentro en viaje.")
		exit()

	cousers_list = ["coscoUser2","cmaUser2","mscUser2","maerskUser2"]

	for username in cousers_list:
		print("Conecto a SLCD como " + username)
		slcd_api.send_user(username = username)

		lists = slcd_api.get_lists_from_voyage(voyage_number)
		for anylist in lists:
			delete_list(anylist)

	print("Conecto a SLCD como " + terminal_username)
	slcd_api.send_user(username = terminal_username)

	lists = slcd_api.get_lists_from_voyage(voyage_number)
	for anylist in lists:
		delete_list(anylist)

	slcd_api.delete_voyage(voyages[0])






