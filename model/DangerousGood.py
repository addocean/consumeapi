import json
import random
import string

packing_types = ["I", "II", "III"]
dg_imo_codes = ["6.1","5.2","6.4","2.7B","3.4C","1.2A"]

class DangerousGood:
    def __init__(self):
        self.id = 0
        self.number = "{0:04d}".format(random.randrange(1,9999))
        self.description = "This DG description"
        self.type = random.choice(dg_imo_codes)
        self.packingType = random.choice(packing_types)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        ddgg = list()
        for json_element in json_data:
            dg = DangerousGood()
            dg.id = json_element["id"]
            dg.number = json_element["number"]
            dg.type = json_element["type"]
            dg.type = DangerousGoodPackingType.create_from_json(json_element["packingType"])
            ddgg.append(dg)
        return ddgg
