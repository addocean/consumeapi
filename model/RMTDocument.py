import json

from model.Company import Company


class RMTDocument:
    def __init__(self):
        self.id = None
        self.reference = ""
        self.dispatchStatus = ""
        self.type = ""
        self.partialPackageQty = 0
        self.partialGoodsWeight = 0
        self.csv = None

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        rmt_document = RMTDocument()
        #rmt_document.id = json_data["id"]
        rmt_document.reference = json_data["reference"]
        rmt_document.type = json_data["type"]
        rmt_document.partialPackageQty = json_data["partialPackageQty"]
        rmt_document.partialGoodsWeight = json_data["partialGoodsWeight"]
        rmt_document.dispatchStatus = json_data["dispatchStatus"]
        rmt_document.csv = json_data["csv"]
        return rmt_document

    def print(self):
        #print("\t\t\tID:           " + str(self.id))
        print("\t\t\tREFERENCE:    " + str(self.reference))
        print("\t\t\tDOC. TYPE:    " + str(self.type))
        print("\t\t\tDOC. STATUS:  " + str(self.dispatchStatus))
        print("\t\t\tTOTAL P. QTY: " + str(self.partialPackageQty))
        print("\t\t\tTOTAL WEIGHT: " + str(self.partialGoodsWeight))
        print("\t\t\tCSV:          " + str(self.csv))

