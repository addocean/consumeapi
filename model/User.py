import json


class User:
    def __init__(self, name, password):
        self.username = name
        self.password = password

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
