import json

from model.Voyage import Voyage

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class List:
    def __init__(self):
        self.id = 0
        self.opVoyageNumber = ""
        self.voyageNumber = ""
        self.circuit = "Carga"
        self.consolidator = ""
        self.type = "Simple"
        self.closingDateTime = None

        self.consolidationState = ""
        self.reportStatus = ""
        self.state = ""

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        list_ = List()
        list_.id = json_data["id"]
        list_.opVoyageNumber = json_data["opVoyageNumber"]
        list_.voyageNumber = json_data["voyageNumber"]
        list_.circuit = json_data["circuit"]
        list_.consolidator = json_data["consolidator"]
        list_.closingDateTime = json_data["closingDateTime"]
        list_.type = json_data["type"]

        list_.consolidationState = json_data["consolidationState"]
        list_.reportStatus = json_data["reportStatus"]
        list_.state = json_data["state"]

        return list_

    def print(self):
        print("\t\tID: " + str(self.id))
        print("\t\tcircuit: " + str(self.circuit))
        print("\t\ttype: " + str(self.type))
