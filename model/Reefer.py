import json
import random
import string


class Reefer:
    def __init__(self):
        self.id = 0

        self.active = random.choice([True,False])

        if self.active is True:

            self.instructions = "Instrucciones en texto libre"
            self.dieselGenerator = random.choice([True,False])

            self.atmosphereControl = random.choice([True,False])

            if self.atmosphereControl is True:
                self.oxygenLevel = random.randrange(0,10000)/100
                self.nitrogenLevel = random.randrange(0,10000)/100
                self.carbonDioxideLevel = random.randrange(0,10000)/100
            else:
                self.oxygenLevel = None
                self.nitrogenLevel = None
                self.carbonDioxideLevel = None

            self.terminalConnected = random.choice([True,False])
            self.shipConnected = random.choice([True,False])

            self.humidifier = random.choice([True, False])

            if self.humidifier is True:
                self.relAirHumidity = random.randrange(0, 10000) / 100
            else:
                self.relAirHumidity = None

            if random.choice([True, False]) is True:
                self.airFlow = random.randrange(0, 10000) / 100
            else:
                self.airFlow = None

            if self.airFlow is not None and self.airFlow > 0:
                self.ventGrillesState = "Abierta"
                self.ventGrillesOpening = random.randrange(0, 10000) / 100
            else:
                self.ventGrillesState = "Cerrada"
                self.ventGrillesOpening = None

            if random.choice([True, False]) is True:
                self.transportTemperature = random.randrange(0, 10000) / 100
                self.minTemperature = None
                self.maxTemperature = None
            else:
                self.transportTemperature = None
                self.minTemperature = -4
                self.maxTemperature = 12

        else:
            self.terminalConnected = None
            self.shipConnected = None
            self.dieselGenerator = None
            self.humidifier = False
            self.atmosphereControl = False
            self.transportTemperature = None
            self.instructions = None
            self.oxygenLevel = None
            self.nitrogenLevel = None
            self.carbonDioxideLevel = None
            self.airFlow = None
            self.ventGrillesState = None


    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        reefer = None
        if json_data is not None:
            reefer = Reefer()
            reefer.id = json_data["id"]
            reefer.active = json_data["active"]

        return reefer
