import json
import random
import string

class XmlData:
    def __init__(self, id, username, xml):
        self.id = id
        self.userData = username
        self.xml = xml

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        xmldata = XmlData()
        xmldata.id = json_element["id"]
        xmldata.userData = json_element["userData"]
        xmldata.xml = json_element["xml"]
        return xmldata
