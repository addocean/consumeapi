import json


class Company:
    def __init__(self, id, name, description, code):
        self.id = id
        self.name = name
        self.description = description
        self.version = 0
        self.code = code

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        company = Company(json_data["id"], json_data["name"], json_data["description"], json_data["code"])
        return company