import json

from model.Company import Company

class Voyage:
    def __init__(self, imo, number, eta):
        self.id = ""
        self.shipImo = imo
        self.number = number
        self.dateTimeETA = eta
        self.type = ""
        self.initOpsDateTime = ""
        self.endOpsDateTime = ""
        self.consolidator = "TERMINAL"
        self.shippingCompany = "ADDO"

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        voyage = Voyage(json_data["shipImo"], json_data["number"], json_data["dateTimeETA"])
        voyage.id = json_data["id"]
        voyage.type = json_data["type"]
        voyage.initOpsDateTime = json_data["initOpsDateTime"]
        voyage.endOpsDateTime = json_data["endOpsDateTime"]
        voyage.consolidator = json_data["consolidator"]
        voyage.shippingCompany = json_data["shippingCompany"]
        voyage.voyageStatus = json_data["voyageStatus"]

        return voyage

    def print(self):
        print("\t\tID: " + str(self.id))
        print("\t\tIMO: " + str(self.shipImo))
        print("\t\tETA: " + str(self.dateTimeETA))
