import json
import random
import string


class Notification:
    def __init__(self):
        self.id = 0
        self.subject = ""
        self.content = ""

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        notification = Notification()
        notification.id = json_data["id"]
        notification.subject = json_data["subject"]
        notification.content = json_data["content"]
        return notification
