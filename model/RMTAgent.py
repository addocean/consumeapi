import json

from model.RMTDocument import RMTDocument


class RMTAgent:
    def __init__(self):
        self.id = None
        self.nif = "1234567G"
        self.utiAgentStatus = "PREPARACION"
        self.customDocumentList = list()
        self.freeDocument = list()
        self.partialPackageQty = 10
        self.partialGoodsWeight = 100.0

        document = RMTDocument()
        document.reference = "20ES00111110001021"
        self.customDocumentList.append(document)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        rmt_agent = RMTAgent()
        #rmt_agent.id = json_data["id"]
        rmt_agent.nif = json_data["nif"]
        #rmt_agent.utiAgentStatus = json_data["utiAgentStatus"]
        rmt_agent.partialPackageQty = json_data["partialPackageQty"]
        rmt_agent.partialGoodsWeight = json_data["partialGoodsWeight"]
        rmt_agent.customDocumentList = list()
        for document_json in json_data["customDocumentList"]:
            document = RMTDocument.create_from_json(document_json)
            rmt_agent.customDocumentList.append(document)

        return rmt_agent

    def print(self):
        #print("\t\tID:           " + str(self.id))
        print("\t\tNIF:          " + str(self.nif))
        #print("\t\tAGENT STATUS: " + str(self.utiAgentStatus))
        print("\t\tTOTAL P. QTY: " + str(self.partialPackageQty))
        print("\t\tTOTAL WEIGHT: " + str(self.partialGoodsWeight))

        print("\t\tDOCUMENTS:    " + str(len(self.customDocumentList)))
        for document in self.customDocumentList:
            document.print()
