# -*- coding: utf-8
import json
import random
import string
from model.List import List
from model.Reefer import Reefer
from model.Seal import Seal
from model.DangerousGood import DangerousGood

length_codes = "1234L"
wh_codes = "0123456789CDEFLMNP"
group_codes = ["G0","G1","G2","G3","V0","V2","V4","R0","R1","R2","R3","H0","H1","H2","H5","H6",
    "U1","U2","U3","U4","U5","T0","T1","T2","T3","T4","T5","T6","T7","T8","T9",
    "B0","B1","B3","B4","B5","B6","P0","P1","P2","P3","P4","P5","S0","S1","S2"]
locodes = ["ESBCD","ESVLC","ESRTM","MYTPP","EGSUZ","CNTNJ"]
shipping_companies = ["CMDU","COSU","FLJF","LDYN","MAEU","MSCU","MAEU","MAEU"]


class Uti:
    def __init__(self, parent_list):
        self.id = None
        self.idLista = parent_list
        self.plate = ""
        self.type = "CN"
        self.billOfLading = "B/L" + "{0:010d}".format(random.randrange(10))
        self.booking = "BN" + "{0:010d}".format(random.randrange(10))
        self.circuit = "EXPORTACION"
        self.coarriNumber = None
        self.destinationPort = random.choice(locodes)
        self.dischargePort = random.choice(locodes)
        self.levanteStatus = "DESCONOCIDO"
        #self.extraBackLength = 0
        #self.extraFrontHeight = 0
        #self.extraHeight = 0
        #self.extraLeftWidth = 0
        #self.extraRightWidth = 0
        self.filledStatus = random.choice(["Lleno", "Vacío"])
        #self.generalRemarks = "Observaciones"
        self.grossWeight = random.randrange(2000, 10000)
        self.iso6346Code = ""
        self.iso6346Compliance = random.choice([True, True, True, False])
        # self.length = 0.0
        self.loadingPort = random.choice(locodes)
        self.utiStatus = ""
        self.soc = False
        self.transshipmentVoyage = ""
        self.levanteStatus = ""
        self.containerOperator = "---"

    def print(self):
        print("\t\tID: " + str(self.id))
        print("\t\tplate: " + str(self.plate))
        print("\t\tcircuit: " + str(self.circuit))

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        uti = Uti(json_data["idLista"])
        uti.id = json_data["id"]
        uti.plate = json_data["plate"]
        uti.iso6346Code = json_data["iso6346Code"]
        uti.grossWeight = json_data["grossWeight"]
        uti.verifiedWeight = json_data["verifiedWeight"]
        uti.coarriNumber = json_data["coarriNumber"]
        uti.filledStatus = json_data["filledStatus"]
        uti.booking = json_data["booking"]
        uti.utiStatus = json_data["utiStatus"]
        uti.circuit = json_data["circuit"]
        uti.stowageCell = json_data["stowageCell"]
        uti.levanteStatus = json_data["levanteStatus"]
        uti.transshipmentVoyage = json_data["transshipmentVoyage"]
        uti.levanteStatus = json_data["levanteStatus"]

        uti.destinationPort = json_data["destinationPort"]
        uti.dischargePort = json_data["dischargePort"]
        uti.loadingPort = json_data["loadingPort"]
        uti.containerOperator = json_data["containerOperator"]

        return uti
