import json


class ConversionRest:
    def __init__(self, message):
        self.inputValue = message
        self.outputValue = ""
        self.conversionType = "COARRI_EDI2XML_95B"
        self.traceTarget = list()

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        conversion = ConversionRest("")
        conversion.outputValue = json_data["outputValue"]
        return conversion
