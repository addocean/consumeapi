import json

from model.RMTAgent import RMTAgent


class RMT:
    def __init__(self):
        self.id = ""
        self.plate = ""
        self.booking = ""
        self.type = "CN"
        self.rmtToListType = "LOLO"
        self.rmtStatus = "PREPARACION"
        self.totalPackageQty = 10
        self.totalGoodsWeight = 100.0
        self.voyageNumber = ""
        self.utiAgentList = list()

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        rmt = RMT()
        rmt.id = json_data["id"]
        rmt.plate = json_data["plate"]
        rmt.booking = json_data["booking"]
        rmt.type = json_data["type"]
        #rmt.rmtToListType = json_data["rmtToListType"]
        rmt.rmtStatus = json_data["rmtStatus"]
        rmt.totalPackageQty = json_data["totalPackageQty"]
        rmt.totalGoodsWeight = json_data["totalGoodsWeight"]
        rmt.voyageNumber = json_data["voyageNumber"]

        rmt.utiAgentList = list()
        for agent_json in json_data["utiAgentList"]:
            agent = RMTAgent.create_from_json(agent_json)
            rmt.utiAgentList.append(agent)

        return rmt

    def print(self):
        print("\tID:           " + str(self.id))
        print("\tPLATE:        " + str(self.plate))
        print("\tBOOKING:      " + str(self.booking))
        print("\tTYPE:         " + str(self.type))
        print("\tLIST TYPE:    " + str(self.rmtToListType))
        print("\tSTATUS:       " + str(self.rmtStatus))
        print("\tTOTAL P. QTY: " + str(self.totalPackageQty))
        print("\tTOTAL WEIGHT: " + str(self.totalGoodsWeight))
        print("\tVOYAGE NUM.:  " + str(self.voyageNumber))

        print("\tAGENTS:       " + str(len(self.utiAgentList)))
        for agent in self.utiAgentList:
            agent.print()