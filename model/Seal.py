import json
import random
import string

seal_types = ["ADUANA", "CUARENTENA", "NAVIERA", "TERMINAL", "VETERINARIO"]

class Seal:
    def __init__(self):
        self.id = 0
        self.number = random.choice(string.ascii_uppercase) + random.choice(string.ascii_uppercase) + "{0:08d}".format(random.randrange(0,99999999))
        self.type = random.choice(seal_types)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        seals = list()
        for json_element in json_data:
            seal = Seal()
            seal.id = json_element["id"]
            seal.number = json_element["number"]
            seal.type = json_element["type"]
            seals.append(seal)
        return seals
