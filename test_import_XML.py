

  from consumeAPIRest import ConsumeAPIRest
import random
from datetime import datetime, timedelta
from model.Voyage import Voyage
import asserts
from model.Xml import XmlData
import xml.etree.ElementTree as ElementTree

URL = "http://pre.addocean-pcs.es/slcd/"
# URL = "http://pruebas.teleport.es/slcd/"
# URL = "http://localhost:8090/"
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class SimpleTest:
    def __init__(self):
        self.api = ConsumeAPIRest(URL)
        self.plates = list()

    def execute(self):
        self.plates = list()
        random_number = random.randint(1, 1000000)
        voyage_number = "FFE" + str(random_number).rjust(7, '0')
        print("VoyageNumber = " + voyage_number)

        self.new_step_message("Vamos a crear un viaje", False)
        response = self.api.send_user("apmtAdmin_ffe")
        asserts.assert_starts_with(response.content.decode("utf-8"), "Bearer ")
        eta_ = (datetime.now() + timedelta(hours=30)).strftime(DATETIME_FORMAT)
        new_voyage = Voyage(imo="9839454", number=voyage_number, eta=eta_)
        new_voyage_id = self.api.create_voyage(new_voyage, True)
        asserts.assert_bigger_than(new_voyage_id, 0)

        self.new_step_message("Vamos a importar un xml", False)
        xml = self.read_from_file("coprars/coprar_carga_template.reg.xml", voyage_number)

        self.send_xml_to_slcd(xml, "coscoUser2_ffe")

    @staticmethod
    def new_step_message(message, force_input=False):
        do_input = False
        if do_input or force_input:
            input("\n-----\n" + message + " _")
        else:
            print(message)

    @staticmethod
    def read_from_file(filename, voyage_number):
        tree = ElementTree.parse(filename)
        root = tree.getroot()
        if voyage_number is not None:
            mtd_group = root.find("MaritimeTransportDetails")
            mtd_group.find("voyageNumber").text = voyage_number
        return ElementTree.tostring(root).decode("utf-8")

    def send_xml_to_slcd(self, xml, user_name):
        if len(xml) >= 25:
            print("Going to send xml: " + xml[:25] + "...")

        xml_rest = XmlData(0, user_name, xml)
        utis_created = self.api.import_xml(xml_rest, False)
        return utis_created


SimpleTest().execute()
