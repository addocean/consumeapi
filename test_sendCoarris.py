import os
import pickle
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import base64
import io
import requests
from model.ConversionRest import ConversionRest
from model.Xml import XmlData
import xml.etree.ElementTree as elementTree
from consumeAPIRest import ConsumeAPIRest
from model.Voyage import Voyage
from model.User import User
from datetime import datetime, timedelta
from consumeTP_APIRest import ConsumeTP_APIRest
from ftplib import FTP_TLS
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/drive']
CONVERSION_URL = "http://pre.addocean-pcs.es/ediConverter/api/converters/"
URLS = [["LOCAL", "http://localhost:8090/", True], ]
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
DATETIME_FTP_FORMAT = "%Y %b %d %H:%M"


def read_voyages_created_from_file():
    if os.path.exists('voyages_created.pickle'):
        with open('voyages_created.pickle', 'rb') as token:
            voyages_created = pickle.load(token)
    else:
        voyages_created = list()
    return voyages_created


def initialize_imos_map():
    if os.path.exists('imos_obtained.pickle'):
        with open('imos_obtained.pickle', 'rb') as token:
            imos_obtained = pickle.load(token)
            print("Imos readed = " + str(len(imos_obtained)))
    else:
        imos_obtained = dict()
        print("New IMOs map")
    return imos_obtained


def get_maritime_details(root):
    mtd_group = root.find("MaritimeTransportDetails")
    if mtd_group is None:
        coarri = root.find("COARRI")
        mtd_group = coarri.find("MaritimeTransportDetails")
    return mtd_group


def get_maritime_details_all(root):
    mtd_group = root.findall("MaritimeTransportDetails")
    if mtd_group is None or len(mtd_group) == 0:
        coarri = root.find("COARRI")
        mtd_group = coarri.findall("MaritimeTransportDetails")
    return mtd_group


def get_message_header(root):
    message_header_group = root.find("MessageHeader")
    if message_header_group is None:
        coarri = root.find("COARRI")
        message_header_group = coarri.find("MessageHeader")
    return message_header_group


def is_massive_coarri(xml):
    root = elementTree.fromstring(xml)
    return root.tag == "COARRIS"


class SendCoarris:
    def __init__(self, send_to_slcd, read_from_ftp):
        self.service = None
        self.drive_service = None
        self.create_service()
        self.apis_names = list()
        self.apis = list()
        self.apis_xml_new_version = list()
        for url in URLS:
            self.apis_names.append(url[0])
            self.apis.append(ConsumeAPIRest(url[1]))
            self.apis_xml_new_version.append(url[2])
        now = datetime.now()
        self.time_string = now.strftime("%Y%m%d%H%M%S")
        self.send_to_slcd = send_to_slcd
        self.ftp = FTP_TLS()
        self.edi_dir = "coarris/edis"
        self.to_read_from_ftp = read_from_ftp
        self.voyages_created = read_voyages_created_from_file()
        self.imos_obtained = initialize_imos_map()

    def delete_from_ftp(self, filename):
        self.ftp.connect('pruebas.teleport.es', 21)
        self.ftp.sendcmd('USER ttia')
        self.ftp.sendcmd('PASS vacu83sKpK6B')
        self.ftp.cwd('outbox_slcd_to_aws')
        self.ftp.delete(filename)
        self.ftp.close()
        print("deleted from ftp %s" % filename)

    def read_from_ftp(self):
        self.ftp.connect('pruebas.teleport.es', 21)
        self.ftp.sendcmd('USER ttia')
        self.ftp.sendcmd('PASS vacu83sKpK6B')
        self.ftp.cwd('outbox_slcd_to_aws')
        files = []
        self.ftp.dir(files.append)
        edi_readed = ''
        filename = None
        earlier_date = datetime.now()
        result = None, None, list()
        for file in files:
            line = file.split(' ')
            actual_filename = line[len(line)-1]
            time_string = str(datetime.now().year) + " " + line[len(line)-4] + " " + line[len(line)-3] + " " + line[len(line)-2]
            actual_date = datetime.strptime(time_string, DATETIME_FTP_FORMAT)
            if actual_date < earlier_date:
                earlier_date = actual_date
                filename = actual_filename

        if filename is not None:
            print(filename)
            filepath = "%s/%s" % (self.edi_dir, filename)
            with open(filepath, 'wb') as pcfile:
                self.ftp.retrbinary("RETR " + filename, pcfile.write)
            pcfile.close()

            with open(filepath, 'r') as pcfile:
                lines = pcfile.readlines()
                for line in lines:
                    edi_readed += edi_readed + line

            self.send_to_drive(filepath, False)

            result = "FTP:" + filename, filename, [edi_readed.encode("UTF-8"), ]
        self.ftp.close()

        return result

    def send_specifics(self, directory):
        for file in os.listdir(directory):
            print("Going to process " + file)
            self.send_specific(directory + file)

    def send_specific_edi(self, filename):
        with open(filename, 'r') as file:
            lines = file.readlines()
            text = ""
            for line in lines:
                text = text + line
            xml = self.get_xml_from_edi("", "", "", text.encode("UTF-8"))
            for api in self.apis:
                user = User("apmt_system1", "e2bcbf95e071db30384448aad676f937")
                api.send_user_new(user)
            utis_in_xml = 0
            utis_created = list()
            if xml is not None:
                utis_in_xml, utis_created = self.send_xml_to_slcd(xml, 0)
                print("Utis in xml = " + str(utis_in_xml) + ". Utis created = " + str(utis_created))
            return xml, utis_in_xml, utis_created

    def send_specific(self, filename):
        xml = self.read_from_file(filename)
        for api in self.apis:
            api.send_user("ttiaAdmin", False)
        xml_to_send, voyage_number, elements = self.find_voyage_from_xml(xml)
        utis_in_xml = 0
        utis_created = list()
        if xml_to_send is not None:
            if voyage_number is not None:
                self.close_list(voyage_number)
            utis_in_xml, utis_created = self.send_xml_to_slcd(xml_to_send, elements)
            print("Utis in xml = " + str(utis_in_xml) + ". Utis created = " + str(utis_created))
        return xml, utis_in_xml, utis_created

    def delete_from_csv(self, csv_file):
        with open(csv_file, 'r') as file:
            lines = file.readlines()
            for line in lines:
                elements = line.split(";")
                voyage_number = elements[5]
                for api in self.apis:
                    api.send_user("ttiaAdmin", False)
                    voyage = api.get_voyages_from_number(voyage_number)
                    if len(voyage) > 0:
                        self.delete_voyage(voyage[0])

    def reprocess_csv(self, csv_file):
        self.print_csv_header()
        with open(csv_file, 'r') as file:
            lines = file.readlines()
            for line in lines:
                elements = line.split(";")
                utis_in_xml = elements[len(elements)-2]
                utis_created = elements[len(elements)-1]
                if len(utis_created) > 1:
                    utis_created = utis_created[:len(utis_created)-1]
                if utis_created.isnumeric() and int(utis_created) == 0 and utis_in_xml.isnumeric() and int(utis_in_xml) > 0:
                    xml_file_name = elements[1]
                    order = elements[2]
                    xml, utis_in_xml_2, utis_created_2 = self.send_specific("coarris/xmls/" + xml_file_name + "_" + order)
                    self.get_info_from_xml_and_save(xml, xml_file_name, order, "", utis_in_xml_2, utis_created_2)

    def process_to_csv(self):
        self.print_csv_header()
        print("\nFirst Message " + str(datetime.now()))
        subject, message_id, messages = self.read_next_message_from_ftp_or_mail()
        emails_readed = 0

        while message_id is not None:
            emails_readed = emails_readed + 1
            message_num = 1
            occurred_error = False
            for message in messages:
                print("Processing " + str(message_id) + "_" + str(message_num))
                xml = self.get_xml_from_edi(subject, message_id, message_num, message)
                if xml is not None and len(xml) > 0:
                    utis_in_xml = 0
                    utis_created = list()
                    xml_to_send = xml
                    if self.send_to_slcd:
                        for api in self.apis:
                            api.send_user("ttiaAdmin", False)
                        xml_to_send, voyage_number, elements = self.find_voyage_from_xml(xml)
                        if xml_to_send is not None:
                            if voyage_number is not None:
                                self.close_list(voyage_number)
                            self.save_to_file(xml_to_send, message_id, message_num, subject)
                            utis_in_xml, utis_created = self.send_xml_to_slcd(xml_to_send, elements)
                        else:
                            xml_to_send = xml
                            self.save_to_file(xml_to_send, message_id, message_num, subject)
                    self.get_info_from_xml_and_save(xml_to_send, message_id, message_num, subject, utis_in_xml, utis_created)

                    for utis_created_actual in utis_created:
                        if utis_in_xml != utis_created_actual:
                            # occurred_error = True
                            pass
                message_num = message_num + 1

            if not occurred_error:
                self.mark_as_processed(message_id)
            else:
                break
            print("\nNext Message (" + str(emails_readed+1) + ") at " + str(datetime.now()))
            subject, message_id, messages = self.read_next_message_from_ftp_or_mail()

        self.persist_voyages_created()
        self.persist_imos_obtained()
        print("\n---------------------------------\n\n")

    def create_service(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first time
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        # Call the Gmail API
        self.service = build('gmail', 'v1', credentials=creds)
        self.drive_service = build('drive', 'v3', credentials=creds, cache_discovery=False)

    def mark_as_processed(self, message_id):
        if self.to_read_from_ftp:
            return self.delete_from_ftp(message_id)
        else:
            return self.mark_as_readed(message_id)

    def mark_as_readed(self, message_id):
        # This will mark the message as read
        self.service.users().messages().modify(userId='me', id=message_id, body={'removeLabelIds': ['UNREAD']}).execute()

    def read_next_message_from_ftp_or_mail(self):
        if self.to_read_from_ftp:
            return self.read_from_ftp()
        else:
            return self.read_next_message()

    def send_to_drive(self, filepath, is_xml):
        xmls_dir = '1pXvg9QA3swFEfcAEi_-dAHkv0eTU7LRG'
        edis_dir = '1R1MTEIjY5EFcHFWut0QY9Yo5UlUUc3o-'
        if is_xml:
            dir_id = xmls_dir
        else:
            dir_id = edis_dir

        file_metadata = {'name': filepath.split("/")[len(filepath.split("/"))-1], 'parents': [dir_id]}
        media = MediaFileUpload(filepath, mimetype='application/xml')

        file = self.drive_service.files().create(body=file_metadata,
                                                 media_body=media,
                                                 fields='id').execute()
        print("File ID: " + str(file.get('id')))

    def resend_edi_from_drive(self, file_ids):
        for file_id in file_ids:
            request = self.drive_service.files().get_media(fileId=file_id)
            filename = file_id + ".edi"
            fh = io.FileIO(filename, "wb")
            downloader = MediaIoBaseDownload(fh, request)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print("Download %d%%." % int(status.progress() * 100))
            fh.close()

            self.copy_to_new_dir_ftp("outbox_slcd_to_aws", filename)
        print("Done")

    def copy_to_new_dir_ftp(self, directory, filename):
        self.ftp.connect('pruebas.teleport.es', 21)
        self.ftp.sendcmd('USER ttia')
        self.ftp.sendcmd('PASS vacu83sKpK6B')
        self.ftp.cwd(directory)
        with open("%s" % filename, 'rb') as pcfile:
            self.ftp.storbinary('STOR ' + filename, pcfile)
        self.ftp.close()
        print("Copied " + filename + " to " + directory)

    def get_drive_id_from_file(self, filename):
        response = self.drive_service.files().list(q="name='" + filename + "'",).execute()
        for file in response.get('files', []):
            print('Found file: %s (%s)' % (file.get('name'), file.get('id')))

    def read_next_message(self):
        results = self.service.users().messages().list(userId='me', labelIds='UNREAD').execute()
        messages = results.get('messages', [])

        if not messages:
            print('No messages found.')
        else:
            print("Found " + str(len(messages)) + " UNREAD messages:")
            for mssg in messages:
                m_id = mssg['id']  # get id of individual message

                message = self.service.users().messages().get(userId='me', id=m_id).execute()  # fetch the message
                messages_readed = list()
                subject = ""

                email_date = ""
                for header in message['payload']['headers']:  # getting the Subject
                    if header['name'] == 'Subject':
                        subject = header['value']
                    if header['name'] == 'Date':
                        email_date = header['value']
                    if header['name'] == 'From':
                        sender = header['value']
                        print("From: " + sender)
                        if sender.find("edi@ttialgeciras.com") < 0:
                            return subject, m_id, list()
                print("Email Date: " + email_date)
                subject = subject + ":" + email_date

                for part in message['payload']['parts']:  # getting payload and attachments
                    if part['filename']:
                        if 'data' in part['body']:
                            data = part['body']['data']
                        else:
                            att_id = part['body']['attachmentId']
                            att = self.service.users().messages().attachments().get(userId='me', messageId=m_id, id=att_id).execute()
                            data = att['data']
                        filename = part['filename']
                        file_data = base64.urlsafe_b64decode(data.encode('UTF-8'))

                        print("\tDescargando adjunto: " + filename)
                        if not self.has_valid_extension(filename):
                            continue

                        if len(file_data) > 75:
                            print("\t\t" + str(file_data)[:75] + "...")
                        else:
                            print("\t\t" + str(file_data) + "...")
                        messages_readed.append(file_data)

                print("Subject: " + subject + ". mId: " + m_id)
                return subject, m_id, messages_readed

        return None, None, None

    def get_xml_from_edi(self, subject, message_id, message_num, message):
        conversion_object = ConversionRest(message.decode("UTF-8"))
        if conversion_object.inputValue.find("COARRI") < 0:
            return None

        headers = {'Content-type': 'application/json'}
        response = requests.post(CONVERSION_URL, data=conversion_object.to_json(), headers=headers)
        if response.ok:
            return ConversionRest.create_from_json(response.json()).outputValue
        else:
            print("ERROR " + response.reason)
            self.write_to_file(subject, message_id, message_num, "", "", "", "", "", "", "", "", "", "")

        return None

    def send_xml_to_slcd(self, xml, elements):
        if len(xml) >= 25:
            print("Going to send xml with " + str(elements) + " utis: " + xml[:25] + "...")

        xml_rest = XmlData(0, "ttiaAdmin", xml)
        utis_created = list()
        index = 0
        for api in self.apis:
            prev_time = datetime.now()
            if self.apis_xml_new_version[index]:
                massive_coarri = is_massive_coarri(xml)
                utis_created.append(api.import_xml_new_coarri(xml_rest, massive_coarri, False))
            else:
                utis_created.append(api.import_xml(xml_rest, False))
            actual_time = datetime.now()
            print("Time to execute import = " + str(actual_time - prev_time))
            index = index + 1
        return int(elements), utis_created

    def close_list(self, voyage_number):
        for api in self.apis:
            lists = api.get_lists_from_voyage(voyage_number)
            for list_data in lists:
                if list_data.state == "PREPARACION":
                    list_data.state = "CERRADA"
                    api.put_list(list_data)

    def find_voyage_from_xml(self, xml):
        root = elementTree.fromstring(xml)
        mtd_group = get_maritime_details(root)

        arrival_date = ""
        if mtd_group.find("arrivalDate") is not None:
            arrival_date = mtd_group.find("arrivalDate").text
        departure_date = ""
        if mtd_group.find("departureDate") is not None:
            departure_date = mtd_group.find("departureDate").text
        vessel_call_sign = ""
        if mtd_group.find("vesselCallSign") is not None:
            vessel_call_sign = mtd_group.find("vesselCallSign").text
        vessel_name = ""
        if mtd_group.find("vesselName") is not None:
            vessel_name = mtd_group.find("vesselName").text

        if not self.has_been_created_voyage_previously(vessel_call_sign, arrival_date):
            print("Voyage never created for " + vessel_call_sign + " and " + arrival_date)
            if mtd_group.find("voyageNumber") is None:
                print("Error message without voyageNumber")
                return None, None, None

            voyage_number = mtd_group.find("voyageNumber").text
            changed_voyage_number = False
            if len(voyage_number) < 7:
                changed_voyage_number = True
                voyage_number = voyage_number.zfill(7)
            if not (voyage_number[:2] == "CR" or voyage_number[:2] == "CF"):
                changed_voyage_number = True
                voyage_number = "CF2/" + voyage_number

            self.voyages_created.append(vessel_call_sign + "-" + arrival_date)
            if changed_voyage_number:
                xml = self.change_voyage_number_in_xml(root, voyage_number)

            for api in self.apis:
                voyages = api.get_voyages_from_number(voyage_number)
                print("Voyages found for " + voyage_number + " = " + str(len(voyages)))
                if len(voyages) == 0:
                    ship_imo = self.find_imo(arrival_date, departure_date, vessel_call_sign, vessel_name)

                    print("Creating voyage with number " + voyage_number)
                    if len(arrival_date) > 0:
                        eta = datetime.strptime(arrival_date, '%Y%m%d%H%M')
                    else:
                        eta = datetime.now()

                    voyage = Voyage(ship_imo, voyage_number, eta.strftime(DATETIME_FORMAT))
                    api.create_voyage(voyage, True)
        else:
            print("Voyage created previously for " + vessel_call_sign + " and " + arrival_date)
            voyage_number = None

        equipments = "0"
        if root.find("containerTotalNumber") is not None:
            equipments = root.find("containerTotalNumber").text
        return xml, voyage_number, equipments

    def get_info_from_xml_and_save(self, xml, message_id, message_num, subject, utis_in_xml, utis_created):
        root = elementTree.fromstring(xml)
        header_group = get_message_header(root)
        sender = header_group.find("senderId").text
        receiver = header_group.find("receiverId").text
        mtd_group = get_maritime_details(root)
        voyage_number = ""
        if mtd_group.find("voyageNumber") is not None:
            voyage_number = mtd_group.find("voyageNumber").text
        shipping_line_code = ""
        if mtd_group.find("shippingLineCode") is not None:
            shipping_line_code = mtd_group.find("shippingLineCode").text
        vessel_name = ""
        if mtd_group.find("vesselName") is not None:
            vessel_name = mtd_group.find("vesselName").text
        vessel_call_sign = ""
        if mtd_group.find("vesselCallSign") is not None:
            vessel_call_sign = mtd_group.find("vesselCallSign").text
        arrival_date = ""
        if mtd_group.find("arrivalDate") is not None:
            arrival_date = mtd_group.find("arrivalDate").text
        departure_date = ""
        if mtd_group.find("departureDate") is not None:
            departure_date = mtd_group.find("departureDate").text
        print("Sender: " + sender + " \tReceiver: " + receiver + " \tVoyageNumber: " + voyage_number +
              " \tShipping_line_code: " + shipping_line_code + " \tVessel_name: " + vessel_name + " \tVessel_call_sign: " + vessel_call_sign +
              " \tArrival_date: " + arrival_date + " \tDeparture_date: " + departure_date + "")

        self.write_to_file(subject, message_id, message_num, sender, receiver, voyage_number, shipping_line_code, vessel_name, vessel_call_sign, arrival_date, departure_date,
                           utis_in_xml, utis_created)

    def save_to_file(self, xml, message_id, message_num, subject):
        filename = "coarris/xmls/" + message_id + "_" + str(message_num)
        with open(filename, 'w') as file:
            file.write("<!-- Subject:" + subject + " -->\n")
            file.write("<!-- MessageId:" + message_id + " -->\n")
            file.write("<!-- Order:" + str(message_num) + " -->\n")
            file.write(xml)
            file.close()

        self.send_to_drive(filename, True)

    def find_imo(self, arrival_date_string, departure_date_string, vessel_call_sign, vessel_name):
        print("finding visit for " + vessel_call_sign + " - " + vessel_name)
        imo = self.find_imo_from_table(vessel_call_sign)

        if imo is not None:
            print("found from old data. Callsign = " + vessel_call_sign + ". IMO = " + imo)
            return imo
        else:
            imo = "1111111"

        if len(arrival_date_string) == 0:
            print("found from old data. Error. Not arrival date.")
            return imo

        arrival_date = datetime.strptime(arrival_date_string, '%Y%m%d%H%M')
        if len(departure_date_string) > 0:
            departure_date = datetime.strptime(departure_date_string, '%Y%m%d%H%M')
            seconds = (departure_date - arrival_date).total_seconds() / 2
            eta = arrival_date + timedelta(0, seconds)
        else:
            eta = arrival_date

        tp_api = ConsumeTP_APIRest()
        tp_api.send_user()
        stops = tp_api.get_stops_from_ETA(eta.strftime("%Y%m%d%H%M"))
        for stop in stops:
            # print("callsign: " + stop["callSign"] + ". vessel: " + stop["shipName"] + ". imo: " + stop["shipImo"])
            self.add_imo_to_map(stop["callSign"], stop["shipImo"])
            if stop["callSign"] == vessel_call_sign or stop["shipName"] == vessel_name:
                imo = stop["shipImo"]
                print("Found IMO: " + imo)
                break
        return imo

    @staticmethod
    def has_valid_extension(filename):
        not_valid_extensions = (".png", ".pdf", ".jpg", ".xls")
        for not_valid_extension in not_valid_extensions:
            if filename.lower().find(not_valid_extension) >= 0:
                return False
        return True

    def write_to_file(self, subject, message_id, message_num, sender, receiver, voyage_number, shipping_line_code, vessel_name, vessel_call_sign, arrival_date, departure_date,
                      utis_in_xml, utis_created):
        with open("coarris/coarris_" + self.time_string + ".csv", 'a') as file:
            line_to_write = subject + ";" + message_id + ";" + str(message_num) + ";" + sender + ";" + receiver + ";" + voyage_number + ";" + shipping_line_code + ";" + \
                            vessel_name + ";" + vessel_call_sign + ";" + arrival_date + ";" + departure_date + ";" + str(utis_in_xml) + ";"
            for uti_created in utis_created:
                line_to_write = line_to_write + str(uti_created) + ";"
            file.write(line_to_write + "\n")
            file.close()

    @staticmethod
    def read_from_file(filename):
        message = ""
        with open(filename, 'r') as file:
            lines = file.readlines()
            for line in lines:
                if not line.startswith("<!--"):
                    message += line
        return message

    def print_csv_header(self):
        # coarri_csv_filename = "coarris/coarris_" + self.time_string + ".csv"
        coarri_csv_filename = "coarris/coarris.csv"
        if not os.path.exists(coarri_csv_filename):
            with open(coarri_csv_filename, 'w') as file:
                header = "Subject;MessageId;Order;Sender;Receiver;VoyageNumber;Shipping_line_code;Vessel_name;Vessel_call_sign;Arrival_date;Departure_date;UtisInXML;"
                for api_name in self.apis_names:
                    header = header + "UtisIn" + str(api_name) + ";"
                print(header, file=file)
                file.close()

    def delete_voyage(self, voyage):
        for api in self.apis:
            lists_from_voyage = api.get_lists_from_voyage(voyage.number)
            for actual_list in lists_from_voyage:
                utis = api.get_utis_from_voyage(actual_list.id)
                for uti in utis:
                    api.delete_uti(uti)
                api.delete_list(actual_list)
            api.delete_voyage(voyage)

    @staticmethod
    def change_voyage_number_in_xml(root, new_voyage_number):
        mtd_groups = get_maritime_details_all(root)
        for mtd_group in mtd_groups:
            mtd_group.find("voyageNumber").text = new_voyage_number
        return elementTree.tostring(root).decode("utf-8")

    def persist_voyages_created(self):
        with open('voyages_created.pickle', 'wb') as token:
            pickle.dump(self.voyages_created, token)

    def has_been_created_voyage_previously(self, vessel_call_sign, arrival_date):
        return len(vessel_call_sign) > 0 and len(arrival_date) > 0 and self.voyages_created.count(vessel_call_sign + "-" + arrival_date) > 0

    def find_imo_from_table(self, vessel_call_sign):
        return self.imos_obtained.get(vessel_call_sign, None)

    def persist_imos_obtained(self):
        with open('imos_obtained.pickle', 'wb') as token:
            pickle.dump(self.imos_obtained, token)

    def add_imo_to_map(self, callsign, imo):
        if len(callsign) > 0 and len(imo) > 0 and self.imos_obtained.get(callsign, None) is not None:
            self.imos_obtained[callsign] = imo


# SendCoarris(send_to_slcd=True, read_from_ftp=True).process_to_csv()
# SendCoarris(send_to_slcd=True, read_from_ftp=True).resend_edi_from_drive(("16z4lBXxYCB5nCTrkKToQH3Tj-UuunMwp", "1-QzBChIC38zKtpwI3DOIDVnXiSpucQ06"))
# SendCoarris(send_to_slcd=True).delete_from_csv("coarris/coarris_20201104192002.csv")
# SendCoarris(send_to_slcd=True).reprocess_csv("coarris/coarris_20201104192002.csv")
SendCoarris(send_to_slcd=True, read_from_ftp=False).send_specifics("coarris/fallos/")

# send_coarris = SendCoarris(send_to_slcd=True, read_from_ftp=False)
# send_coarris.send_specific_edi("coarris/apmt/1609781527832COARRI_APBA.edi")
#send_coarris.send_specific_edi("coarris/apmt/1609782388380COARRI_APBA.edi")
#send_coarris.send_specific_edi("coarris/apmt/1609782491255COARRI_APBA.edi")
#send_coarris.send_specific_edi("coarris/apmt/1609782541927COARRI_APBA.edi")
