import sys
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.User import User
from consumeAPIRest import ConsumeAPIRest
from consumeTP_APIRest import ConsumeTP_APIRest

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"
voyage_number = ""

if len(sys.argv) == 2:
	voyage_number = sys.argv[1]
elif len(sys.argv) == 1:
	print("Vamos a buscar todos los viajes LOLO abiertos para cerrarlos.")
	print("Para cerrar un viaje concreto (LOLO o RORO) use: test_voyage.py voyage_number.")
	init_id = int(input("Introduzca un id de viaje, por el que empezar: "))
	all = 's'
else:
	print("Argumentos incorrectos. Uso: test_endvoyage.py voyage_number")
	exit()

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")
#slcd_api = ConsumeAPIRest("https://pruebas.teleport.es/slcd/")

usernames = ["ttia_system1", "apmt_system1", "aml_user2", "tras_user2", "baem_user2", "ints_user2", "frs_user2"]

if all is not 's':

	found = False
	for username in usernames:

		if found is not True:

			user = User(username, "e2bcbf95e071db30384448aad676f937")
			result = slcd_api.send_user_new(user, False)
			if not result.ok:
				print("No he podido conectarme como " + username)
				exit()
			else:
				print("Conectado como " + username)

			voyages = slcd_api.get_voyages_from_number(voyage_number)
			if len(voyages) == 0:
				print("No encuentro viaje como " + username)
			else:
				if voyages[0].type == "RORO":
					# Compruebo que el usuario es de la misma compania que la del viaje
					#print(username[0:3].upper() + " - " + voyages[0].shippingCompany)
					if username[0:3].upper() == voyages[0].shippingCompany[0:3]:
						print("Encontrado viaje como " + username)
						found = True
				else:
					print("Encontrado viaje como " + username)
					found = True

	if len(voyages) == 0:

		go = input("Quieres crear el viaje? ")
		if go == 's':

			username = "ttiaAdmin"
			print("Conecto a SLCD como " + username)
			slcd_api.send_user(username=username)

			voyage_eta = datetime.now() + timedelta(days=10)
			voyage_imo = "9450337"
			voyage = Voyage(imo=voyage_imo, number=voyage_number, eta=voyage_eta.strftime(DATETIME_FORMAT))
			slcd_api.create_voyage(voyage)
			print(voyage.to_json())

			voyages = slcd_api.get_voyages_from_number(voyage_number)
		else:
			print("FIN de la prueba")
			exit()

	if len(voyages) == 0:
		print("Algo ha ido mal ...")
		exit()

	print("Vamos a modificar el viaje " + voyage_number)

	new_voyage = voyages[0]
	print(new_voyage.to_json())

	go = input("Quieres actualizar fecha-hora de inicio? ")
	if go == 's':
		voyage_init = datetime.now() + timedelta(hours=-2)
		new_voyage.initOpsDateTime = voyage_init.strftime(DATETIME_FORMAT)
		slcd_api.put_voyage(new_voyage)
		new_voyage = slcd_api.get_voyages_from_number(voyage_number)[0]
		print(new_voyage.to_json())

	go = input("Quieres actualizar fecha-hora de fin? ")
	if go == 's':
		voyage_end = datetime.now() + timedelta(hours=-2)
		new_voyage.endOpsDateTime = voyage_end.strftime(DATETIME_FORMAT)
		slcd_api.put_voyage(new_voyage)
		new_voyage = slcd_api.get_voyages_from_number(voyage_number)[0]
		print(new_voyage.to_json())

	'''
	print("Listas consolidadas del viaje:")
	
	rxlists = slcd_api.get_lists_from_voyage(voyage_number)
	print("Obtengo " + str(len(rxlists)) + " listas:")
	for rxlist in rxlists:
		print(rxlist.to_json())
	'''
else:

	# Cierro todos los viajes

	voyage_init = datetime.now() + timedelta(hours=-4)
	voyage_end = datetime.now() + timedelta(hours=-2)

	contador_salida = 0
	voyage = None

	for counter in range(init_id,1000000):

		print(">>> Buscando viaje con id " + str(counter))

		found = False
		for username in usernames:

			if found is not True:

				user = User(username, "e2bcbf95e071db30384448aad676f937")
				result = slcd_api.send_user_new(user, False)
				if not result.ok:
					print("\tNo he podido conectarme como " + username)
					exit()
				else:
					print("\tConectado como " + username)

				voyage = slcd_api.get_voyage_from_id(counter)
				if voyage is None:
					print("\tNo encuentro viaje como " + username)
				else:
					if voyage.type == "RORO":
						# Compruebo que el usuario es de la misma compania que la del viaje
						# print(username[0:3].upper() + " - " + voyages[0].shippingCompany)
						if username[0:3].upper() == voyage.shippingCompany[0:3]:
							print("Encontrado viaje como " + username)
							found = True
					else:
						print("Encontrado viaje como " + username)
						found = True

		if voyage is not None:

			consolidated_lists = slcd_api.get_lists_from_voyage(voyage.number)
			for alist in consolidated_lists:
				if alist.consolidationState == "PREPARACION":
					print("\t>>>>> List ID: " + str(alist.id) + " " + alist.type + " " + alist.circuit + " " +
						alist.consolidationState + " " + alist.closingDateTime)
					alist.state = "CERRADA"
					slcd_api.put_list(alist)

			print("\t" + voyage.number + " " + voyage.voyageStatus)

			if voyage.initOpsDateTime is None:
				print("\t" + voyage.number + " " + voyage.voyageStatus + " " + ">>> Inicio")
				voyage.initOpsDateTime = voyage_init.strftime(DATETIME_FORMAT)
				slcd_api.put_voyage(voyage)

			if voyage.endOpsDateTime is None:
				print("\t" + voyage.number + " " + voyage.voyageStatus + " " + ">>> Fin")
				voyage.endOpsDateTime = voyage_init.strftime(DATETIME_FORMAT)
				slcd_api.put_voyage(voyage)

			contador_salida = 0

		else:
			contador_salida += 1
			if contador_salida > 20:
				break

print("FIN de la prueba")
print(str(datetime.now()))




