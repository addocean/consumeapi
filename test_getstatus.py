import datetime
import random
import sys
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.User import User
from consumeAPIRest import ConsumeAPIRest

class UtiDescargada:
	def __init__(self, plate, uti_circuit, voyage):
		self.plate = plate
		self.uti_circuit = uti_circuit
		self.voyage = voyage

class VoyageUser:
	def __init__(self, voyage_id, username):
		self.voyage_id = voyage_id
		self.username = username

analizar_transbordos = False
arreglar_transbordos = False
voyage_id_init = 50

str_input = input("Analizar transbordos?(s/n): ")
if str_input == "s":
	analizar_transbordos = True

str_input = input("Arreglar transbordos?(s/n): ")
if str_input == "s":
	arreglar_transbordos = True

str_input = input("Recalcular todo?(s/n): ")
if str_input == "s":
	voyage_id_init = 1

# -------
total_LOLO_aceptadas = 0
total_LOLO_rechazadas = 0
total_LOLO_pendientes = 0
total_LOLO_modificadas = 0
total_LOLO_añadidas = 0
total_LOLO_total = 0
total_LOLO_llenas = 0
total_LOLO_vacias = 0
total_LOLO_carga_exportacion = 0
total_LOLO_carga_importacion = 0
total_LOLO_carga_transbordos_misma = 0
total_LOLO_carga_transbordos_2T = 0
total_LOLO_descarga_exportacion = 0
total_LOLO_descarga_importacion = 0
total_LOLO_descarga_transbordos_misma = 0
total_LOLO_descarga_transbordos_2T = 0
total_LOLO_carga = 0
total_LOLO_descarga = 0
total_LOLO_exportacion_ACCEPTED = 0
total_LOLO_exportacion_NON_ACCEPTED = 0
total_LOLO_exportacion_NO_NECESSARY = 0
total_LOLO_exportacion_UNKNOWN = 0
total_LOLO_transbordo_ACCEPTED = 0
total_LOLO_transbordo_NON_ACCEPTED = 0
total_LOLO_transbordo_NO_NECESSARY = 0
total_LOLO_transbordo_UNKNOWN = 0
# -------
total_RORO_aceptadas = 0
total_RORO_rechazadas = 0
total_RORO_pendientes = 0
total_RORO_modificadas = 0
total_RORO_añadidas = 0
total_RORO_total = 0
total_RORO_llenas = 0
total_RORO_vacias = 0
total_RORO_carga_exportacion = 0
total_RORO_carga_importacion = 0
total_RORO_carga_transbordos_misma = 0
total_RORO_carga_transbordos_2T = 0
total_RORO_carga = 0
total_RORO_exportacion_ACCEPTED = 0
total_RORO_exportacion_NON_ACCEPTED = 0
total_RORO_exportacion_NO_NECESSARY = 0
total_RORO_exportacion_UNKNOWN = 0
# -----
total_listas_carga_en_preparacion = 0
total_listas_descarga_en_preparacion = 0
total_transbordos_encontrados = 0
total_viajes_LOLO = 0
total_viajes_RORO = 0
# -----
if voyage_id_init > 1:
	total_LOLO_aceptadas = 480
	total_LOLO_rechazadas = 99
	total_LOLO_pendientes = 0
	total_LOLO_modificadas = 1113
	total_LOLO_añadidas = 17036
	total_LOLO_total = 18728
	total_LOLO_llenas = 16023
	total_LOLO_vacias = 2705
	total_LOLO_carga_exportacion = 1306
	total_LOLO_carga_importacion = 0
	total_LOLO_carga_transbordos_misma = 9254
	total_LOLO_carga_transbordos_2T = 0
	total_LOLO_descarga_exportacion = 0
	total_LOLO_descarga_importacion = 911
	total_LOLO_descarga_transbordos_misma = 8254
	total_LOLO_descarga_transbordos_2T = 0
	total_LOLO_carga = 10560
	total_LOLO_descarga = 8168
	total_LOLO_exportacion_ACCEPTED = 1000
	total_LOLO_exportacion_NON_ACCEPTED = 33
	total_LOLO_exportacion_NO_NECESSARY = 273
	total_LOLO_exportacion_UNKNOWN = 0
	total_LOLO_transbordo_ACCEPTED = 4172
	total_LOLO_transbordo_NON_ACCEPTED = 4281
	total_LOLO_transbordo_NO_NECESSARY = 801
	total_LOLO_transbordo_UNKNOWN = 0
	# -------
	total_RORO_aceptadas = 0
	total_RORO_rechazadas = 0
	total_RORO_pendientes = 0
	total_RORO_modificadas = 0
	total_RORO_añadidas = 0
	total_RORO_total = 0
	total_RORO_llenas = 0
	total_RORO_vacias = 0
	total_RORO_carga_exportacion = 0
	total_RORO_carga_importacion = 0
	total_RORO_carga_transbordos_misma = 0
	total_RORO_carga_transbordos_2T = 0
	total_RORO_carga = 0
	total_RORO_exportacion_ACCEPTED = 0
	total_RORO_exportacion_NON_ACCEPTED = 0
	total_RORO_exportacion_NO_NECESSARY = 0
	total_RORO_exportacion_UNKNOWN = 0
	# -----
	total_viajes_LOLO = 30
	total_viajes_RORO = 0

# -----
#slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")
slcd_api = ConsumeAPIRest("https://pruebas.teleport.es/slcd/")

usernames = ["ttia_system1","apmt_system1","ints_user2","aml_user2","frs_user2","tras_user2","baem_user2"]

utis_descargadas_list = list()

voyage_user_list = list()
voyage_user_list.append(VoyageUser(75,"apmt_system1"))
voyage_user_list.append(VoyageUser(76,"apmt_system1"))
voyage_user_list.append(VoyageUser(77,"apmt_system1"))
voyage_user_list.append(VoyageUser(78,"apmt_system1"))
voyage_user_list.append(VoyageUser(79,"apmt_system1"))
voyage_user_list.append(VoyageUser(80,"apmt_system1"))
voyage_user_list.append(VoyageUser(82,"ints_user2"))
voyage_user_list.append(VoyageUser(83,"aml_user2"))
voyage_user_list.append(VoyageUser(84,"apmt_system1"))
voyage_user_list.append(VoyageUser(86,"frs_user2"))
voyage_user_list.append(VoyageUser(87,"ints_user2"))
voyage_user_list.append(VoyageUser(88,"aml_user2"))
voyage_user_list.append(VoyageUser(89,"frs_user2"))
voyage_user_list.append(VoyageUser(90,"ints_user2"))
voyage_user_list.append(VoyageUser(91,"frs_user2"))
voyage_user_list.append(VoyageUser(92,"aml_user2"))
voyage_user_list.append(VoyageUser(93,"ints_user2"))
voyage_user_list.append(VoyageUser(94,"frs_user2"))
voyage_user_list.append(VoyageUser(95,"aml_user2"))
voyage_user_list.append(VoyageUser(97,"ints_user2"))
voyage_user_list.append(VoyageUser(98,"ints_user2"))
voyage_user_list.append(VoyageUser(99,"frs_user2"))
voyage_user_list.append(VoyageUser(100,"frs_user2"))
voyage_user_list.append(VoyageUser(101,"frs_user2"))
voyage_user_list.append(VoyageUser(102,"frs_user2"))
voyage_user_list.append(VoyageUser(103,"aml_user2"))
voyage_user_list.append(VoyageUser(104,"aml_user2"))
voyage_user_list.append(VoyageUser(105,"ints_user2"))
voyage_user_list.append(VoyageUser(106,"frs_user2"))
voyage_user_list.append(VoyageUser(107,"frs_user2"))
voyage_user_list.append(VoyageUser(108,"frs_user2"))
voyage_user_list.append(VoyageUser(109,"aml_user2"))
voyage_user_list.append(VoyageUser(110,"frs_user2"))
voyage_user_list.append(VoyageUser(111,"aml_user2"))
voyage_user_list.append(VoyageUser(112,"frs_user2"))
voyage_user_list.append(VoyageUser(113,"aml_user2"))
voyage_user_list.append(VoyageUser(114,"aml_user2"))
voyage_user_list.append(VoyageUser(115,"frs_user2"))
voyage_user_list.append(VoyageUser(116,"ints_user2"))
voyage_user_list.append(VoyageUser(117,"frs_user2"))
voyage_user_list.append(VoyageUser(118,"ints_user2"))
voyage_user_list.append(VoyageUser(119,"frs_user2"))
voyage_user_list.append(VoyageUser(120,"ints_user2"))
voyage_user_list.append(VoyageUser(121,"aml_user2"))
voyage_user_list.append(VoyageUser(122,"frs_user2"))
voyage_user_list.append(VoyageUser(123,"ints_user2"))

valid_co_list = ["CMA","NILE","OOL","HLC","ONE","COS","OPD","MGS","APL",
				 "MFR","COE","MSC","MSK","EMS","ZIM","DAL","SEJ","HSB","EMC",
				 "INTE","AML","FRSI"]


print("======================= INI (", datetime.datetime.now().strftime('%d/%m/%Y %H:%M'), ") =======================")

contador_fin = 0
voyage_id_init = 200
for voyage_id in range(voyage_id_init, 380):

	if voyage_id == 371:
		voyage_id = 441

	if voyage_id == 81 or voyage_id == 85 or voyage_id == 371:
		continue

	username = ""
	is_not_connected = True
	'''
	for voyage_user in voyage_user_list:
		if voyage_user.voyage_id == voyage_id:
			username = voyage_user.username
			user = User(username, "e2bcbf95e071db30384448aad676f937")
			result = slcd_api.send_user_new(user, False)
			if not result.ok:
				print("No he podido conectarme como ",username)
				exit()
			is_not_connected = False
	'''

	if is_not_connected:
		username = "ttia_system1"
		user = User(username, "e2bcbf95e071db30384448aad676f937")
		result = slcd_api.send_user_new(user, False)
		if not result.ok:
			print("No he podido conectarme como ", username)
			exit()

	print("========================================================================")
	voyage = slcd_api.get_voyage_from_id(voyage_id)
	rxlists = list()
	if voyage is not None:
		rxlists = slcd_api.get_lists_from_voyage(voyage.number)

	if voyage is None or len(rxlists) == 0:
		continue

		found = False
		for username in usernames:
			user = User(username, "e2bcbf95e071db30384448aad676f937")
			result = slcd_api.send_user_new(user, False)
			if not result.ok:
				print("No he podido conectarme como ",username)
				exit()

			voyage = slcd_api.get_voyage_from_id(voyage_id)
			if voyage is None:
				print("\tNo encuentro viaje como " + username)
			else:
				if voyage.type == "RORO":
					# Compruebo que el usuario es de la misma compañia que la del viaje
					#print(username[0:3].upper() + " - " + voyage.shippingCompany)
					if username[0:3].upper() == voyage.shippingCompany[0:3]:
						found = True
					else:
						print("\tNo encuentro viaje como " + username)
				else:
					found = True

			if found:
				contador_fin = 0
				break

	if voyage is None:
		print("\tNo encuentro viaje", voyage_id, "con ningún usuario")
		contador_fin += 1
		if contador_fin > 2:
			break
	else:

		print("Viaje", voyage.id, voyage.number, "de", username, voyage.voyageStatus)
		rxlists = slcd_api.get_lists_from_voyage(voyage.number)

		if len(rxlists) == 0:
			print("No encuentro las listas consolidadas.")
		else:
			total_utis = 0
			for rxlist in rxlists:
				utilist = slcd_api.get_utis_from_voyage(rxlist.id)
				print(">>>", voyage.number, rxlist.type, rxlist.circuit, rxlist.consolidationState, "informe",
					  rxlist.reportStatus, "con", str(len(utilist)), "UTIs.")

				utiStatus_list = list()
				utiFilledStatus_list = list()
				utiCircuit_list = list()
				utiLevante_list = list()

				exportacion_ACCEPTED = 0
				exportacion_NON_ACCEPTED = 0
				exportacion_NO_NECESSARY = 0
				exportacion_UNKNOWN = 0

				transbordo_ACCEPTED = 0
				transbordo_NON_ACCEPTED = 0
				transbordo_NO_NECESSARY = 0
				transbordo_UNKNOWN = 0

				if rxlist.circuit == "Carga":
					if rxlist.reportStatus == "PREPARATION":
						total_listas_carga_en_preparacion += 1
				elif rxlist.circuit == "Descarga":
					if rxlist.reportStatus == "PREPARATION":
						total_listas_descarga_en_preparacion += 1

				for uti in utilist:
					'''
					#print(uti.to_json())
					is_valid_co = False
					for valid_co in valid_co_list:
						if uti.containerOperator == valid_co:
							is_valid_co = True

					if is_valid_co is not True:
						print("CO inválido",uti.plate,uti.containerOperator)
						exit()
					'''

					if voyage.type == "LOLO" and analizar_transbordos:
						if rxlist.circuit == "Carga":
							# Analizo transbordos
							if uti.circuit == "TRANSBORDO_MISMA":
								for uti_descargada in utis_descargadas_list:
									if uti.plate == uti_descargada.plate and uti.transshipmentVoyage != uti_descargada.voyage:
										if uti.levanteStatus == "NON ACCEPTED":
											print(uti.plate, "encontrada en", uti_descargada.voyage, uti.levanteStatus)
											total_transbordos_encontrados += 1

											if arreglar_transbordos:
												uti.transshipmentVoyage = uti_descargada.voyage
												slcd_api.put_uti(uti)

						elif rxlist.circuit == "Descarga":
							# Me guardo las descargadas para analizar los transbordos
							uti_descargada = UtiDescargada(plate=uti.plate, uti_circuit=uti.circuit,
														   voyage=rxlist.voyageNumber)
							utis_descargadas_list.append(uti_descargada)

					'''
					if uti.utiStatus == "PENDING":
						print('\033[91m', uti.plate, uti.utiStatus, '\033[0m')
					'''

					if uti.levanteStatus == "NON ACCEPTED" and uti.circuit == "EXPORTACION":
						print('\033[91m', uti.plate, uti.levanteStatus, uti.circuit, '\033[0m')

					utiStatus_list.append(uti.utiStatus)
					utiFilledStatus_list.append(uti.filledStatus)
					utiCircuit_list.append(uti.circuit)
					utiLevante_list.append(uti.levanteStatus)

					if rxlist.circuit == "Carga":

						if uti.circuit == "EXPORTACION":
							if uti.levanteStatus == "ACCEPTED":
								exportacion_ACCEPTED += 1
							elif uti.levanteStatus == "NO NECESSARY":
								exportacion_NO_NECESSARY += 1
							elif uti.levanteStatus == "NON ACCEPTED":
								exportacion_NON_ACCEPTED += 1
							# print(uti.plate)
							elif uti.levanteStatus == "UNKNOWN":
								exportacion_UNKNOWN += 1

						if uti.circuit == "TRANSBORDO_MISMA" or uti.circuit == "TRANSBORDO_2T":
							if uti.levanteStatus == "ACCEPTED":
								transbordo_ACCEPTED += 1
							elif uti.levanteStatus == "NO NECESSARY":
								transbordo_NO_NECESSARY += 1
							elif uti.levanteStatus == "NON ACCEPTED":
								transbordo_NON_ACCEPTED += 1
							# print(uti.plate)
							elif uti.levanteStatus == "UNKNOWN":
								transbordo_UNKNOWN += 1

				aceptadas = utiStatus_list.count("ACCEPTED")
				rechazadas = utiStatus_list.count("REJECTED")
				pendientes = utiStatus_list.count("PENDING")
				modificadas = utiStatus_list.count("MODIFIED")
				añadidas = utiStatus_list.count("UNPLANNED")
				total = aceptadas + rechazadas + pendientes + modificadas + añadidas
				llenas = utiFilledStatus_list.count("Lleno")
				vacias = utiFilledStatus_list.count("Vacío")
				exportacion = utiCircuit_list.count("EXPORTACION")
				importacion = utiCircuit_list.count("IMPORTACION")
				transbordos_misma = utiCircuit_list.count("TRANSBORDO_MISMA")
				transbordos_2T = utiCircuit_list.count("TRANSBORDO_2T")
				levante_ok = utiLevante_list.count("ACCEPTED")
				levante_ko = utiLevante_list.count("NON ACCEPTED")
				levante_nn = utiLevante_list.count("NO NECESSARY")
				levante_uk = utiLevante_list.count("UNKNOWN")

				if total > 0:
					print("\tACCEPTED: " + str(aceptadas))
					print("\tREJECTED: " + str(rechazadas))
					print("\tPENDING : " + str(pendientes))
					print("\tMODIFIED: " + str(modificadas))
					print("\tADDED   : " + str(añadidas))
					print("\t--------------------")
					print("\tLlenas  : " + str(llenas))
					print("\tVacías  : " + str(vacias))
					print("\t--------------------")
					if rxlist.circuit == "Carga":
						print("\tExport. :", exportacion,
							  "V:" + str(exportacion_ACCEPTED), "R:" + str(exportacion_NON_ACCEPTED),
							  "NN:" + str(exportacion_NO_NECESSARY), "UNK:" + str(exportacion_UNKNOWN))
						print("\tTrans.MT:", transbordos_misma,
							  "V:" + str(transbordo_ACCEPTED), "R:" + str(transbordo_NON_ACCEPTED),
							  "NN:" + str(transbordo_NO_NECESSARY), "UNK:" + str(transbordo_UNKNOWN))
						print("\tTrans.2T:", transbordos_2T)
					else:
						print("\tImport. :", importacion)
						print("\tTrans.MT:", transbordos_misma)
						print("\tTrans.2T:", transbordos_2T)
					print("\t--------------------")
					print("\tLevte.OK:", levante_ok)
					print("\tLevte.KO:", levante_ko)
					print("\tLevte.NN:", levante_nn)
					print("\tLevte.UK:", levante_uk)
					print("\t--------------------")
					print("\tTotal   : " + '\033[91m' + str(total) + '\033[0m')

				if voyage.type == "LOLO":

					if rxlist.circuit == "Carga":
						total_LOLO_carga += len(utilist)
					elif rxlist.circuit == "Descarga":
						total_LOLO_descarga += len(utilist)

					total_LOLO_aceptadas += aceptadas
					total_LOLO_rechazadas += rechazadas
					total_LOLO_pendientes += pendientes
					total_LOLO_modificadas += modificadas
					total_LOLO_añadidas += añadidas
					total_LOLO_total += total
					total_LOLO_llenas += llenas
					total_LOLO_vacias += vacias
					if rxlist.circuit == "Carga":
						total_LOLO_carga_exportacion += exportacion
						total_LOLO_carga_importacion += importacion
						total_LOLO_carga_transbordos_misma += transbordos_misma
						total_LOLO_carga_transbordos_2T += transbordos_2T
					elif rxlist.circuit == "Descarga":
						total_LOLO_descarga_exportacion += exportacion
						total_LOLO_descarga_importacion += importacion
						total_LOLO_descarga_transbordos_misma += transbordos_misma
						total_LOLO_descarga_transbordos_2T += transbordos_2T

					total_LOLO_exportacion_ACCEPTED += exportacion_ACCEPTED
					total_LOLO_exportacion_NON_ACCEPTED += exportacion_NON_ACCEPTED
					total_LOLO_exportacion_NO_NECESSARY += exportacion_NO_NECESSARY
					total_LOLO_exportacion_UNKNOWN += exportacion_UNKNOWN

					total_LOLO_transbordo_ACCEPTED += transbordo_ACCEPTED
					total_LOLO_transbordo_NON_ACCEPTED += transbordo_NON_ACCEPTED
					total_LOLO_transbordo_NO_NECESSARY += transbordo_NO_NECESSARY
					total_LOLO_transbordo_UNKNOWN += transbordo_UNKNOWN

				if voyage.type == "RORO":

					if rxlist.circuit == "Carga":
						total_RORO_carga += len(utilist)

					total_RORO_aceptadas += aceptadas
					total_RORO_rechazadas += rechazadas
					total_RORO_pendientes += pendientes
					total_RORO_modificadas += modificadas
					total_RORO_añadidas += añadidas
					total_RORO_total += total
					total_RORO_llenas += llenas
					total_RORO_vacias += vacias
					if rxlist.circuit == "Carga":
						total_RORO_carga_exportacion += exportacion
						total_RORO_carga_importacion += importacion
						total_RORO_carga_transbordos_misma += transbordos_misma
						total_RORO_carga_transbordos_2T += transbordos_2T

					total_RORO_exportacion_ACCEPTED += exportacion_ACCEPTED
					total_RORO_exportacion_NON_ACCEPTED += exportacion_NON_ACCEPTED
					total_RORO_exportacion_NO_NECESSARY += exportacion_NO_NECESSARY
					total_RORO_exportacion_UNKNOWN += exportacion_UNKNOWN

				total_utis += len(utilist)

			if len(utilist) > 0:
				if voyage.type == "LOLO":
					total_viajes_LOLO += 1
				elif voyage.type == "RORO":
					total_viajes_RORO += 1

print("========================================================================")
print("TOTAL LOLO",total_viajes_LOLO, "viajes:")
print("\tCarga   : " + str(total_LOLO_carga))
print("\t    Exportación  :", total_LOLO_carga_exportacion)
print("\t\tLevante VERDE :", total_LOLO_exportacion_ACCEPTED)
print("\t\tLevante ROJO  :", total_LOLO_exportacion_NON_ACCEPTED)
print("\t\tLevante NO NEC:", total_LOLO_exportacion_NO_NECESSARY)
print("\t\tLevante DESCON:", total_LOLO_exportacion_UNKNOWN)
print("\t    Transbordo 2T:", total_LOLO_carga_transbordos_2T)
print("\t    Transbordo MT:", total_LOLO_carga_transbordos_misma)
print("\t\tLevante VERDE :", total_LOLO_transbordo_ACCEPTED)
print("\t\tLevante ROJO  :", total_LOLO_transbordo_NON_ACCEPTED)
print("\t\tLevante NO NEC:", total_LOLO_transbordo_NO_NECESSARY)
print("\t\tLevante DESCON:", total_LOLO_transbordo_UNKNOWN)
print("\tDescarga: " + str(total_LOLO_descarga))
print("\t    Importación  :", total_LOLO_descarga_importacion)
print("\t    Transbordo 2T:", total_LOLO_descarga_transbordos_2T)
print("\t    Transbordo MT:", total_LOLO_descarga_transbordos_misma)
print("\t--------------------")
print("\tACCEPTED: " + str(total_LOLO_aceptadas))
print("\tREJECTED: " + str(total_LOLO_rechazadas))
print("\tPENDING : " + str(total_LOLO_pendientes))
print("\tMODIFIED: " + str(total_LOLO_modificadas))
print("\tADDED   : " + str(total_LOLO_añadidas))
print("\t--------------------")
print("\tLlenas  : " + str(total_LOLO_llenas))
print("\tVacías  : " + str(total_LOLO_vacias))
print("\t--------------------")
print("\tTotal   : " + '\033[91m' + str(total_LOLO_total) + '\033[0m')
print("========================================================================")
print("TOTAL RORO",total_viajes_RORO, "viajes:")
print("\tCarga   : " + str(total_RORO_carga))
print("\t    Exportación  :", total_RORO_carga_exportacion)
print("\t\tLevante VERDE :", total_RORO_exportacion_ACCEPTED)
print("\t\tLevante ROJO  :", total_RORO_exportacion_NON_ACCEPTED)
print("\t\tLevante NO NEC:", total_RORO_exportacion_NO_NECESSARY)
print("\t\tLevante DESCON:", total_RORO_exportacion_UNKNOWN)
#print("\t    Transbordo 2T:", total_RORO_carga_transbordos_2T)
#print("\t    Transbordo MT:", total_RORO_carga_transbordos_misma)
print("\t--------------------")
print("\tACCEPTED: " + str(total_RORO_aceptadas))
print("\tREJECTED: " + str(total_RORO_rechazadas))
print("\tPENDING : " + str(total_RORO_pendientes))
#print("\tMODIFIED: " + str(total_RORO_modificadas))
#print("\tADDED   : " + str(total_RORO_añadidas))
print("\t--------------------")
print("\tLlenas  : " + str(total_RORO_llenas))
print("\tVacías  : " + str(total_RORO_vacias))
print("\t--------------------")
print("\tTotal   : " + '\033[91m' + str(total_RORO_total) + '\033[0m')

if total_RORO_total != total_RORO_llenas + total_RORO_vacias:
	print("ERROR en los cálculos, revisar llenas!!!")

print("========================================================================")
print("Total Viajes:",str(total_viajes_LOLO+total_viajes_RORO))
print("Total UTIs:" + '\033[91m' + str(total_LOLO_total+total_RORO_total) + '\033[0m')
print(total_transbordos_encontrados, "transbordos en rojo con descarga identificada")
print(total_listas_carga_en_preparacion+total_listas_descarga_en_preparacion,"listas con informe en preparación")
print("======================= END (", datetime.datetime.now().strftime('%d/%m/%Y %H:%M'), ") =======================")
