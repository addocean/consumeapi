import sys
from consumeAPIRest import ConsumeAPIRest
from model.Xml import XmlData

voyage_number = "XMLTEST2/01"
plate = "FPPU0000001"

def main():

	if len(sys.argv) == 2:
		filename = sys.argv[1]
	else:
		print("Argumentos incorrectos. Uso: test_importXML.py filename")
		exit()

	file = open(filename, "r")
	lines = file.readlines()
	xml = ""
	for line in lines:
		xml = xml + line
	file.close()

	vn_ini = xml.find("<voyageNumber>")
	vn_end = xml.find("</voyageNumber>")
	print("Importando fichero en viaje: " + xml[vn_ini+len("<voyageNumber>"):vn_end])

	username = input("Introduzca usuario: " )
	slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")

	xmldata = XmlData(id=0, username=username, xml=xml)

	print("Conecto a SLCD como " + username)
	result = slcd_api.send_user(username=username)
	if not result.ok:
		print("No he podido conectarme como " + username)
		exit()
	else:
		print("Conectado como " + username)

	print("Importando ...")
	slcd_api.import_xml(xmldata=xmldata, print_json=False)

main()
print("FIN de la prueba")
