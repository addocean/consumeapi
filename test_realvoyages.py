from datetime import datetime, timedelta
import random
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from consumeAPIRest import ConsumeAPIRest
from consumeTP_APIRest import ConsumeTP_APIRest
from Generator import Generator

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"

ttia_locations = ['MUNORTE', 'MUESTE']
apmt_locations = ['JCIEST', 'JCINOR', 'JCISUR']


tp_api = ConsumeTP_APIRest()
slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")

print("Conecto a Teleport")
tp_api.send_user()

horizon = 0
horizon = int(input("Para cuantos dias quieres buscar estancias? "))

voyage_list = list()

for delta in range(0,horizon):

	find_eta = (datetime.now() + timedelta(days=delta)).strftime("%Y%m%d%H12")

	stops = tp_api.get_stops_from_ETA(find_eta)

	# Compruebo si alguna de estas estancias tiene un ETA futuro y está AUTORIZADA:
	for stop in stops:

		eta_stop = datetime.strptime(stop["stopEta"],DATETIME_FORMAT) + timedelta(minutes = 5)
		if eta_stop > datetime.now().astimezone(None):

			if stop["visitStatusId"] == "ACEPTADO" or stop["visitStatusId"] == "AUTORIZADO":

				# Compruebo si es de una localización de TTIA
				ttia_valid_stop = False
				for location in ttia_locations:
					if location == stop["locationCode"]:
						ttia_valid_stop = True

				# Compruebo si es de una localización de APMT
				apmt_valid_stop = False
				for location in apmt_locations:
					if location == stop["locationCode"]:
						apmt_valid_stop = True

				if ttia_valid_stop is True or apmt_valid_stop is True:

					if ttia_valid_stop is True:
						voyage_number = "TTIA/" + str(stop["id"])
					elif apmt_valid_stop is True:
						voyage_number = "APMT/" + str(stop["id"])

					new_voyage = Voyage(imo = stop["shipImo"], number = voyage_number, eta = eta_stop.strftime(DATETIME_FORMAT))

					# Si no la tengo ya, la añado
					include = 1
					for voyage in voyage_list:
						if voyage.number == new_voyage.number:
							include = 0

					if include:
						voyage_list.append(new_voyage)

if len(voyage_list) > 0:

	print(str(len(voyage_list)) + " estancias encontradas:")
	for voyage in voyage_list:
		print(voyage.number + " " + voyage.shipImo + " " + voyage.dateTimeETA)

	go = input("Quieres crear los viajes que no existen? ")
	if go is 's':

		terminals_username = ["apmtAdmin", "ttiaAdmin"]

		terminal_username = "apmtAdmin"
		print("Conecto a SLCD como " + terminal_username)
		slcd_api.send_user(username=terminal_username, print_log=True)

		print("Creo los viajes que no existan ...")
		for voyage in voyage_list:
			if voyage.number[0:4] == "APMT":
				slcd_api.create_voyage(voyage)

		terminal_username = "ttiaAdmin"
		print("Conecto a SLCD como " + terminal_username)
		slcd_api.send_user(username=terminal_username, print_log=True)

		print("Creo los viajes que no existan ...")
		for voyage in voyage_list:
			if voyage.number[0:4] == "TTIA":
				slcd_api.create_voyage(voyage)

		go = input("Quieres crear listas y echarles UTIs? ")
		if go is not 's':
			print("\nexit")
			exit()

		generator = Generator(slcd_api)

		for voyage in voyage_list:

			if voyage.number[0:4] == "APMT":

				terminal_username = "apmtAdmin"
				slcd_api.send_user(username = terminal_username)

				voyages = slcd_api.get_voyages_from_number(voyage.number)
				if len(voyages) > 0:
					generator.addSimpleLists2Voyage(voyages[0], 10)

			elif voyage.number[0:4] == "TTIA":

				terminal_username = "ttiaAdmin"
				slcd_api.send_user(username = terminal_username)

				voyages = slcd_api.get_voyages_from_number(voyage.number)
				if len(voyages) > 0:
					generator.addSimpleLists2Voyage(voyages[0], 10)

else:
	print("Ninguna estancia encontrada.")