import os
import datetime
from consumeTP_APIRest import ConsumeTP_APIRest

tp_api = ConsumeTP_APIRest()

print("Conecto a Teleport")
tp_api.send_tda_user()

#plates = ["OOLU0544655","MRKU8124634","HLBU1032671"]
plates = list()

filenames = os.listdir(".")
segment_to_find = "EQD+CN+"

for filename in filenames:
    if ".edi" in filename:

        print("Analizando " + filename)
        result_file = open(filename[0:len(filename)-4] + "_result.txt", "w")

        file = open(filename,"r")
        lines = file.readlines()

        cont = 0
        transbordos = 0
        for line in lines:
            segments = line.split("'")

            for segment in segments:
                pos = segment.find(segment_to_find)
                if pos >= 0:
                    newsegment = segment[pos+len(segment_to_find):pos+len(segment_to_find)+11]

                    if lines[cont+5].find("TDT+10") < 0:
                        plates.append(newsegment)
                    else:
                        transbordos += 1

            cont += 1


        counter = 0
        total = len(plates)

        result_line = "Descartados " + str(transbordos) + " contendores de transbordos"
        result_file.write(result_line + "\n")
        print(result_line)

        result_line = "Analizando " + str(total) + " contendores de exportación"
        result_file.write(result_line + "\n")
        print(result_line)

        for plate in plates:

            found_dsc = 0
            found_dua = 0

            counter +=1
            result_line = "Analizando " + plate + " " + str(counter) + "/" + str(total) + ":"
            result_file.write(result_line + "\n")
            print(result_line)

            containerTDADataList = tp_api.get_tdaData_from_containerPlate(plate)

            if len(containerTDADataList) > 0:

                for tda in containerTDADataList:

                    if tda["documentType"] != "DSC" and tda["documentType"] != "DUA":

                        result_line = "    Encontrado " 
                        result_line += tda["documentType"] + " "
                        result_line += tda["documentNumber"]

                        result_file.write(result_line + "\n")
                        print(result_line)

                        docTDADataList = tp_api.get_tdaData_from_docNumber(tda["documentNumber"])

                        for tda in docTDADataList:

                            result_line = "        "
                            result_line += tda["containerPlate"] + "," 
                            result_line += tda["documentType"] + "," 
                            result_line += tda["documentNumber"] + "," 
                            result_line += tda["documentDate"] + "," 
                            result_line += str(tda["totalElements"]) + "," 
                            result_line += str(tda["elementsWeight"]) + ","
                            result_line += str(tda["documentStatus"])

                            result_file.write(result_line + "\n")
                            print(result_line)

                    elif tda["documentType"] == "DSC" and found_dsc == 0:

                            result_line = "    Encontrado " + tda["documentType"]
                            result_file.write(result_line + "\n")
                            print(result_line)
                            found_dsc = 1

                    elif tda["documentType"] == "DUA" and found_dua == 0:

                            result_line = "    Encontrado " + tda["documentType"]
                            result_file.write(result_line + "\n")
                            print(result_line)
                            found_dua = 1


            else:
                result_line = "    No se encuentra la matrícula en TDA" 
                result_file.write(result_line + "\n")
                print(result_line)

        file.close()
        result_file.close()


'''
tdaDataList = tp_api.get_tdaData_from_dates("202002010000","202002010005")

if len(tdaDataList) > 0:
    print(str(len(tdaDataList)))
    #print(tdaData)
else:
    print("No se encuentra nada en estas fechas en TDA")

'''


