import requests
import json
from datetime import datetime, timedelta

URL_PRU = "http://pre.addocean-pcs.es/slcd/"
URL_PRE = "https://pruebas.teleport.es/slcd/"
URL_PRO = "https://www.teleport.es/slcd/"

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"


class User:
    def __init__(self, name, password):
        self.username = name
        self.password = password

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

class Voyage:
	def __init__(self, imo, number, eta):
		self.id = ""
		self.shipImo = imo
		self.number = number
		self.dateTimeETA = eta
		self.type = ""
		self.initOpsDateTime = ""
		self.endOpsDateTime = ""
		self.consolidator = "TERMINAL"
		self.shippingCompany = "MRSK"
		self.voyageStatus = ""

	def to_json(self):
		return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

	@staticmethod
	def create_from_json(json_data):
		voyage = Voyage(json_data["shipImo"], json_data["number"], json_data["dateTimeETA"])
		voyage.id = json_data["id"]
		voyage.type = json_data["type"]
		voyage.initOpsDateTime = json_data["initOpsDateTime"]
		voyage.endOpsDateTime = json_data["endOpsDateTime"]
		voyage.consolidator = json_data["consolidator"]
		voyage.shippingCompany = json_data["shippingCompany"]
		voyage.voyageStatus = json_data["voyageStatus"]

		return voyage

	def print(self):
		print("\t\tID:             " + str(self.id))
		print("\t\tNUMBER:         " + str(self.number))
		print("\t\tTYPE:           " + str(self.type))
		print("\t\tIMO:            " + str(self.shipImo))
		print("\t\tETA:            " + str(self.dateTimeETA))
		print("\t\tSHIPPING CO.:   " + str(self.shippingCompany))
		print("\t\tCONSOLIDATOR:   " + str(self.consolidator))
		print("\t\tINIT OPS DT:    " + str(self.initOpsDateTime))
		print("\t\tEND  OPS DT:    " + str(self.endOpsDateTime))
		print("\t\tSTATUS:         " + str(self.voyageStatus))

def main():

	print()
	print("Script de Pruebas de Integración de las Terminales vía API RESTful.")
	print("Contiene las siguientes pruebas:")
	print("\tPRUEBA 1: Creación de un nuevo viaje.")
	print("\tPRUEBA 2: Modificación de un viaje existente.")
	test = input("Seleccione la prueba que desea realizar (1/2): ")

	print("\t----------------------------------------")
	print("\tPREPARATIVOS: Selección del entorno de pruebas.")
	print("\t----------------------------------------")

	env = input("\tSeleccione el entorno contra el que quiere hacer la prueba (PRU/PRE/PRO): ")

	url = ""
	while (url == ""):
		if env == "PRU":
			url = URL_PRU
		elif env == "PRE":
			url = URL_PRE
		elif env == "PRO":
			url = URL_PRO
		else:
			print("\tDebe especificar un entorno de los tres.")
			env = input("\tSeleccione el entorno contra el que quiere hacer la prueba (PRU/PRE/PRO): ")

	print("\tEntorno seleccionado para la prueba: " + env + ", url: " + url)

	terminal = input("\tSeleccione la Terminal con la que quiere hacer la prueba (TTIA/APMT): ")

	username = ""
	while (username == ""):
		if terminal == "TTIA":
			username = "ttia_system1"
		elif terminal == "APMT":
			username = "apmt_system1"
		else:
			print("\tDebe especificar una terminal de las dos posibles.")
			terminal = input("\tSelecciona la Terminal con la que quiere hacer la prueba (TTIA/APMT): ")

	print("\tTerminal seleccionada para la prueba: " + terminal + ", usuario: " + username)

	headers_ = {'Content-type': 'application/json'}
	token = None
	user = User(username, "e2bcbf95e071db30384448aad676f937")
	response = requests.post(url + "user", data=user.to_json(), headers=headers_)
	if not response.ok:
		print("\tERROR al conectar como: " + username + str(response))
		print("\tNo se puede continuar con la prueba.")
		exit()
	token = response.content

	print("\tConectado como " + username)

	if test == '1':

		print("\t----------------------------------------")
		print("\tPRUEBA 1: Creación de un nuevo viaje.")
		print("\t----------------------------------------")

		print("\tIntroduzca los datos del nuevo viaje:")
		voyage_number = input("\tNúmero de viaje (7 a 13 caracteres alfanuméricos): ")
		voyage_imo = input("\tIMO (hasta 7 caracteres numéricos): ")
		voyage_shippingCo = input("\tNaviera (código de compañía válido (por ejemplo: \"MRSK\"): ")

		selec_consolidator = input("\tConsolidador (T:TERMINAL/V:VESSEL_OPERATOR): ")

		voyage_consolidator = ""
		while (voyage_consolidator == ""):
			if selec_consolidator == "T":
				voyage_consolidator = "TERMINAL"
			elif selec_consolidator == "V":
				voyage_consolidator = "VESSEL_OPERATOR"
			else:
				print("\tDebe especificar un tipo de consolidador de los dos posibles.")
				terminal = input("\tConsolidador (T:TERMINAL/V:VESSEL_OPERATOR): ")

		eta_dt = None
		while True:
			try:
				eta_dt_str = input("\tETA (formato: YY/MM/DD hh:mm:ss): ")
				eta_dt = datetime.strptime(eta_dt_str, '%y/%m/%d %H:%M:%S')
				break
			except ValueError:
				print("\tNo ha introducido una fecha-hora correcta, inténtelo de nuevo ...")

		voyage = Voyage(imo=voyage_imo, number=voyage_number, eta=eta_dt.strftime(DATETIME_FORMAT))
		voyage.shippingCompany = voyage_shippingCo
		voyage.consolidator = voyage_consolidator

		print("\tCreando viaje ...")
		print(voyage.to_json())

		headers_ = {'Content-type': 'application/json', 'Authorization': token}
		response = requests.post(url + "voyage-api/voyage/", data=voyage.to_json(), headers=headers_)
		if not response.ok:
			print("\tERROR al crear el viaje: "+ str(response))
			print(voyage.to_json())
			print("\tNo se puede continuar con la prueba.")
			exit()

		print("\tConsultamos el viaje para comprobar resultado:")

		found_qty = 0
		while (found_qty == 0):
			response = requests.get(url + "voyage-api/voyage/" + "findByParams?number=" + str(voyage_number), headers=headers_)
			if not response.ok:
				print("\tNo encuentro viaje: " + voyage_number + " " + str(response))
				print("\tNo se puede continuar con la prueba.")
				exit()
			else:
				found_qty = len(response.json())

		if found_qty > 1:
			print("\tSe ha encontrado más de un viaje con el número: " + voyage_number)
			print("\tNo se puede continuar con la prueba.")
			exit()

		voyage = Voyage.create_from_json(response.json()[0])
		voyage.print()

	if test == '2':

		print("\t----------------------------------------")
		print("\tPRUEBA 2: Modificación de un viaje existente.")
		print("\t----------------------------------------")

		voyage_number = input("\tIntroduzca el número de viaje: ")

		headers_ = {'Content-type': 'application/json', 'Authorization': token}
		found_qty = 0
		while (found_qty == 0):
			response = requests.get(url + "voyage-api/voyage/" + "findByParams?number=" + str(voyage_number), headers=headers_)
			if not response.ok:
				print("\tNo encuentro viaje: " + voyage_number + " " + str(response))
				print("\tDebe especificar un número de viaje válido de la terminal " + terminal)
				voyage_number = input("\tIntroduzca el número de viaje: ")
			else:
				found_qty = len(response.json())

		if found_qty > 1:
			print("\tSe ha encontrado más de un viaje con el número: " + voyage_number)
			print("\tNo se puede continuar con la prueba.")
			exit()

		voyage = Voyage.create_from_json(response.json()[0])
		print("\tEncontrado viaje:")
		voyage.print()

		init_dt = None
		print("\tIntroduzca nueva fecha-hora de INICIO de Operaciones ('X' para saltar este paso).")
		while True:
			try:
				init_dt_str = input("\tNueva fecha-hora de INICIO de Operaciones (formato: YY/MM/DD hh:mm:ss): ")
				if init_dt_str == 'X':
					print("\tHa seleccionado saltar el paso de modificación de fecha-hora de INICIO de Operaciones.")
					break
				else:
					init_dt = datetime.strptime(init_dt_str, '%y/%m/%d %H:%M:%S')
					break
			except ValueError:
				print("\tNo ha introducido una fecha-hora correcta, inténtelo de nuevo ...")

		if init_dt is not None:
			voyage.initOpsDateTime = init_dt.strftime(DATETIME_FORMAT)
			response = requests.put(url + "voyage-api/voyage/", data=voyage.to_json(), headers=headers_)
			if not response.ok:
				print("\tERROR al modificar el viaje: " + str(response))
				print("\tNo se puede continuar con la prueba.")
				exit()

			response = requests.get(url + "voyage-api/voyage/" + str(voyage.id), headers=headers_)
			if not response.ok:
				print("\tERROR al obtener el viaje por su id: " + str(voyage.id) + " - ERROR: " + str(response))
				print("\tNo se puede continuar con la prueba.")
				exit()
			new_voyage_1 = Voyage.create_from_json(response.json())
			print("\tObtenemos de nuevo en viaje, compruebe resultado:")
			new_voyage_1.print()

		end_dt = None
		print("\tIntroduzca nueva fecha-hora de FIN de Operaciones ('X' para saltar este paso).")
		while True:
			try:
				end_dt_str = input("\tNueva fecha-hora de FIN de Operaciones (formato: YY/MM/DD hh:mm:ss): ")
				if end_dt_str == 'X':
					print("\tHa seleccionado saltar el paso de modificación de fecha-hora de FIN de Operaciones.")
					break
				else:
					end_dt = datetime.strptime(end_dt_str, '%y/%m/%d %H:%M:%S')
					break
			except ValueError:
				print("\tNo ha introducido una fecha-hora correcta, inténtelo de nuevo ...")

		if end_dt is not None:
			voyage.endOpsDateTime = end_dt.strftime(DATETIME_FORMAT)
			response = requests.put(url + "voyage-api/voyage/", data=voyage.to_json(), headers=headers_)
			if not response.ok:
				print("\tERROR al modificar el viaje: " + str(response))
				print("\tNo se puede continuar con la prueba.")
				exit()

			response = requests.get(url + "voyage-api/voyage/" + str(voyage.id), headers=headers_)
			if not response.ok:
				print("\tERROR al obtener el viaje por su id: " + str(voyage.id) + " - ERROR: " + str(response))
				print("\tNo se puede continuar con la prueba.")
				exit()

			new_voyage_2 = Voyage.create_from_json(response.json())
			print("\tObtenemos de nuevo en viaje, compruebe resultado:")
			new_voyage_2.print()

	print("\t----------------------------------------")
	print("\tFIN de la prueba")
	print()

main()
