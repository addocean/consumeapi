# -*- coding: utf-8
import random

from model.List import List
from model.Uti import Uti
from model.DangerousGood import DangerousGood
from model.Seal import Seal
from model.Reefer import Reefer
from model.User import User

from time import time
import statistics as stats

length_codes = "1234L"
wh_codes = "0123456789CDEFLMNP"
group_codes = ["G0","G1","G2","G3","V0","V2","V4","R0","R1","R2","R3","H0","H1","H2","H5","H6",
    "U1","U2","U3","U4","U5","T0","T1","T2","T3","T4","T5","T6","T7","T8","T9",
    "B0","B1","B3","B4","B5","B6","P0","P1","P2","P3","P4","P5","S0","S1","S2"]

#DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"


class Generator:
	def __init__(self, api):
		self.api = api
		self.used_plates = list()
		self.response_times_list = list()
		
	def addUTI(self, thelist):

		uti = Uti(thelist.id)

		# Establezco valores típicos aleatoriamente

		uti.iso6346Compliance = random.choice([True, True, True, False])
		uti.booking = "BN/TEST/8888"

		used_plate = True
		while used_plate is True:

			if uti.iso6346Compliance == True:
				uti.iso6346Code = random.choice(length_codes) \
								  + random.choice(wh_codes) \
								  + random.choice(group_codes)
				# uti.length = 0
				# uti.height = 0
				# uti.width = 0
				uti.plate = "ADDU{0:03d}{1:04d}".format(random.randrange(0, 999), random.randrange(1, 9999))
				uti.verifiedWeight = uti.grossWeight
			else:
				uti.iso6346Code = ""
				uti.length = random.randrange(700, 3000)
				uti.height = random.randrange(120, 300)
				uti.width = random.randrange(2000, 3000)
				uti.plate = "{0:011d}".format(random.randrange(1, 99999999999))

			used_plate = False
			for plate in self.used_plates:
				if uti.plate == plate:
					print("<<< Used plate: " + plate)
					used_plate = True

		if thelist.circuit == "Carga":
			uti.circuit = random.choice(["EXPORTACION", "TRANSBORDO_MISMA", "TRANSBORDO_ENTRE"])
		else:
			uti.circuit = random.choice(["IMPORTACION", "TRANSBORDO_MISMA", "TRANSBORDO_ENTRE"])

		uti.filledStatus = random.choice(["Lleno", "Lleno", "Lleno", "Vacío"])

		if uti.filledStatus == "Lleno":

			dg_counter = 0
			dg_maxqy = 0
			# 1 de cada 3 con DG
			if random.randrange(3) == 0:
				dg_maxqy = random.randrange(10)
			dangerous_goods = list()
			while dg_counter < dg_maxqy:
				dangerous_goods.append(DangerousGood())
				dg_counter += 1
			uti.dangerousGoods = dangerous_goods

			seal_counter = 0
			seal_maxqy = random.randrange(5)
			seals = list()
			while seal_counter < seal_maxqy:
				seals.append(Seal())
				seal_counter += 1
			uti.seals = seals

			add_reefer = random.choice([True, False])
			if add_reefer:
				uti.reefer = Reefer()

		else:
			uti.dangerousGoods = list()
			uti.seals = list()
			uti.reefer = None

		uti.transshipmentVoyage = "pepe12345"

		start_time = time()
		result = self.api.send_uti_to_voyage(uti)
		response_time = time() - start_time

		self.used_plates.append(uti.plate)

		if not result.ok:
			print("Algo ha ido mal creando esta UTI:")
			print(uti.to_json())
			#exit()
		else:
			self.response_times_list.append(response_time)
			print("\t>>> UTI plate: {} Resp: {:02.3f} MED: {:02.3f} MIN: {:02.3f} MAX: {:02.3f}".format(
				uti.plate,
				response_time,
				stats.mean(self.response_times_list),
				min(self.response_times_list),
				max(self.response_times_list)))

	def addSimpleLists2Voyage(self, voyage, list_size):

		print("Pido las listas del viaje " + voyage.number)
		rxlists = self.api.get_lists_from_voyage(voyage.number)
		print("Obtengo " + str(len(rxlists)) + " listas:")

		utilists = list()

		for rxlist in rxlists:
			utilists = self.api.get_utis_from_voyage(rxlist.id)
			print("Lista id: " + str(rxlist.id) + " Tipo: " + rxlist.type + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")
			if rxlist.type == "Consolidada" and rxlist.circuit == "Carga":
				carga_parent_list = rxlist
			elif rxlist.type == "Consolidada" and rxlist.circuit == "Descarga":
				descarga_parent_list = rxlist
			else:
				print("Algo va mal ...")
				exit()

		usernames =["apl_user1","ems_user1","gsl_user1","cma_user1","cos_user1"]
		for username in usernames:

			print("====================")
			print("Conecto como " + username)
			user = User(username, "e2bcbf95e071db30384448aad676f937")
			result = self.api.send_user_new(user, False)
			if not result.ok:
				print("No he podido conectarme como " + username)
				exit()

			print("Pido las listas del viaje " + voyage.number)
			rxlists = self.api.get_lists_from_voyage(voyage.number)

			if len(rxlists) == 0:

				print("No hay lista simples, creo listas simples de Carga y Descarga:")
				newlist = List()
				newlist.voyageNumber = voyage.number
				newlist.circuit = "Carga"
				self.api.create_list(newlist)
				newlist = List()
				newlist.voyageNumber = voyage.number
				newlist.circuit = "Descarga"
				self.api.create_list(newlist)
			
				print("Y pido de nuevo las listas del viaje." + voyage.number)
				rxlists = self.api.get_lists_from_voyage(voyage.number)
				if len(rxlists) == 0:
					print("Algo ha ido mal creando listas simples ...")
					exit()

			elif len(rxlists) == 1:

				print("Hay una lista simple de " + rxlists[0].circuit + " creo la otra:")

				if rxlists[0].circuit == "Carga":
					newlist = List()
					newlist.voyageNumber = voyage.number
					newlist.circuit = "Descarga"
					self.api.create_list(newlist)
				else:
					newlist = List()
					newlist.voyageNumber = voyage.number
					newlist.circuit = "Carga"
					self.api.create_list(newlist)

				print("Y pido de nuevo las listas del viaje." + voyage.number)
				rxlists = self.api.get_lists_from_voyage(voyage.number)
				if len(rxlists) == 0:
					print("Algo ha ido mal creando listas simples ...")
					exit()

			print("Obtengo " + str(len(rxlists)) + " listas:")

			for rxlist in rxlists:
				print("Lista id: " + str(rxlist.id) + " Tipo: " + rxlist.type + " Circuito: " + rxlist.circuit +
					" del viaje " + voyage.number)

				total_utis = list_size

				print("Añado " + str(total_utis) + " utis ...")

				for counter in range(0,total_utis):
					print("Añadiendo UTI " + str(counter+1) + " de " + str(total_utis) + " en " + voyage.number + " " + rxlist.circuit + " por " + username + " ...")
					self.addUTI(rxlist)

				print("Used plates: " + str(len(self.used_plates)))


'''
				print("------------")
				print("A ver qué hay en la lista ...")
				uti_list = list()
				uti_list = self.api.get_utis_from_voyage(rxlist.id)
'''





