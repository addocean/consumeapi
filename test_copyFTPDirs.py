from datetime import datetime
from ftplib import FTP_TLS
import time

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
DATETIME_FTP_FORMAT = "%Y %b %d %H:%M"


class CopyFTPDirs:
    def __init__(self):
        self.ftp = FTP_TLS()
        self.edi_dir = "coarris/temp"

    def process(self):
        filename = self.read_from_ftp()
        while filename is not None:
            self.copy_to_new_dir_ftp('outbox_slcd_to_edi', filename)
            self.copy_to_new_dir_ftp('outbox_slcd_to_aws', filename)
            self.copy_to_new_dir_ftp('outbox_slcd_backup', filename)
            self.delete_from_ftp(filename)
            time.sleep(5)
            filename = self.read_from_ftp()

    def delete_from_ftp(self, filename):
        self.ftp.connect('pruebas.teleport.es', 21)
        self.ftp.sendcmd('USER ttia')
        self.ftp.sendcmd('PASS vacu83sKpK6B')
        self.ftp.cwd('outbox_slcd')
        self.ftp.delete(filename)
        self.ftp.close()
        print("deleted from ftp %s" % filename)

    def copy_to_new_dir_ftp(self, directory, filename):
        self.ftp.connect('pruebas.teleport.es', 21)
        self.ftp.sendcmd('USER ttia')
        self.ftp.sendcmd('PASS vacu83sKpK6B')
        self.ftp.cwd(directory)
        with open("%s/%s" % (self.edi_dir, filename), 'rb') as pcfile:
            self.ftp.storbinary('STOR ' + filename, pcfile)
        self.ftp.close()
        print("Copied " + filename + " to " + directory)

    def read_from_ftp(self):
        print ("\nReading from ftp " + str(datetime.now().strftime(DATETIME_FORMAT)))
        self.ftp.connect('pruebas.teleport.es', 21)
        self.ftp.sendcmd('USER ttia')
        self.ftp.sendcmd('PASS vacu83sKpK6B')
        self.ftp.cwd('outbox_slcd')
        files = []
        self.ftp.dir(files.append)
        filename = None
        earlier_date = datetime.now()
        result = None
        for file in files:
            line = file.split(' ')
            actual_filename = line[len(line)-1]
            time_string = str(datetime.now().year) + " " + line[len(line)-4] + " " + line[len(line)-3] + " " + line[len(line)-2]
            actual_date = datetime.strptime(time_string, DATETIME_FTP_FORMAT)
            if actual_date < earlier_date:
                earlier_date = actual_date
                filename = actual_filename

        if filename is not None:
            print(filename)
            with open("%s/%s" % (self.edi_dir, filename), 'wb') as pcfile:
                self.ftp.retrbinary("RETR " + filename, pcfile.write)
            pcfile.close()

            result = filename
        self.ftp.close()

        return result


CopyFTPDirs().process()
