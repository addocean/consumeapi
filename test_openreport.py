import sys
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.User import User
from consumeAPIRest import ConsumeAPIRest
from consumeTP_APIRest import ConsumeTP_APIRest

voyage_number = ""
if len(sys.argv) == 2:
	voyage_number = sys.argv[1]
else:
	print("Argumentos incorrectos. Uso: test_endvoyage.py voyage_number")
	exit()


#slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")
slcd_api = ConsumeAPIRest("https://pruebas.teleport.es/slcd/")

username = "ttia_system1"
#username = "apmt_system1"

result = slcd_api.send_user(username, False)
if not result.ok:
	print("\tNo puedo conectar como " + username)
	exit()

voyage = slcd_api.get_voyages_from_number(voyage_number)
print("Voyage ID:",voyage[0].id)

rxlists = slcd_api.get_lists_from_voyage(voyage_number)

if len(rxlists) == 0:
	print("No encuentro las listas consolidadas.")
	exit()
else:
	total_utis = 0
	for rxlist in rxlists:
		utilist = slcd_api.get_utis_from_voyage(rxlist.id)
		print(">>>", rxlist.voyageNumber, rxlist.type, rxlist.circuit, rxlist.consolidationState, "informe",
			  rxlist.reportStatus, "con", str(len(utilist)), "UTIs.")

		if rxlist.reportStatus == "CLOSED":
			go = input("Confirmar con ENTER o 'n' para saltar")
			if go is not 'n':
				rxlist.reportStatus = "PREPARATION"
				slcd_api.put_list(rxlist)

print("FIN de la prueba")




