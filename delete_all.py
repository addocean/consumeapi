from consumeAPIRest import ConsumeAPIRest
import random
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.RMT import RMT
import asserts

URL = "http://localhost:8090/"
DELETE_OBJECTS = False
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class DeleteAllTest:
    def __init__(self):
        self.api = ConsumeAPIRest(URL)

    @staticmethod
    def new_step_message(message, force_input=False):
        do_input = False
        if do_input or force_input:
            input("\n-----\n" + message + " _")
        else:
            print(message)

    def execute(self):

        self.api.send_user("coscoAdmin_ffe")

        voyage_number = input("Enter voyage number to delete: ")
        actual_voyages = self.api.get_voyages_from_number(voyage_number)
        asserts.assert_equals_than(len(actual_voyages), 1)
        actual_voyage = actual_voyages[0]
        actual_voyage.print()

        lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
        for actual_list in lists_from_voyage:
            if actual_list.type.code == "SIMPLE":
                utis = self.api.get_utis_from_voyage(actual_list.id)
                for uti in utis:
                    self.new_step_message("Vamos a borrar la rmt", False)
                    rmts = self.api.find_rmt(uti.plate, uti.booking, False)
                    print("RMTs encontradas " + str(len(rmts)))

                    for rmt in rmts:
                        self.api.delete_rmt(rmt)

                    self.new_step_message("Vamos a buscar la rmt por matrícula y reserva", False)
                    rmts = self.api.find_rmt(uti.plate, uti.booking, False)
                    print("RMTs encontradas " + str(len(rmts)))
                    asserts.assert_equals_than(0, len(rmts))

                    self.api.delete_uti(uti)

                utis = self.api.get_utis_from_voyage(actual_list.id)
                asserts.assert_equals_than(len(utis), 0)
                self.api.delete_list(actual_list)

        lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
        asserts.assert_equals_than(len(lists_from_voyage), 0)

        self.api.send_user("apmtAdmin_ffe")
        lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
        asserts.assert_equals_than(len(lists_from_voyage), 2)
        for actual_list in lists_from_voyage:
            if actual_list.type.code == "CONSOLIDADA":
                self.api.delete_list(actual_list)

        lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
        asserts.assert_equals_than(len(lists_from_voyage), 0)

        self.api.delete_voyage(actual_voyage)
        self.new_step_message("Vamos a pedir el viaje por id " + str(actual_voyage.id))
        actual_voyage = self.api.get_voyage_from_id(actual_voyage.id)
        asserts.assert_none_object(actual_voyage)


DeleteAllTest().execute()
