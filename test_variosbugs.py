# -*- coding: utf-8
import sys
from datetime import datetime, timedelta

from Generator import Generator
from consumeAPIRest import ConsumeAPIRest
from model.Voyage import Voyage

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
#DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")

go = input("\nProbar creación de dos viajes con misma estancia (s/n)? ")

if go == "s":

	terminal_username = "apmtAdmin"
	print("Conecto como " + terminal_username)
	result = slcd_api.send_user(username = terminal_username)
	if not result.ok:
		print("No he podido conectarme como " + terminal_username)
		exit()

	voyage = Voyage(imo="9220885", number="TEST/AAAA", eta="2020-07-14T00:20:00")
	slcd_api.create_voyage(voyage)
	voyages_list = slcd_api.get_voyages_from_number("TEST/AAAA")

	print("--------------------------")

	voyage = Voyage(imo="9220885", number="TEST/BBBB", eta="2020-07-14T00:20:00")
	slcd_api.create_voyage(voyage)
	voyages_list = slcd_api.get_voyages_from_number("TEST/BBBB")

	if len(voyages_list) > 0:
		print(">>>>> Prueba fallida")
	else:
		print(">>>> Prueba OK")

	exit()

go = input("\nProbar si puedo pedir viajes de la otra terminal (s/n)? ")

if go == "s":

	terminal_username = "ttiaAdmin"
	print("Conecto como " + terminal_username)
	result = slcd_api.send_user(username = terminal_username)
	if not result.ok:
		print("No he podido conectarme como " + terminal_username)
		exit()

	print("--------------------------")

	voyage_number = "FPP1/0001"
	print("Pido viaje por number: " + voyage_number)
	voyages_list = slcd_api.get_voyages_from_number(voyage_number)
	if len(voyages_list) == 0:
		print("No obtengo ningún viaje.")
		#exit()
	else:
		print("Obtengo el viaje de id: " + str(voyages_list[0].id))

	print("--------------------------")

	find_id =  voyages_list[0].id
	print("Pido viaje por id: " + str(find_id))
	voyage = slcd_api.get_voyage_from_id(find_id)

	print("--------------------------")

	terminal_username = "apmtAdmin"
	print("Conecto como " + terminal_username)
	result = slcd_api.send_user(username = terminal_username)
	if not result.ok:
		print("No he podido conectarme como " + terminal_username)
		exit()

	find_id =  voyages_list[0].id
	print("Pido viaje por id: " + str(find_id))
	voyage = slcd_api.get_voyage_from_id(find_id)

	if voyage is None:
		print(">>>>> Prueba OK")
	else:
		print(">>>> Prueba fallida")

	exit()

go = input("\nProbar si puedo pedir listas de otro CO (s/n)? ")

if go == "s":

	voyage_number = "FPP1/0001"

	co_username = "coscoAdmin"
	print("Conecto como " + co_username)
	result = slcd_api.send_user(username = co_username)
	if not result.ok:
		print("No he podido conectarme como " + co_username)
		exit()

	print("--------------------------")

	print("Pido las listas del viaje " + voyage_number)
	rxlists = slcd_api.get_lists_from_voyage(voyage_number)
	print("Obtengo " + str(len(rxlists)) + " listas:")

	for rxlist in rxlists:
		utilists = slcd_api.get_utis_from_voyage(rxlist.id)
		print("Lista id: " + str(rxlist.id) + " Tipo: " + rxlist.type + " Circuito: " + rxlist.circuit + " con " + str(len(utilists)) + " UTIs.")

	print("--------------------------")

	co_username = "cmaAdmin"
	print("Conecto como " + co_username)
	result = slcd_api.send_user(username = co_username)
	if not result.ok:
		print("No he podido conectarme como " + co_username)
		exit()

	print("--------------------------")

	print("Pido lista de otro CO por su id:")
	new_list = slcd_api.get_list_from_id(rxlists[0].id)
	utis = slcd_api.get_utis_from_voyage(rxlist.id)
	print("OBTENGO Lista id: " + str(new_list.id) + " con " + str(len(utis)) + " UTIs.")






