from consumeAPIRest import ConsumeAPIRest
import random
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
from model.RMT import RMT

URL = "http://localhost:8090/"
NUMBER_OF_LISTS = 1
NUMBER_OF_CONTAINERS = 10
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class SimpleTestLevanteMaritimo():
    def __init__(self):
        self.api = ConsumeAPIRest(URL)

    @staticmethod
    def new_step_message(message, force_input=False):
        do_input = False
        if do_input or force_input:
            input("\n-----\n" + message + " _")
        else:
            print(message)

    def execute(self):

        for j in range(NUMBER_OF_LISTS):
            random_number = random.randint(1, 1000000)
            voyage_number = "FFE" + str(random_number).rjust(7, '0')
            print("VoyageNumber = " + voyage_number)
        
            self.new_step_message("Vamos a crear un viaje", False)
            self.api.send_user("apmtAdmin_ffe")
            eta_ = ("2020-03-25T00:00:00.000")
            new_voyage = Voyage(imo="9164251", number=voyage_number, eta=eta_)
            new_voyage_id = self.api.create_voyage(new_voyage)

            self.new_step_message("Vamos a pedir el viaje por número " + voyage_number)
            actual_voyage = self.api.get_voyages_from_number(voyage_number)[0]
            actual_voyage.print()

            self.new_step_message("Vamos a obtener las listas del viaje por el número de viaje " + voyage_number)
            parent_lists = self.api.get_lists_from_voyage(voyage_number)
            for parent_list in parent_lists:
                parent_list.print()

            self.new_step_message("Cambio de usuario")
            self.api.send_user("coscoAdmin")
        
            self.new_step_message("Vamos a crear una lista simple")
            simple_list = List(actual_voyage, "Descarga", parent_lists[0])
            self.api.create_list(simple_list)
        
            self.new_step_message("Vamos a perdir las listas para este usuario")
            lists_from_voyage = self.api.get_lists_from_voyage(voyage_number)
            print("Listas actuales para el viaje = " + str(len(lists_from_voyage)))
            for list_from_voyage in lists_from_voyage:
                list_from_voyage.print()
        
            self.new_step_message("Pasamos a la gestión de UTIs", False)
        
            for actual_list in lists_from_voyage:
                if actual_list.type.code == "SIMPLE":
                    utis = self.api.get_utis_from_voyage(actual_list.id)
                    print("Utis en la lista: " + str(len(utis)))
        
                    for i in range(NUMBER_OF_CONTAINERS):
                        self.new_step_message("Vamos a crear una UTI " + str(i+1))
                        new_uti = Uti(actual_list)
                        self.api.send_uti_to_voyage(new_uti)


SimpleTestLevanteMaritimo().execute()
