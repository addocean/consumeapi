
import requests
import json
import base64

URL = "http://localhost:8080/api/"
USER_ENDPOINT = "user/"
DOCUMENT_ENDPOINT = "document/"
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class DocumentData:
    def __init__(self):
        self.id = None
        self.name = None
        self.data = None

    def to_json(self):
        return json.dumps({"id":None,"name":"name","data":self.data})
        # return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        document = DocumentData()
        document.id = json_data["id"]
        document.name = json_data["name"]
        document.data = json_data["data"]
        return document


class User:
    def __init__(self):
        self.userName = ""
        self.token = ""
        self.teleportSession = ""

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        user = User()
        user.userName = json_data["userName"]
        user.token = json_data["token"]
        user.teleportSession = json_data["teleportSession"]
        return user


class DocumentsTest:
    def __init__(self):
        pass

    @staticmethod
    def send_user(user, print_log=False):
        user_returned = None
        headers = {'Content-type': 'application/json'}
        response = requests.post(URL + USER_ENDPOINT, data=user.to_json(), headers=headers)
        if not response.ok:
            print("send_user - KO " + str(response))
        else:
            print("send_user - OK")
            if print_log:
                print("send_user - Result: " + str(response.content))
            user_returned = User.create_from_json(response.json())
        return user_returned

    @staticmethod
    def get_documents(user, print_log=False):
        result = list()
        headers = {'Content-type': 'application/json', 'Authorization': user.token}
        response = requests.get(URL + DOCUMENT_ENDPOINT, headers=headers)
        if response.ok:
            print("get_document - OK")
            if print_log:
                print("get_document - Result: " + str(response.content))
            for json_element in response.json():
                result.append (DocumentData.create_from_json(json_element))
        else:
            print("get_document - KO")
        return result

    @staticmethod
    def add_document(user, document, print_log=False):
        result = None
        headers = {'Content-type': 'application/json', 'Authorization': user.token}
        response = requests.post(URL + DOCUMENT_ENDPOINT, document.to_json(), headers=headers)
        if response.ok:
            print("add_document - OK")
            if print_log:
                print("add_document - Result: " + response.content)
            result = DocumentData.create_from_json(response.json())
        else:
            print("add_document - KO")
        return result

    def start(self):
        user = User()
        user.userName = "francho"
        user_returned = self.send_user(user, False)

        documents_db = self.get_documents(user_returned, True)
        for document_db in documents_db:
            print("Document ID: " + str(document_db.id))

        document = DocumentData()
        document.name = "name"
        document.data = b'data to be encoded'
        document_db = self.add_document(user_returned, document)
        if document_db is not None:
            print("Document ID: " + str(document_db.id))


DocumentsTest().start()

