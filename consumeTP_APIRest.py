import requests
from datetime import datetime, timedelta
from model.Voyage import Voyage
from model.List import List
from model.Uti import Uti
import random

#URL = "http://localhost:8090/"
URL_PRE = "http://pruebas.teleport.es/"
URL_PRO = "http://www.teleport.es/"

USER_ENDPOINT = "visitReader/api/user/"
STOP_ENDPOINT = "visitReader/api/stop/"
COMPANY_ENDPOINT = ""
TDA_ENDPOINT = ""

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class ConsumeTP_APIRest:

    def __init__(self):
        self.name = ""
        self.api = URL_PRO
        self.token = None

    def send_user(self):
        headers = {'Content-type': 'application/json'}
        #response = requests.post(self.api + USER_ENDPOINT, data="{\"teleportSession\": \"\", \"token\": \"\", \"userName\":\"slcdc_admin\", \"password\":\"d1c8114e5701ee7ea18d932a944c1efc\"}", headers=headers)
        response = requests.post(self.api + USER_ENDPOINT, data="{\"teleportSession\": \"\", \"token\": \"\", \"userName\":\"addffelez\", \"password\":\"9e35ec07c317bc6e67a6058e7c884bc6\"}", headers=headers)
        if not response.ok:
            print("\tsend_user - ERROR: " + str(response))
            self.token = None
        else:
            print("\tsend_user - Token: Ok")
            self.token = response.json()["token"]

    def get_stops_from_ETA(self, eta):
        result = list()
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        response = requests.get(self.api + STOP_ENDPOINT + "query/{}?ETA=" + eta, headers=headers)
        if not response.ok:
            print("\tget_stops_from_ETA - ERROR: " + str(response))
        else:
            print("\tget_stops_from_ETA (Result = " + str(len(response.json())) + ")")
            result = response.json()
        return result

    def get_stops_from_Number(self, visit_number):
        result = list()
        headers = {'Content-type': 'application/json', 'Authorization': self.token}
        response = requests.get(self.api + STOP_ENDPOINT + "query/{}?visitNumber=" + visit_number, headers=headers)
        if not response.ok:
            print("\tget_stops_from_Number - ERROR: " + str(response))
        else:
            print("\tget_stops_from_Number (Result = " + str(len(response.json())) + ")")
            result = response.json()
        return result


