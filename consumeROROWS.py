# -*- coding: utf-8
from suds.client import Client
import logging

logging.basicConfig(level=logging.INFO)
# logging.getLogger('suds.client').setLevel(logging.DEBUG)

MESSAGE = '<![CDATA[ ' \
          '<?xml version="1.0" encoding="UTF-8"?> ' \
          '<AvisoLlegadaNavieras  xmlns=\"http://www.teleport.es/teleport/xml/1.0/AvisoEntradaNavieras\"  xmlns:c=\"http://www.teleport.es/teleport/xml/1.0/TiposComunes\"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xsi:schemaLocation=\"http://www.teleport.es/teleport/xml/1.0/AvisoEntradaNavierasAvisoLlegadaNaviera.xsd \"> ' \
          '<NifDeclarante>12345678A</NifDeclarante> ' \
          '<FechaHoraAviso>2012-01-31T12:00:00</FechaHoraAviso> ' \
          '<IdEscala> ' \
          '<c:AñoEscala>2020</c:AñoEscala> ' \
          '<c:NumeroEscala>12332</c:NumeroEscala> </IdEscala> ' \
          '<ZonaPortuariaDestino>EMB_CEUTA</ZonaPortuariaDestino> ' \
          '<FechaHoraSalidaBuque>2012-02-01T12:00:00</FechaHoraSalidaBuque> ' \
          '<Avisos> ' \
          '<Aviso> ' \
          '<Equipamiento> ' \
          '<c:TipoEquipamiento>ART</c:TipoEquipamiento> ' \
          '<c:Matricula>ADDU0000010</c:Matricula> </Equipamiento> ' \
          '<NifTransitario>34234546T</NifTransitario> ' \
          '<NumeroDocumentoConductor>76578884R</NumeroDocumentoConductor> ' \
          '<TipoDocumentoConductor>DNI</TipoDocumentoConductor> ' \
          '<NombreApellidosConductor>Ramón Pérez</NombreApellidosConductor> ' \
          '<EsVacio>false</EsVacio> </Aviso> <Aviso> <Equipamiento> ' \
          '<c:TipoEquipamiento>LOR</c:TipoEquipamiento> ' \
          '<c:Matricula>ADDU0000011</c:Matricula> </Equipamiento> ' \
          '<NifTransitario>34234546T</NifTransitario> ' \
          '<NumeroDocumentoConductor>76578884R</NumeroDocumentoConductor> ' \
          '<TipoDocumentoConductor>DNI</TipoDocumentoConductor> ' \
          '<NombreApellidosConductor>Ramón Pérez</NombreApellidosConductor> ' \
          '<EsVacio>false</EsVacio> </Aviso> </Avisos> </AvisoLlegadaNavieras>' \
          ']]>'

class ConsumeROROWS:
    def __init__(self):
        url = 'file:///Users/fpardo/work/SLCD/consumeapi/listaCarga.wsdl'
        self.client = Client(url)

    def send_message(self, username, message):
        print(self.client)
        self.client.options
        result = self.client.service.getListaCarga(username, "$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq", message)
        print(result)


'''
ws = ConsumeROROWS()
ws.send_message("ttiaAdmin", MESSAGE)
'''