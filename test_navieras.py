import requests
import json
from datetime import datetime, timedelta

URL_PRU = "http://pre.addocean-pcs.es/slcd/"
URL_PRE = "https://pruebas.teleport.es/slcd/"
URL_PRO = "https://www.teleport.es/slcd/"


class RMT:
    def __init__(self):
        self.id = ""
        self.plate = ""
        self.booking = ""
        self.type = "---"
        self.rmtToListType = "---"
        self.rmtStatus = "---"
        self.totalPackageQty = 10
        self.totalGoodsWeight = 100.0
        self.voyageNumber = ""
        self.utiAgentList = None

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        print(json_data)
        rmt = RMT()
        rmt.id = json_data["id"]
        rmt.plate = json_data["plate"]
        rmt.booking = json_data["booking"]
        rmt.type = json_data["type"]
        # rmt.rmtToListType = json_data["rmtToListType"]
        #rmt.rmtStatus = json_data["rmtStatus"]
        rmt.totalPackageQty = json_data["totalPackageQty"]
        rmt.totalGoodsWeight = json_data["totalGoodsWeight"]
        rmt.voyageNumber = json_data["voyageNumber"]

        rmt.utiAgentList = list()
        for agent_json in json_data["utiAgentList"]:
            agent = RMTAgent.create_from_json(agent_json)
            rmt.utiAgentList.append(agent)

        return rmt

    def print(self):
        print("\tID:           " + str(self.id))
        print("\tPLATE:        " + str(self.plate))
        print("\tBOOKING:      " + str(self.booking))
        print("\tTYPE:         " + str(self.type))
        print("\tLIST TYPE:    " + str(self.rmtToListType))
        print("\tSTATUS:       " + str(self.rmtStatus))
        print("\tTOTAL P. QTY: " + str(self.totalPackageQty))
        print("\tTOTAL WEIGHT: " + str(self.totalGoodsWeight))
        print("\tVOYAGE NUM.:  " + str(self.voyageNumber))

        print("\tAGENTS:       " + str(len(self.utiAgentList)))
        for agent in self.utiAgentList:
            agent.print()


class RMTAgent:
    def __init__(self):
        self.id = None
        self.nif = "1234567G"
        self.utiAgentStatus = None
        self.customDocumentList = list()
        self.freeDocument = list()
        self.partialPackageQty = 10
        self.partialGoodsWeight = 100.0

        document = RMTDocument()
        document.reference = "20ES00111110001021"
        self.customDocumentList.append(document)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        rmt_agent = RMTAgent()
        #rmt_agent.id = json_data["id"]
        rmt_agent.nif = json_data["nif"]
        #rmt_agent.utiAgentStatus = json_data["utiAgentStatus"]
        rmt_agent.partialPackageQty = json_data["partialPackageQty"]
        rmt_agent.partialGoodsWeight = json_data["partialGoodsWeight"]
        rmt_agent.customDocumentList = list()
        for document_json in json_data["customDocumentList"]:
            document = RMTDocument.create_from_json(document_json)
            rmt_agent.customDocumentList.append(document)

        return rmt_agent

    def print(self):
        #print("\t\tID:           " + str(self.id))
        print("\t\tNIF:          " + str(self.nif))
        #print("\t\tAGENT STATUS: " + str(self.utiAgentStatus))
        print("\t\tTOTAL P. QTY: " + str(self.partialPackageQty))
        print("\t\tTOTAL WEIGHT: " + str(self.partialGoodsWeight))

        print("\t\tDOCUMENTS:    " + str(len(self.customDocumentList)))
        for document in self.customDocumentList:
            document.print()


class RMTDocument:
    def __init__(self):
        self.id = None
        self.reference = ""
        self.dispatchStatus = ""
        self.type = ""
        self.partialPackageQty = 0
        self.partialGoodsWeight = 0
        self.csv = None

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    @staticmethod
    def create_from_json(json_data):
        rmt_document = RMTDocument()
        #rmt_document.id = json_data["id"]
        rmt_document.reference = json_data["reference"]
        rmt_document.type = json_data["type"]
        rmt_document.partialPackageQty = json_data["partialPackageQty"]
        rmt_document.partialGoodsWeight = json_data["partialGoodsWeight"]
        rmt_document.dispatchStatus = json_data["dispatchStatus"]
        rmt_document.csv = json_data["csv"]
        return rmt_document

    def print(self):
        #print("\t\t\tID:           " + str(self.id))
        print("\t\t\tREFERENCE:    " + str(self.reference))
        print("\t\t\tDOC. TYPE:    " + str(self.type))
        print("\t\t\tDOC. STATUS:  " + str(self.dispatchStatus))
        print("\t\t\tTOTAL P. QTY: " + str(self.partialPackageQty))
        print("\t\t\tTOTAL WEIGHT: " + str(self.partialGoodsWeight))
        print("\t\t\tCSV:          " + str(self.csv))

def main():

    print()
    print("Script de Pruebas de Integración de las Navieras vía API RESTful.")
    print("Contiene las siguientes pruebas:")
    print("\tPRUEBA 1: Consulta de una RMT por matrícula y booking.")
    print("\tPRUEBA 2: Consulta de una RMT por matrícula y viaje.")
    print("\tPRUEBA 3: Embarque / desembarque de una UTI.")
    test = input("Seleccione la prueba que desea realizar (1/2/3): ")

    print("\t----------------------------------------")
    print("\tPREPARATIVOS: Selección del entorno de pruebas.")
    print("\t----------------------------------------")

    #env = input("\tSeleccione el entorno contra el que quiere hacer la prueba (PRU/PRE/PRO): ")
    env = "PRE"

    url = ""
    while (url == ""):
        if env == "PRU":
            url = URL_PRU
        elif env == "PRE":
            url = URL_PRE
        elif env == "PRO":
            url = URL_PRO
        else:
            print("\tDebe especificar un entorno de los tres.")
            env = input("\tSeleccione el entorno contra el que quiere hacer la prueba (PRU/PRE/PRO): ")

    print("\tEntorno seleccionado para la prueba: " + env + ", url: " + url)

    if test == '1':
        print("\t----------------------------------------")
        print("\tPRUEBA 1: Consulta de una RMT por matrícula y booking.")
        print("\t----------------------------------------")

        username = input("\tIntroduzca un usuario con rol de OL para hacer la prueba: ")

        headers_ = {'Content-type': 'application/json'}
        token = None
        response = requests.post(url + "userOld", data=username, headers=headers_)
        if not response.ok:
            print("\tERROR al conectar como: " + username + str(response))
            print("\tNo se puede continuar con la prueba.")
            exit()
        token = response.content

        rmt_list = list()
        again = 's'
        while again == 's':

            plate = input("\tIntroduzca matrícula: ")
            bn = input("\tIntroduzca BN: ")

            headers_ = {'Content-type': 'application/json', 'Authorization': token}
            response = requests.get(url + "rmt-api/rmt/" + "findByParams?plate=" + plate + "&booking=" + bn,
                                    headers=headers_)
            if not response.ok:
                print("\tERROR consultando RMT " + plate + " " + bn + ": ")
            else:
                for json_element in response.json():
                    rmt = RMT.create_from_json(json_element)
                    rmt_list.append(rmt)

            print("\tResultado:")
            if len(rmt_list) > 0:
                for rmt_ in rmt_list:
                    rmt_.print()
                    print("\t---")
            else:
                print("\tNo se encuentra esta RMT.")

            again = input("\tDesea probar de nuevo? (s/n): ")

    if test == '2':
        print("\t----------------------------------------")
        print("\tPRUEBA 2: Consulta de una RMT por matrícula y viaje.")
        print("\t----------------------------------------")

        username = input("\tIntroduzca un usuario con rol de OL para hacer la prueba: ")

        headers_ = {'Content-type': 'application/json'}
        token = None
        response = requests.post(url + "userOld", data=username, headers=headers_)
        if not response.ok:
            print("\tERROR al conectar como: " + username + str(response))
            print("\tNo se puede continuar con la prueba.")
            exit()
        token = response.content

        rmt_list = list()
        again = 's'
        while again == 's':

            plate = input("\tIntroduzca matrícula: ")
            voyage_number = input("\tIntroduzca Viaje: ")

            headers_ = {'Content-type': 'application/json', 'Authorization': token}
            response = requests.get(url + "rmt-api/rmt/" + "findByPlateAndVoyage?plate=" + plate + "&voyageNumber=" + voyage_number,
                                    headers=headers_)
            if not response.ok:
                print("\tERROR consultando RMT " + plate + " " + voyage_number + ": ")
            else:
                for json_element in response.json():
                    rmt = RMT.create_from_json(json_element)
                    rmt_list.append(rmt)

            print("\tResultado:")
            if len(rmt_list) > 0:
                for rmt_ in rmt_list:
                    rmt_.print()
                    print("\t---")
            else:
                print("\tNo se encuentra esta RMT.")

            again = input("\tDesea probar de nuevo? (s/n): ")

    if test == '3':
        print("\t----------------------------------------")
        print("\tPRUEBA 3: Embarque / desembarque de una UTI.")
        print("\t----------------------------------------")

        #username = input("\tIntroduzca un usuario con rol de OL para hacer la prueba: ")
        username = "aml_user1"

        headers_ = {'Content-type': 'application/json'}
        token = None
        response = requests.post(url + "userOld", data=username, headers=headers_)
        if not response.ok:
            print("\tERROR al conectar como: " + username + str(response))
            print("\tNo se puede continuar con la prueba.")
            exit()
        token = response.content

        again = 's'
        while again == 's':

            plate = input("\tIntroduzca matrícula: ")

            headers_ = {'Content-type': 'application/json', 'Authorization': token}
            response = requests.get(url + "uti-api/uti/" + "findByPlate?plate=" + plate,headers=headers_)
            if not response.ok:
                print("\tERROR consultando UTI " + plate + ": " + str(response))
            else:
                for json_element in response.json():
                    print(json_element)

            again = input("\tDesea probar de nuevo? (s/n): ")

    print("\t----------------------------------------")
    print("\tFIN de la prueba")
    print()

main()