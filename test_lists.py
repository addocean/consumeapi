# -*- coding: utf-8
import sys
from datetime import datetime, timedelta
from time import time
import statistics as stats

from consumeAPIRest import ConsumeAPIRest
from model.Voyage import Voyage
from model.List import List
from model.User import User
from model.Xml import XmlData

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

def connect_user(api,username):
	print("====================")
	print("Conecto como " + username)
	user = User(username, "e2bcbf95e071db30384448aad676f937")
	result = api.send_user_new(user, False)
	if not result.ok:
		print("No he podido conectarme como " + username)
		exit()

def show_lists(api,lists):
	# Cuento las UTIs de la listas y pinto información
	print("\tObtengo " + str(len(lists)) + " listas:")
	utilists = list()
	for alist in lists:
		utis = api.get_utis_from_voyage(alist.id)
		print("\tList ID: " + str(alist.id) + " " + alist.type + " " + alist.circuit + " / " + str(len(utis)) + " UTI")

def main():

	if len(sys.argv) == 4:
		terminal_username = sys.argv[1]
		voyage_prefix = sys.argv[2]
		voyage_qty = int(sys.argv[3])
	else:
		print("Argumentos incorrectos. Uso: test_utis.py terminal_username voyage_prefix voyage_qty")
		exit()

	response_times_list = list()

	# Cargo las plantillas para COPRAR de carga y descarga

	file = open("coprar_carga_template.xml", "r")
	lines = file.readlines()
	coprar_carga_template = ""
	for line in lines:
		coprar_carga_template = coprar_carga_template + line
	file.close()

	file = open("coprar_descarga_template.xml", "r")
	lines = file.readlines()
	coprar_descarga_template = ""
	for line in lines:
		coprar_descarga_template = coprar_descarga_template + line
	file.close()

	slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")
	#slcd_api = ConsumeAPIRest("https://pruebas.teleport.es/slcd/")

	print(">>> INICIO <<< de la prueba")
	connect_user(slcd_api,terminal_username)

	for counter in range(1, voyage_qty + 1):

		print("====================")
		voyage_number = "{}/{:02d}".format(voyage_prefix, counter)
		print("Pido viaje por number: " + voyage_number)
		voyages_list = slcd_api.get_voyages_from_number(voyage_number)

		# Si el viaje no existe, lo creo
		if len(voyages_list) == 0:
			print("No existe el viaje, lo creo: " + voyage_number)
			voyage_eta = datetime.now() + timedelta(days=20)
			voyage_imo = "9450337"
			voyage = Voyage(imo = voyage_imo, number = voyage_number, eta = voyage_eta.strftime(DATETIME_FORMAT))
			slcd_api.create_voyage(voyage)
			voyages_list = slcd_api.get_voyages_from_number(voyage_number)
		else:
			print("Ya existe el viaje: " + voyage_number)

		if len(voyages_list) == 0:
			print("Algo ha ido mal creando viaje ...")
			exit()

		print("Pido las listas del viaje " + voyage_number)
		consolidated_lists = slcd_api.get_lists_from_voyage(voyage_number)
		show_lists(slcd_api,consolidated_lists)

		# Voy a crear listas simples de estos 5 CO
		co_usernames = ["apl_user1", "ems_user1", "gsl_user1", "cma_user1", "cos_user1"]
		for co_username in co_usernames:

			connect_user(slcd_api,co_username)

			print("Pido las listas del viaje " + voyage_number)
			co_lists = slcd_api.get_lists_from_voyage(voyage_number)

			# Si no tiene listas simples las creo
			if len(co_lists) == 0:
				print("No hay lista simples, creo listas simples de Carga y Descarga ...")
				newlist = List()
				newlist.voyageNumber = voyage_number
				newlist.circuit = "Carga"
				slcd_api.create_list(newlist)
				newlist = List()
				newlist.voyageNumber = voyage_number
				newlist.circuit = "Descarga"
				slcd_api.create_list(newlist)

				print("Y pido de nuevo las listas")
				co_lists = slcd_api.get_lists_from_voyage(voyage_number)

			if len(co_lists) != 2:
				print("Algo ha ido mal creando listas simples ...")
				exit()

			show_lists(slcd_api,co_lists)
			co_short = co_username[0:3].upper()

			# Preparo el COPRAR de carga para este viaje y CO
			xml_to_import = coprar_carga_template
			xml_to_import = xml_to_import.replace("VOYNUMXXX",voyage_number)
			xml_to_import = xml_to_import.replace("ADDU",co_short +"U")
			xml_to_import = xml_to_import.replace("COX", co_short)
			#print(xml_to_import)

			# Importo COPRAR de carga en xml
			print("\t>>> Importo lista de carga:")
			start_time = time()
			xmldata = XmlData(id=0, username=co_username, xml=xml_to_import)
			slcd_api.import_xml(xmldata=xmldata, print_json=False)
			response_time = time() - start_time
			response_times_list.append(response_time)

			print("\tImport time: {:02.3f} MED: {:02.3f} MIN: {:02.3f} MAX: {:02.3f}".format(
				response_time,stats.mean(response_times_list),min(response_times_list),max(response_times_list)))

			# Preparo el COPRAR de descarga para este viaje y CO
			xml_to_import = coprar_descarga_template
			xml_to_import = xml_to_import.replace("VOYNUMXXX",voyage_number)
			xml_to_import = xml_to_import.replace("ADDU",co_short +"U")
			xml_to_import = xml_to_import.replace("COX", co_short)
			#print(xml_to_import)

			# Importo COPRAR de descarga en xml
			print("\t>>> Importo lista de descarga:")
			start_time = time()
			xmldata = XmlData(id=0, username=co_username, xml=xml_to_import)
			slcd_api.import_xml(xmldata=xmldata, print_json=False)
			response_time = time() - start_time
			response_times_list.append(response_time)

			print("\tImport time: {:02.3f} MED: {:02.3f} MIN: {:02.3f} MAX: {:02.3f}".format(
				response_time,stats.mean(response_times_list),min(response_times_list),max(response_times_list)))

			show_lists(slcd_api,co_lists)

		connect_user(slcd_api,terminal_username)
		show_lists(slcd_api,consolidated_lists)

	print(">>> FIN <<< de la prueba")

	exit()

main()



