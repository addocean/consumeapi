from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import base64
import ftplib

# Para habilitar Gmail API y descargar credenciales:
# Ir https://developers.google.com/gmail/api/quickstart/python
# pulsar "Enable the Gmail API" y seguir los pasos

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.modify']

def main():

    # connect to the FTP server
    ftp_session = ftplib.FTP("pruebas.teleport.es", "slcd", "wg45hwb57j")
    ftp_session.cwd("entrada")

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    # Call the Gmail API
    service = build('gmail', 'v1', credentials=creds)
    results = service.users().messages().list(userId='me',labelIds='UNREAD').execute()
    messages = results.get('messages', [])

    if not messages:
        print('No messages found.')
    else:
        print("Found " + str(len(messages)) + " UNREAD messages:")
        for mssg in messages:

            m_id = mssg['id']  # get id of individual message
            message = service.users().messages().get(userId='me', id=m_id).execute()  # fetch the message

            for header in message['payload']['headers']:  # getting the Subject
                if header['name'] == 'Subject':
                    subject = header['value']
                    print("Subject: " + subject)

            for part in message['payload']['parts']: # getting payload and attachments
                if part['filename']:
                    if 'data' in part['body']:
                        data = part['body']['data']
                    else:
                        att_id = part['body']['attachmentId']
                        att = service.users().messages().attachments().get(userId='me', messageId=m_id,id=att_id).execute()
                        data = att['data']
                    file_data = base64.urlsafe_b64decode(data.encode('UTF-8'))
                    filename = part['filename']
                    path_filename = "files/" + filename

                    if filename.find(".S") > 0:
                        print("\tDescargando adjunto: " + filename)

                        with open(path_filename, 'w') as f:
                            f.write(file_data) # write file to local dir

                        with open(path_filename, 'r') as f:
                            ftp_session.storbinary("STOR " + filename, f) # upload file to ftp

            # This will mark the message as read
            service.users().messages().modify(userId='me', id=m_id, body={'removeLabelIds': ['UNREAD']}).execute()

    ftp_session.dir()
    ftp_session.quit()

main()
