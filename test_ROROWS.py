import sys
from consumeROROWS import ConsumeROROWS

def main():

	if len(sys.argv) == 2:
		filename = sys.argv[1]
	else:
		print("Argumentos incorrectos. Uso: test_ROROWS.py filename")
		exit()

	file = open(filename, "r")
	lines = file.readlines()
	xml_message = ""
	for line in lines:
		xml_message = xml_message + line
	file.close()
	#print(xml_message)

	year_ini = xml_message.find("<c:AñoEscala>")
	year_end = xml_message.find("</c:AñoEscala>")
	escala_ini = xml_message.find("<c:NumeroEscala>")
	escala_end = xml_message.find("</c:NumeroEscala>")

	print("Enviando aviso para viaje: " +
		  xml_message[year_ini+len("<c:AñoEscala>"):year_end] +
		  xml_message[escala_ini+len("<c:NumeroEscala>"):escala_end])

	ws = ConsumeROROWS()
	#ws.send_message("trasmeAdmin", xml_message)
	ws.send_message("baleariaAdmin", xml_message)

main()
print("FIN de la prueba")
