# -*- coding: utf-8
import sys
from datetime import datetime, timedelta

from Generator import Generator
from consumeAPIRest import ConsumeAPIRest

if len(sys.argv) == 3:
	list_id = int(sys.argv[1])
	uti_qty = int(sys.argv[2])
else:
	print("Argumentos incorrectos. Uso: test_addUTIs.py username list_id uti_qty")
	exit()

slcd_api = ConsumeAPIRest("http://pre.addocean-pcs.es/slcd/")
generator = Generator(slcd_api)

usernames = ["ttiaAdmin", "apmtAdmin", "coscoAdmin", "cmaAdmin", "mscAdmin", "maerskAdmin"]
alist = None
found = 0
for username in usernames:
	if found == 0:
		print("Conecto como " + username)
		result = slcd_api.send_user(username=username)
		if not result.ok:
			print("No he podido conectarme como " + username)
			exit()

		found = 1
		alist = slcd_api.get_list_from_id(list_id)

		if alist == None:
			print("No encuentro lista con id: " + str(list_id) + " como " + username)
			found = 0
		else:
			print("Encontrada lista con id: " + str(list_id) + " como " + username)

if found == 1:

	utilist = slcd_api.get_utis_from_voyage(list_id)

	print("Lista Tipo: " + alist.type + " Circuito: " + alist.circuit + " Viaje: " + alist.voyageNumber +
		  " en estado: " + alist.state + " consolidación " + alist.consolidationState + " informe " + alist.reportStatus +
		  " con " + str(len(utilist)) + " UTIs.")

	utiStatus_list = list()
	statusUti_list = list()
	for uti in utilist:
		utiStatus_list.append(uti.utiStatus)
		statusUti_list.append(uti.statusUti)

	aceptadas = utiStatus_list.count("ACEPTADA")
	rechazadas = utiStatus_list.count("RECHAZADA")
	pendientes = utiStatus_list.count("PENDIENTE")

	print("\tACEPTADAS : " + str(aceptadas))
	print("\tRECHAZADAS: " + str(rechazadas))
	print("\tPENDIENTES: " + str(pendientes))
	print("\tTotal     : " + str(aceptadas + rechazadas + pendientes))

	print("Se van a añadir " + str(uti_qty) + " UTIs de " + username + " a esta lista de id: " + str(alist.id))
	go = input("Continuar? ")
	if go is not 's':
		exit()

	for counter in range(1,uti_qty + 1):
		print("Añadiendo UTI " + str(counter) + " de " + str(uti_qty) + " ...")
		generator.addUTI(alist)

exit()





