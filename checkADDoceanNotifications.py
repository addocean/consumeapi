import os
import pickle
from slack import WebClient
from slack import RTMClient
from slack.errors import SlackApiError

from datetime import datetime
from consumeAPIRest import ConsumeAPIRest

URL = "http://localhost:8090/"
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"


class CheckADDoceanNotifications:
    def __init__(self, ):
        self.api = ConsumeAPIRest(URL)
        now = datetime.now()
        self.time_string = now.strftime("%Y%m%d%H%M%S")
        self.last_id = self.get_last_id()
        self.slack_token = 'xoxb-390040244247-1486328754087-FhHmtHoUUKauhUQHWfwTG7Hu'

        rtm_client = RTMClient(token=self.slack_token)
        rtm_client.start()

    def start(self):
        print("Checking at " + str(datetime.now()))
        self.api.send_user("addocean")
        notifications = self.api.get_notifications()
        new_notifications = list()
        max_id = -1
        for notification in notifications:
            if notification.id > self.last_id:
                new_notifications.append(notification)
            if notification.id > max_id:
                max_id = notification.id

        for notification in new_notifications:
            print("NEW!! -> " + str(notification.id) + ": " + notification.subject + " - " + notification.content)

        self.save_max_id(max_id)

        if len(new_notifications) > 0:
            self.send_slack_message("Hay " + str(len(new_notifications)) + " nueva(s) notificacion(es) para ADDocean en SLCD")

    @staticmethod
    def get_last_id():
        last_id = -1
        if os.path.exists('checkADDoceanNotifications.lastId'):
            with open('checkADDoceanNotifications.lastId', 'rb') as token:
                last_id = pickle.load(token)
        return last_id

    @staticmethod
    def save_max_id(max_id):
        with open('checkADDoceanNotifications.lastId', 'wb') as token:
            pickle.dump(max_id, token)

    def send_slack_message(self, message):
        slack_client = WebClient(self.slack_token)
        try:
            response = slack_client.chat_postMessage(channel='#scp', text=message)
            assert response["message"]["text"] == message
        except SlackApiError as e:
            # You will get a SlackApiError if "ok" is False
            assert e.response["ok"] is False
            assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
            print(f"Got an error: {e.response['error']}")


CheckADDoceanNotifications().start()
